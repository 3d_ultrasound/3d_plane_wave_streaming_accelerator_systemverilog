/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  testbench.sv                                        //
//                                                                     //
//  Description :  Testbench for beamforming pipeline; this reads      //
//                 data files and distributes data to various modules. //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

`timescale 1ns/100ps

// Add your testbench here

// extern void print_header(string str);
// extern void print_cycles();
// extern void print_stage(string div, int inst, int npc, int valid_inst);
// extern void print_reg(int wb_reg_wr_select_out_hi, int wb_reg_wr_select_out_lo,
//                       int wb_reg_wr_idx_out, int wb_reg_wr_en_out);
// extern void print_membus(int proc2mem_command, int mem2proc_response,
//                          int proc2mem_addr_hi, int proc2mem_addr_lo,
//                          int proc2mem_data_hi, int proc2mem_data_lo);
// extern void print_close();

module testbench;

    // Parameters
    parameter SAMPLE_BIT_WIDTH = 32;
    parameter MAX_BF_AP_SIZE = 32;
    parameter REG_FILE_X = 8;
    parameter REG_FILE_Y = 4;
    parameter NUM_BANKS = 4;
    parameter NUM_BANK_ENTRIES = ((REG_FILE_X*REG_FILE_Y)/NUM_BANKS);
    parameter BITSTREAM_BUFFER_LEN = 64;
    parameter TAU_TX1_MAX_VAL = BITSTREAM_BUFFER_LEN-4;

    // Testbench Signals
    logic        clock;
    logic        reset;
    logic [31:0] clock_count;

    // Module Signals
    // Inputs
    logic        clka; // clock for port A
    logic        cena; // chip enable (clock gating) (if read and write enable, or them and invert)...active LOW, tie to 0
    logic [7:0]  aa; // address A
    logic        clkb; // clock for port B
    logic        cenb; // chip enable (clock gating) (if read and write enable, or them and invert)...active LOW, tie to 0
    logic [7:0]  ab; // address B
    logic [11:0] db; // data B
    logic [2:0]  emaa; // extra margin adjustment A (tunable delay signal); some constant (user guide?); Austin uses 2 in TSMC16
    logic        emasa; // extra margin ajustment - tie to 0
    logic [2:0]  emab; // extra margin adjustment B (tunable delay signal); some constant (user guide?); Austin uses 2 in TSMC16
    logic [1:0]  emawb; // tie to 1
    logic        nwa; // try setting to 1?
    logic        tena; // test mode enable - tie to 1
    logic        bena; // try setting to 1?
    logic        tcena; // chip enable test input, active LOW - tie to 1
    logic [7:0]  taa; // address test input - tie to 0
    logic [11:0] tqa; // test mode data input - tie to 0
    logic        tenb; // test mode enable - tie to 1
    logic        tcenb; // chip enable test input, active LOW - tie to 1
    logic [7:0]  tab; // address test input - tie to 0
    logic [11:0] tdb; // test mode data input - tie to 0
    logic        stova; // self timed override - tie to 0
    logic        stovb; // tie to 0

    // Outputs
    logic        cenya; // chip enable multiplexer output
    logic [7:0]  aya; // address multiplexer output
    logic        cenyb; // chip enable multiplexer output
    logic [7:0]  ayb; // no idea
    logic [11:0] dyb; // no idea
    logic [11:0] qa; // output data for A



    logic [63:0] chk_value;
    logic [15:0] test_counter;

    integer testfile; // output file descriptor

    // Instantiate the isr units
    sram_2p_hs_v60_uvtsvt bank (
        // Inputs
        .CLKA(clock), // clock for port A
        .CENA(cena), // chip enable (clock gating) (if read and write enable, or them and invert)...active LOW, tie to 0
        .AA(aa), // address A
        .CLKB(clock), // clock for port B
        .CENB(cenb), // chip enable (clock gating) (if read and write enable, or them and invert)...active LOW, tie to 0
        .AB(ab), // address B
        .DB(db), // data B
        .EMAA(emaa), // extra margin adjustment A (tunable delay signal); some constant (user guide?); Austin uses 2 in TSMC16
        .EMASA(emasa), // extra margin ajustment - tie to 0
        .EMAB(emab), // extra margin adjustment B (tunable delay signal); some constant (user guide?); Austin uses 2 in TSMC16
        .EMAWB(emawb), // tie to 1
        .NWA(nwa), // try setting to 1?
        .TENA(tena), // test mode enable - tie to 1
        .BENA(bena), // try setting to 1?
        .TCENA(tcena), // chip enable test input, active LOW - tie to 1
        .TAA(taa), // address test input - tie to 0
        .TQA(tqa), // test mode data input - tie to 0
        .TENB(tenb), // test mode enable - tie to 1
        .TCENB(tcenb), // chip enable test input, active LOW - tie to 1
        .TAB(tab), // address test input - tie to 0
        .TDB(tdb), // test mode data input - tie to 0
        .STOVA(stova), // self timed override - tie to 0
        .STOVB(stovb), // tie to 0

        // Outputs
        .CENYA(cenya), // chip enable multiplexer output // assign CENYA_ = TENA_ ? CENA_ : TCENA_;
        .AYA(aya), // address multiplexer output         // assign AYA_ = TENA_ ? AA_ : TAA_;
        .CENYB(cenyb), // chip enable multiplexer output // assign CENYB_ = TENB_ ? CENB_ : TCENB_;
        .AYB(ayb), // no idea                            // assign AYB_ = TENB_ ? AB_ : TAB_;
        .DYB(dyb), // no idea                            // assign DYB_ = TENB_ ? DB_ : TDB_;
        .QA(qa) // output data for A                     // assign QA_ = BENA_ ? ((STOVA_ ? (QA_int_delayed) : (QA_int))) : TQA_;
    );

    // Generate System Clock
    always begin
        #(`VERILOG_CLOCK_PERIOD/2.0);
        clock = ~clock;
    end

    // Count the number of posedges and number of instructions completed
    // till simulation ends
    always @(posedge clock) begin
        if(reset) begin
            clock_count <= `SD 0;
            // instr_count <= `SD 0;
        end else begin
            clock_count <= `SD (clock_count + 1);
            // instr_count <= `SD (instr_count + pipeline_completed_insts);
        end
    end

    // always_comb begin
    //     if(clock) begin
    //         $display("clock: %b", clock);
    //     end
    // end

    // Print the output
    always @(posedge clock) begin

        $fdisplay(testfile, "START");
        $fdisplay(testfile, "mem[]: %b", bank.mem[0]);
        $fdisplay(testfile, "mem[]: %b", bank.mem[1]);
        $fdisplay(testfile, "mem[]: %b", bank.mem[2]);
        $fdisplay(testfile, "mem[]: %b", bank.mem[3]);
        $fdisplay(testfile, "mem[]: %b", bank.mem[4]);
        $fdisplay(testfile, "mem[]: %b", bank.mem[5]);
        $fdisplay(testfile, "mem[]: %b", bank.mem[6]);
        $fdisplay(testfile, "mem[]: %b", bank.mem[7]);
        $fdisplay(testfile, "mem[]: %b", bank.mem[8]);
        $fdisplay(testfile, "mem[]: %b", bank.mem[9]);
        $fdisplay(testfile, "mem[]: %b", bank.mem[10]);
        $fdisplay(testfile, "mem[]: %b", bank.mem[11]);
        $fdisplay(testfile, "mem[]: %b", bank.mem[12]);
        $fdisplay(testfile, "mem[]: %b", bank.mem[13]);
        $fdisplay(testfile, "mem[]: %b", bank.mem[14]);
        $fdisplay(testfile, "mem[]: %b", bank.mem[15]);
        $fdisplay(testfile, "END");

        // $fdisplay(testfile, "cenya: %b", cenya);
        // $fdisplay(testfile, "aya: %b", aya);
        // $fdisplay(testfile, "cenyb: %b", cenyb);
        // $fdisplay(testfile, "ayb: %b", ayb);
        // $fdisplay(testfile, "dyb: %b", dyb);
        // $fdisplay(testfile, "qa: %b", qa);
        $display("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@qa: %d", qa);

        // if(rotate_en_in) begin
        //     $display("Cycles: %d, Input EN: %d, Val: %d", clock_count, rotate_en_in, rotate_in);
        // end
        // if(select_en_out) begin
        //     $display("Cycles: %d, Output NUM: %d, Val: %d, %d, %d, %d", clock_count, select_num_out, select_out[0], select_out[1], select_out[2], select_out[3]);
        //     // $display("%d", select_out[0]);
        //     // $display("%d", select_out[1]);
        //     // $display("%d", select_out[2]);
        //     // $display("%d", select_out[3]);

        //     // data_check[0] = (datum[0]/4) + (datum[1]*3/4);
        //     // data_check[1] = (datum[0]/2) + (datum[1]/2);
        //     // data_check[2] = (datum[0]*3/4) + (datum[1]/4);
        //     // data_check[3] = datum[0];

        //     $display("tau_rx_tx2_next: %b", slct.tau_rx_tx2_next);
        //     // result_check(datum, data_check, select_out);
        // end
        // $display("Cycles: %d, bf_ap_idx: %d, col_tail: %d, reg_val_en_in: %b, wr_bank_idx[0]: %d, wr_idx[0]: %d", clock_count, rgfl.bf_ap_idx, rgfl.col_tail[0], rgfl.reg_val_en_in, rgfl.wr_bank_idx[0], rgfl.wr_idx[0]);
        // $display("Cycles: %d, bf_ap_idx: %d, reg_val_en_in: %b, col_tail[0]: %d", clock_count, rgfl.bf_ap_idx, rgfl.reg_val_en_in, rgfl.col_tail[0]);
        // $display("wr_bank_idx_temp[0]:%d", rgfl.wr_bank_idx_temp[0]);
        // $display("wr_bank_idx_temp[1]:%d", rgfl.wr_bank_idx_temp[1]);
        // $display("wr_bank_idx_temp[2]:%d", rgfl.wr_bank_idx_temp[2]);
        // $display("wr_bank_idx_temp[3]:%d", rgfl.wr_bank_idx_temp[3]);
        // $display("wr_en[0]:%d, wr_idx[0]:%d, wr_data[0]:%d", rgfl.wr_en[0], rgfl.wr_idx[0], rgfl.wr_data[0]);
        // $display("wr_en[1]:%d, wr_idx[1]:%d, wr_data[1]:%d", rgfl.wr_en[1], rgfl.wr_idx[1], rgfl.wr_data[1]);
        // $display("wr_en[2]:%d, wr_idx[2]:%d, wr_data[2]:%d", rgfl.wr_en[2], rgfl.wr_idx[2], rgfl.wr_data[2]);
        // $display("wr_en[3]:%d, wr_idx[3]:%d, wr_data[3]:%d", rgfl.wr_en[3], rgfl.wr_idx[3], rgfl.wr_data[3]);
        // $display("Cycles: %d, regfile_en_in:%d, regfile_in:%d, regfile_bf_ap_done_in:%d, bf_ap_idx:%d", clock_count, regfile_en_in, regfile_in, regfile_bf_ap_done_in, pd.bf_ap_idx);
        // $display("Cycles: %d, apod_en_out:%d, apod_out:%d, apod_bf_ap_done_out:%d", clock_count, apod_en_out, apod_out, apod_bf_ap_done_out);
    end

    initial begin
        // $dumpvars(0,rgfl);
        // $dumpfile("test.vcd");
        // $dumpon;

        testfile = $fopen("test.out", "w");

        // $monitor("Time:%4.0f reset:%b rotate_in:%d select_out:%d", $time, reset, rotate_in, select_out);
        
        // Initialize inputs
        clock <= `SD 1'b0;
        // reset <= `SD 1'b0;

        // clka  <= `SD clock; // clock for port A
        cena  <= `SD 1; // chip enable (clock gating) (if read and write enable, or them and invert)...active LOW, tie to 0
        aa    <= `SD 0; // address A (read?)

        emaa  <= `SD 0; // extra margin adjustment A (tunable delay signal); some constant (user guide?); Austin uses 2 in TSMC16
        emasa <= `SD 0; // extra margin ajustment - tie to 0
        nwa   <= `SD 0; // try setting to 1?
        bena  <= `SD 1; // try setting to 1?
        stova <= `SD 0; // self timed override - tie to 0

        // clkb  <= `SD clock; // clock for port B
        cenb  <= `SD 1; // chip enable (clock gating) (if read and write enable, or them and invert)...active LOW, tie to 0
        ab    <= `SD 0; // address B (write?)
        db    <= `SD 0; // data B (write?)

        emab  <= `SD 0; // extra margin adjustment B (tunable delay signal); some constant (user guide?); Austin uses 2 in TSMC16
        emawb <= `SD 1; // tie to 1
        stovb <= `SD 0; // tie to 0

        tena  <= `SD 1; // test mode enable - tie to 1
        tcena <= `SD 1; // chip enable test input, active LOW - tie to 1
        taa   <= `SD 0; // address test input - tie to 0
        tqa   <= `SD 0; // test mode data input - tie to 0
        tenb  <= `SD 1; // test mode enable - tie to 1
        tcenb <= `SD 1; // chip enable test input, active LOW - tie to 1
        tab   <= `SD 0; // address test input - tie to 0
        tdb   <= `SD 0; // test mode data input - tie to 0

        // Pulse the reset signal
        // $display("@@\n@@\n@@  %t  Asserting System reset......", $realtime);
        $display("@@\n@@\n@@Asserting System reset......");
        @(posedge clock);
        @(posedge clock);

        cenb  <= `SD 1;
        ab    <= `SD 0;
        db    <= `SD 12'b000000000011;

        cena  <= `SD 1;
        aa    <= `SD 0;

        @(posedge clock);
        
        for(int i = 0; i < 10; i++) begin
            cenb  <= `SD 0;
            ab    <= `SD i;
            db    <= `SD i;

            @(posedge clock);

            cena  <= `SD 0;
            aa    <= `SD i;
        end // for(int i = 0; i < 10; i++)
        
        cenb  <= `SD 1;
        
        @(posedge clock);

        cena  <= `SD 1;

        @(posedge clock);
        @(posedge clock);


        $display("@@@DONE\n");
        $fclose(testfile);
        $finish;
    end

endmodule
