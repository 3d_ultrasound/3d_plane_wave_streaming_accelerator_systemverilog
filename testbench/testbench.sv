/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  testbench.sv                                        //
//                                                                     //
//  Description :  Testbench for beamforming pipeline; this reads      //
//                 data files and distributes data to various modules. //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

`timescale 1ns/100ps

// Add your testbench here

// extern void print_open();
// extern void print_header(string str);
// extern void print_cycles();
// extern void print_stage(string div, int inst, int npc, int val2_inst);
// extern void print_reg(int wb_reg_wr_data_out_hi, int wb_reg_wr_data_out_lo,
//                       int wb_reg_wr_idx_out, int wb_reg_wr_en_out);
// extern void print_membus(int proc2mem_command, int mem2proc_response,
//                          int proc2mem_addr_hi, int proc2mem_addr_lo,
//                          int proc2mem_data_hi, int proc2mem_data_lo);
// extern void print_close();

module testbench;

    // Parameters
    parameter INPUT_BIT_WIDTH           = 32; // used as a #define for input files
    parameter IN_OUT_SAMPLE_BIT_WIDTH   = 12;
    parameter PIPELINE_SAMPLE_BIT_WIDTH = 10;
    parameter BITSTREAM_BUFFER_LEN      = 64;
    parameter TAU_TX1_MAX_VAL           = 1024;
    parameter NUM_CHANNELS_X            = 32;
    parameter MAX_BF_AP_SIZE            = 32; // must be less than or equal to NUM_CHANNELS_X and greater than or equal to BF_AP_X
    parameter NUM_SHIFTS                = `BF_AP_SIZE; // this is the floor of (clock_speed / sample_speed), or the number of cycles between each sample
    parameter BF_AP_X                   = `BF_AP_SIZE-1; // the size of the beamforming aperture in the x-dim (this is only in the testbench)
    parameter REG_FILE_X                = 32;
    parameter REG_FILE_Y                = 32;
    parameter NUM_BANKS                 = 4;
    parameter NUM_BANK_ENTRIES          = ((REG_FILE_X*REG_FILE_Y)/NUM_BANKS);
    parameter MAC_BUFFER_ENTRIES        = 32;
    parameter NUM_SRAM_BANKS            = (524288 / `TAU_SRAM_WIDTH / `TAU_SRAM_NUM_ENTRIES);
    parameter NUM_STAGES                = 8; // number of stages in the apodization pipeline; must be a multiple of 2 (min value of 2)


    // Loop Parameters - determines how many times to run the simulation before completing
    parameter LOOP_NUM_FIRINGS_START = 2;
    parameter LOOP_NUM_FIRINGS_END   = 2;
    parameter LOOP_NUM_STAGES_START  = 1;
    parameter LOOP_NUM_STAGES_END    = 1;
    parameter LOOP_NUM_SLICES_START  = 1;
    parameter LOOP_NUM_SLICES_END    = 1;


    // Testbench Signals
    logic                                                                       clock;
    logic                                                                       reset;
    logic [31:0]                                                                clock_count;

    // Pipeline Signals
    logic                                                                       run;
    bsg_fsb_pkt_s                                                               fsb_packet_in;
    logic                                                                       fsb_packet_valid_in;
    logic                                                                       fsb2_ready_in;
    logic [3:0]                                                                 data_tau_rx_tx2_in;
    logic                                                                       data_tau_rx_tx2_en_in;
    logic [$clog2(TAU_TX1_MAX_VAL)-1:0]                                         tau_rx_tx2_sram_rd_delay;
    logic [$clog2(MAX_BF_AP_SIZE)-1:0]                                          data_bf_ap_size_in;
    logic [NUM_CHANNELS_X-1:0][MAX_BF_AP_SIZE-1:0][IN_OUT_SAMPLE_BIT_WIDTH-1:0] data_apod_consts_in;

    bsg_fsb_pkt_s                                                               fsb_packet_out;
    logic                                                                       fsb_packet_valid_out;
    logic                                                                       pipeline_wrapper_ready_out;


    logic [63:0] chk_value;
    logic [15:0] test_counter;

    // Instantiate the isr units
    pipeline_wrapper #(.IN_OUT_SAMPLE_BIT_WIDTH(IN_OUT_SAMPLE_BIT_WIDTH), .PIPELINE_SAMPLE_BIT_WIDTH(PIPELINE_SAMPLE_BIT_WIDTH), .BITSTREAM_BUFFER_LEN(BITSTREAM_BUFFER_LEN), .TAU_TX1_MAX_VAL(TAU_TX1_MAX_VAL), .MAX_BF_AP_SIZE(MAX_BF_AP_SIZE), .REG_FILE_X(REG_FILE_X), .REG_FILE_Y(REG_FILE_Y), .NUM_BANKS(NUM_BANKS), .NUM_SRAM_BANKS(NUM_SRAM_BANKS), .NUM_BANK_ENTRIES(NUM_BANK_ENTRIES), .MAC_BUFFER_ENTRIES(MAC_BUFFER_ENTRIES), .NUM_STAGES(NUM_STAGES), .NUM_CHANNELS_X(NUM_CHANNELS_X)) pipeline_wrap (
        // Inputs
        .clock(clock),
        .reset(reset),
        // Input packet
        .fsb_packet_in(fsb_packet_in),
        .fsb_packet_valid_in(fsb_packet_valid_in),
        // Output buffer not full
        .fsb2_ready_in(fsb2_ready_in),

        // Outputs
        // Output packet
        .fsb_packet_out(fsb_packet_out),
        .fsb_packet_valid_out(fsb_packet_valid_out),
        // Input packet accepted
        .pipeline_wrapper_ready_out(pipeline_wrapper_ready_out)
    );

    // // Some students have had problems just using "@(posedge done)" because their
    // // "done" signals glitch (even though they are the output of a register). This
    // // prevents that by making sure "done" is high at the clock edge.
    // task wait_until_done;
    //     forever begin : wait_loop
    //         @(posedge data_en_out);
    //         @(negedge clock);
    //         if(data_en_out) disable wait_until_done;
    //     end
    // endtask

    // // Task that reports errors
    // task exit_on_error;
    //     input int idx;
    //     input [1:0][IN_OUT_SAMPLE_BIT_WIDTH-1:0] data_in;
    //     input [IN_OUT_SAMPLE_BIT_WIDTH-1:0] chk_value;
    //     input [IN_OUT_SAMPLE_BIT_WIDTH-1:0] data_out;
    //     begin
    //             $display("@@@ Incorrect at time %4.0f", $time);
    //             $display("@@@ Time:%4.0f idx:%d datum:%d old_datum:%d expected value:%d actual value:%d", $time, idx, data_in[0], data_in[1], chk_value, data_out);
    //             $display("@@@Failed");
    //             $finish;
    //     end
    // endtask

    // // Task that compares data_out to what is expected
    // task result_check;
    //     input [1:0][IN_OUT_SAMPLE_BIT_WIDTH-1:0] data_in;
    //     input [3:0][IN_OUT_SAMPLE_BIT_WIDTH-1:0] chk_value;
    //     input [3:0][IN_OUT_SAMPLE_BIT_WIDTH-1:0] data_out;
    //     begin
    //         // Check the answer...
    //         for(int i = 0; i < 3; i++) begin
    //             if(chk_value[i] != data_out[i]) begin
    //                 // $display("datum:%h", data_in);
    //                 exit_on_error(i, data_in, chk_value[i], data_out[i]);
    //             end
    //         end
    //     end
    // endtask

    integer ppfile; // output file descriptor
    integer testfile; // output file descriptor
    integer testfile2; // output file descriptor
    integer testfile3; // output file descriptor
    integer apod_consts_filename; // input file descriptor
    integer tau_tx1_filename; // input file descriptor
    integer rx_signal_filename; // input file descriptor
    integer tau_rx_tx2_filename; // input file descriptor
    integer code; // return code for fscanf
    logic [31:0] input_apod_consts; // assumes the file always contains 32-bit integers
    logic [`APOD_CONSTS_FILE_READ_WIDTH-1:0][IN_OUT_SAMPLE_BIT_WIDTH-1:0] input_apod_consts_temp;
    logic [31:0] input_tau_tx1; // assumes the file always contains 32-bit integers
    logic [`TAU_TX1_FILE_READ_WIDTH-1:0][$clog2(TAU_TX1_MAX_VAL)-1:0] input_tau_tx1_temp;
    logic [31:0] input_rx_signal; // assumes the file always contains 32-bit integers
    logic [`INPUT_DATA_FILE_READ_WIDTH-1:0][IN_OUT_SAMPLE_BIT_WIDTH-1:0] input_rx_signal_temp;
    logic [`TAU_RX_TX2_FILE_READ_WIDTH-1:0][31:0] input_tau_rx_tx2; // assumes the file always contains 32-bit integers
    logic [$clog2(NUM_CHANNELS_X)-1:0] idx; // used to map the apodization constants to the correct pipelines (pipeline 0 doesn't process scanline 0)

    // Generate System Clock
    always begin
        #(`VERILOG_CLOCK_PERIOD/2.0);
        clock = ~clock;
    end

    // Count the number of posedges and number of instructions completed
    // till simulation ends
    always @(posedge clock) begin
        if(reset) begin
            clock_count <= `SD 0;
            // instr_count <= `SD 0;
        end else begin
            clock_count <= `SD (clock_count + 1);
            // instr_count <= `SD (instr_count + pipeline_completed_insts);
        end
    end

    // Print the output
    always @(posedge clock) begin
        /* Apod Unit Testing START */

        /* Inputs */
        // regfile_en_out - CORRECT
        // if(pipeline_wrap.pipeline_0.regfile_en_out[0]) begin
        //     $fdisplay(testfile, "%b", pipeline_wrap.pipeline_0.regfile_en_out[0]);
        // end // if(pipeline_wrap.pipeline_0.regfile_en_out[0])

        // regfile_out - CORRECT
        // if(pipeline_wrap.pipeline_0.regfile_en_out[0]) begin
        //     $fdisplay(testfile, "regfile_out: %d", $signed(pipeline_wrap.pipeline_0.regfile_out[0]));
        // end // if(pipeline_wrap.pipeline_0.regfile_en_out[0])

        // regfile_bf_ap_done_out - CORRECT
        // if(pipeline_wrap.pipeline_0.regfile_bf_ap_done_out[0]) begin
        //     $fdisplay(testfile, "%b", pipeline_wrap.pipeline_0.regfile_bf_ap_done_out[0]);
        // end // if(pipeline_wrap.pipeline_0.regfile_bf_ap_done_out[0])

        // data_apod_consts_in - need a better way to test (this only happens during reset)
        // if(pipeline_wrap.pipeline_0.data_apod_consts_in[0]) begin
        //     $fdisplay(testfile, "1");
        // end // if(pipeline_wrap.pipeline_0.data_apod_consts_in[0])

        /* Outputs */
        // apod_en_out - CORRECT
        // if(pipeline_wrap.pipeline_0.apod_en_out[0]) begin
        //     $fdisplay(testfile, "%b", pipeline_wrap.pipeline_0.apod_en_out[0]);
        // end // if(pipeline_wrap.pipeline_0.apod_en_out[0])

        // apod_out - CORRECT
        // if(pipeline_wrap.pipeline_0.apod_en_out[0]) begin
        //     $fdisplay(testfile, "apod_out: %d", $signed(pipeline_wrap.pipeline_0.apod_out[0]));
        // end // if(pipeline_wrap.pipeline_0.apod_en_out[0])

        // apod_bf_ap_done_out - CORRECT
        // if(pipeline_wrap.pipeline_0.apod_bf_ap_done_out[0]) begin
        //     $fdisplay(testfile, "%b", pipeline_wrap.pipeline_0.apod_bf_ap_done_out[0]);
        // end // if(pipeline_wrap.pipeline_0.apod_bf_ap_done_out[0])

        /* RANDOM */
        // Verification of signed values - CORRECT
        // if(pipeline_wrap.pipeline_0.regfile_en_out[0]) begin
        //     $fdisplay(testfile, "START");
        //     $fdisplay(testfile, "apod_consts: %d", $signed(pipeline_wrap.pipeline_0.pd[0].apod_consts[pipeline_wrap.pipeline_0.pd[0].bf_ap_idx]));
        //     $fdisplay(testfile, "apod_consts: %b", $signed(pipeline_wrap.pipeline_0.pd[0].apod_consts[pipeline_wrap.pipeline_0.pd[0].bf_ap_idx]));
        //     $fdisplay(testfile, "regfile_in: %d", $signed(pipeline_wrap.pipeline_0.pd[0].regfile_in));
        //     $fdisplay(testfile, "regfile_in: %b", $signed(pipeline_wrap.pipeline_0.pd[0].regfile_in));
        //     $fdisplay(testfile, "buffer_next: %d", $signed(pipeline_wrap.pipeline_0.pd[0].buffer_next));
        //     $fdisplay(testfile, "buffer_next: %b", $signed(pipeline_wrap.pipeline_0.pd[0].buffer_next));
        //     $fdisplay(testfile, "buffer_next[22:11]: %d", $signed(pipeline_wrap.pipeline_0.pd[0].buffer_next[22:11]));
        //     $fdisplay(testfile, "buffer_next[22:11]: %b", $signed(pipeline_wrap.pipeline_0.pd[0].buffer_next[22:11]));
        //     $fdisplay(testfile, "END");
        // end // if(pipeline_wrap.pipeline_0.regfile_en_out[0])

        /* Apod Unit Testing END */


        /* Accum Unit Testing START */

        /* Inputs */
        // apod_en_out
        // if(pipeline_wrap.pipeline_0.apod_en_out[0]) begin
        //     $fdisplay(testfile, "%b", pipeline_wrap.pipeline_0.apod_en_out[0]);
        // end // if(pipeline_wrap.pipeline_0.apod_en_out[0])

        // apod_out
        // if(pipeline_wrap.pipeline_0.apod_en_out[0]) begin
        //     $fdisplay(testfile, "apod_out: %d", $signed(pipeline_wrap.pipeline_0.apod_out[0]));
        // end // if(pipeline_wrap.pipeline_0.apod_en_out[0])

        // apod_bf_ap_done_out
        // if(pipeline_wrap.pipeline_0.apod_bf_ap_done_out[0]) begin
        //     $fdisplay(testfile, "%b", pipeline_wrap.pipeline_0.apod_bf_ap_done_out[0]);
        // end // if(pipeline_wrap.pipeline_0.apod_bf_ap_done_out[0])

        /* Outputs */
        // data_en_out
        // if(pipeline_wrap.pipeline_0.data_en_out[0]) begin
        //     $fdisplay(testfile, "%b", pipeline_wrap.pipeline_0.data_en_out[0]);
        // end // if(pipeline_wrap.pipeline_0.data_en_out[0])

        // data_out
        // if(pipeline_wrap.pipeline_0.data_en_out[0]) begin
        //     $fdisplay(testfile, "data_out: %d", $signed(pipeline_wrap.pipeline_0.data_out[0]));
        // end // if(pipeline_wrap.pipeline_0.data_en_out[0])

        /* Accum Unit Testing END */


        /* RANDOM Testing START */
        
        // if(pipeline_wrap.tau_rx_tx2_en) begin
        //     $fdisplay(testfile, "%b", pipeline_wrap.tau_rx_tx2[0]);
        //     $fdisplay(testfile, "%b", pipeline_wrap.tau_rx_tx2[1]);
        //     $fdisplay(testfile, "%b", pipeline_wrap.tau_rx_tx2[2]);
        //     $fdisplay(testfile, "%b", pipeline_wrap.tau_rx_tx2[3]);
        // end // if(pipeline_wrap.pipeline_0.data_tau_rx_tx2_en_in)
        
        // if(pipeline_wrap.pipeline_0.slct[22].tau_rx_tx2_en_in) begin
        //     $fdisplay(testfile, "%b", pipeline_wrap.pipeline_0.slct[22].tau_rx_tx2_in[0]);
        //     $fdisplay(testfile, "%b", pipeline_wrap.pipeline_0.slct[22].tau_rx_tx2_in[1]);
        //     $fdisplay(testfile, "%b", pipeline_wrap.pipeline_0.slct[22].tau_rx_tx2_in[2]);
        //     $fdisplay(testfile, "%b", pipeline_wrap.pipeline_0.slct[22].tau_rx_tx2_in[3]);
        // end // if(pipeline_wrap.pipeline_0.slct[22].tau_rx_tx2_en_in)
        
        // if(pipeline_wrap.tau_rx_tx2_sram_tau_wr_en) begin
        //     // $fdisplay(testfile, "%b", pipeline_wrap.pipeline_0.tau_rx_tx2_sram_tau_wr_en);
        //     // $fdisplay(testfile, "%d", pipeline_wrap.pipeline_0.wr_idx);
        //     for(int i = 0; i < `TAU_RX_TX2_FILE_READ_WIDTH; i++) begin
        //         $fdisplay(testfile, "%b", pipeline_wrap.tau_rx_tx2_sram_tau_wr_data[i]);
        //     end // for(int i = 0; i < `TAU_RX_TX2_FILE_READ_WIDTH; i++)
        // end // if(pipeline_wrap.pipeline_0.data_tau_rx_tx2_en_in)
        
        // if(pipeline_wrap.tau_wr_en) begin
        //     // $fdisplay(testfile, "%b", pipeline_wrap.tau_wr_en_next[pipeline_wrap.tau_wr_bank_idx]);
        //     // $fdisplay(testfile, "%d", pipeline_wrap.pipeline_0.wr_idx);
        //     for(int i = 0; i < `TAU_RX_TX2_FILE_READ_WIDTH; i++) begin
        //         $fdisplay(testfile, "%b", pipeline_wrap.tau_wr_data[0][i]);
        //     end // for(int i = 0; i < `TAU_RX_TX2_FILE_READ_WIDTH; i++)
        // end // if(pipeline_wrap.pipeline_0.data_tau_rx_tx2_en_in)

        // if(data_tau_rx_tx2_sram_wr_en_in && ~init) begin
        //     $display("hmmm...");
        // end // if(data_tau_rx_tx2_sram_wr_en_in && ~init)
        
        // if(pipeline_wrap.tau_b_wr_en[0]) begin
        //     // $fdisplay(testfile, "%b", pipeline_wrap.tau_b_wr_en);
        //     $fdisplay(testfile, "%b: %d", pipeline_wrap.tau_b_wr_en, pipeline_wrap.tau_wr_addr[$clog2(`TAU_SRAM_NUM_ENTRIES)-1:2]);
        // end // if(pipeline_wrap.pipeline_0.tau_wr_en)
        // if(pipeline_wrap.tau_b_wr_en[1]) begin
        //     // $fdisplay(testfile, "%b", pipeline_wrap.tau_b_wr_en);
        //     $fdisplay(testfile, "%b: %d", pipeline_wrap.tau_b_wr_en, pipeline_wrap.tau_wr_addr[$clog2(`TAU_SRAM_NUM_ENTRIES)-1:2]);
        // end // if(pipeline_wrap.pipeline_0.tau_wr_en)
        // if(pipeline_wrap.tau_b_wr_en[2]) begin
        //     // $fdisplay(testfile, "%b", pipeline_wrap.tau_b_wr_en);
        //     $fdisplay(testfile, "%b: %d", pipeline_wrap.tau_b_wr_en, pipeline_wrap.tau_wr_addr[$clog2(`TAU_SRAM_NUM_ENTRIES)-1:2]);
        // end // if(pipeline_wrap.pipeline_0.tau_wr_en)
        // if(pipeline_wrap.tau_b_wr_en[3]) begin
        //     // $fdisplay(testfile, "%b", pipeline_wrap.tau_b_wr_en);
        //     $fdisplay(testfile, "%b: %d", pipeline_wrap.tau_b_wr_en, pipeline_wrap.tau_wr_addr[$clog2(`TAU_SRAM_NUM_ENTRIES)-1:2]);
        // end // if(pipeline_wrap.pipeline_0.tau_wr_en)
        
        // if(pipeline_wrap.tau_wr_en) begin
        //     // $fdisplay(testfile, "%d", pipeline_wrap.pipeline_0.wr_addr);
        //     for(int i = 0; i < (`TAU_SRAM_WIDTH/4); i++) begin
        //         $fdisplay(testfile, "%b", pipeline_wrap.tau_wr_data[i][0]);
        //         $fdisplay(testfile, "%b", pipeline_wrap.tau_wr_data[i][1]);
        //         $fdisplay(testfile, "%b", pipeline_wrap.tau_wr_data[i][2]);
        //         $fdisplay(testfile, "%b", pipeline_wrap.tau_wr_data[i][3]);
        //     end
        // end // if(pipeline_wrap.pipeline_0.tau_wr_en)

        // if(pipeline_wrap.tau_b_rd_en_next[0]) begin
        //     // $fdisplay(testfile, "%b", pipeline_wrap.tau_b_rd_en_next);
        //     // $fdisplay(testfile, "%d", pipeline_wrap.counter[$clog2(`TAU_SRAM_NUM_ENTRIES*(`TAU_SRAM_WIDTH/4))-1:$clog2(`TAU_SRAM_WIDTH/4)]);
        //     $fdisplay(testfile, "%b: %d", pipeline_wrap.tau_b_rd_en_next, pipeline_wrap.tau_rd_addr[$clog2(`TAU_SRAM_NUM_ENTRIES)-1:2]);
        // end // if(pipeline_wrap.pipeline_0.tau_wr_en)
        // if(pipeline_wrap.tau_b_rd_en_next[1]) begin
        //     // $fdisplay(testfile, "%b", pipeline_wrap.tau_b_rd_en_next);
        //     // $fdisplay(testfile, "%d", pipeline_wrap.counter[$clog2(`TAU_SRAM_NUM_ENTRIES*(`TAU_SRAM_WIDTH/4))-1:$clog2(`TAU_SRAM_WIDTH/4)]);
        //     $fdisplay(testfile, "%b: %d", pipeline_wrap.tau_b_rd_en_next, pipeline_wrap.tau_rd_addr[$clog2(`TAU_SRAM_NUM_ENTRIES)-1:2]);
        // end // if(pipeline_wrap.pipeline_0.tau_wr_en)
        // if(pipeline_wrap.tau_b_rd_en_next[2]) begin
        //     // $fdisplay(testfile, "%b", pipeline_wrap.tau_b_rd_en_next);
        //     // $fdisplay(testfile, "%d", pipeline_wrap.counter[$clog2(`TAU_SRAM_NUM_ENTRIES*(`TAU_SRAM_WIDTH/4))-1:$clog2(`TAU_SRAM_WIDTH/4)]);
        //     $fdisplay(testfile, "%b: %d", pipeline_wrap.tau_b_rd_en_next, pipeline_wrap.tau_rd_addr[$clog2(`TAU_SRAM_NUM_ENTRIES)-1:2]);
        // end // if(pipeline_wrap.pipeline_0.tau_wr_en)
        // if(pipeline_wrap.tau_b_rd_en_next[3]) begin
        //     // $fdisplay(testfile, "%b", pipeline_wrap.tau_b_rd_en_next);
        //     // $fdisplay(testfile, "%d", pipeline_wrap.counter[$clog2(`TAU_SRAM_NUM_ENTRIES*(`TAU_SRAM_WIDTH/4))-1:$clog2(`TAU_SRAM_WIDTH/4)]);
        //     $fdisplay(testfile, "%b: %d", pipeline_wrap.tau_b_rd_en_next, pipeline_wrap.tau_rd_addr[$clog2(`TAU_SRAM_NUM_ENTRIES)-1:2]);
        // end // if(pipeline_wrap.pipeline_0.tau_wr_en)
        
        // if(pipeline_wrap.tau_rd_en) begin
        //     // $fdisplay(testfile, "%d", pipeline_wrap.pipeline_0.rd_addr);
        //     // for(int i = 0; i < (`TAU_SRAM_WIDTH/4); i++) begin
        //         $fdisplay(testfile, "%b", pipeline_wrap.tau_rd_data[pipeline_wrap.tau_rd_bank_idx_saved][pipeline_wrap.tau_rd_idx_saved][0]);
        //         $fdisplay(testfile, "%b", pipeline_wrap.tau_rd_data[pipeline_wrap.tau_rd_bank_idx_saved][pipeline_wrap.tau_rd_idx_saved][1]);
        //         $fdisplay(testfile, "%b", pipeline_wrap.tau_rd_data[pipeline_wrap.tau_rd_bank_idx_saved][pipeline_wrap.tau_rd_idx_saved][2]);
        //         $fdisplay(testfile, "%b", pipeline_wrap.tau_rd_data[pipeline_wrap.tau_rd_bank_idx_saved][pipeline_wrap.tau_rd_idx_saved][3]);
        //     // end
        // end // if(pipeline_wrap.pipeline_0.rd_en)
        
        // if(pipeline_wrap.asdf) begin
        //     // $fdisplay(testfile, "%d", pipeline_wrap.pipeline_0.rd_addr);
        //     // for(int i = 0; i < (`TAU_SRAM_WIDTH/4); i++) begin
        //         $fdisplay(testfile, "%d", pipeline_wrap.tau_rd_bank_idx);
        //     // end
        // end // if(pipeline_wrap.pipeline_0.rd_en)

        // if(pipeline_wrap.tau_rx_tx2_en) begin
        //     // $fdisplay(testfile, "%d", pipeline_wrap.pipeline_0.rd_addr);
        //     $fdisplay(testfile, "%b", pipeline_wrap.tau_rx_tx2[0]);
        //     $fdisplay(testfile, "%b", pipeline_wrap.tau_rx_tx2[1]);
        //     $fdisplay(testfile, "%b", pipeline_wrap.tau_rx_tx2[2]);
        //     $fdisplay(testfile, "%b", pipeline_wrap.tau_rx_tx2[3]);
        // end // if(pipeline_wrap.pipeline_0.tau_rx_tx2_en_in)
        
        // if(pipeline_wrap.pipeline_0.data_tau_rx_tx2_en_in[0]) begin
        //     $fdisplay(testfile, "In: %d", $signed(pipeline_wrap.pipeline_0.sample_out[0]));
        // end // if(pipeline_wrap.pipeline_0.sample_en_out[0])

        // if(pipeline_wrap.pipeline_0.interp_en_out[0]) begin
        //     $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.interp_out[0][0]));
        //     $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.interp_out[0][1]));
        //     $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.interp_out[0][2]));
        //     $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.interp_out[0][3]));
        // end // if(pipeline_wrap.pipeline_0.interp_en_out[0])

        // if(pipeline_wrap.pipeline_0.sample_en_out[0]) begin
        //     $fdisplay(testfile, "%b", $signed(pipeline_wrap.pipeline_0.ntrp[0].buffer_next[0]));
        //     $fdisplay(testfile, "%b", $signed(pipeline_wrap.pipeline_0.ntrp[0].buffer_next[1]));
        //     $fdisplay(testfile, "%b", $signed(pipeline_wrap.pipeline_0.ntrp[0].buffer_next[2]));
        //     $fdisplay(testfile, "%b", $signed(pipeline_wrap.pipeline_0.ntrp[0].buffer_next[3]));
        // end // if(pipeline_wrap.pipeline_0.interp_en_out[0])

        // if(pipeline_wrap.pipeline_0.rotate_en_out[22] && (pipeline_wrap.pipeline_0.rotate_bf_ap_idx_out[22] == 10)) begin
        //     $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.rotate_out[22][0]));
        //     $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.rotate_out[22][1]));
        //     $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.rotate_out[22][2]));
        //     $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.rotate_out[22][3]));
        // end // if(pipeline_wrap.pipeline_0.interp_en_out[22])

        if(pipeline_wrap.pipeline_0.rotate_en_out[30]) begin
            if((pipeline_wrap.pipeline_0.slct[30].rotate_bf_ap_idx_in == 2)) begin
                // $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.rotate_out[22][0]));
                // $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.rotate_out[22][1]));
                // $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.rotate_out[22][2]));
                // $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.rotate_out[22][3]));
                // $fdisplay(testfile, "Head: %d", pipeline_wrap.pipeline_0.slct[22].head);
                // $fdisplay(testfile, "tau_tx1_in: %d", pipeline_wrap.pipeline_0.slct[22].tau_tx1_in);
                // $fdisplay(testfile, "Tail: %d", pipeline_wrap.pipeline_0.slct[22].tail);
                // $fdisplay(testfile, "Decrementer: %d", pipeline_wrap.pipeline_0.slct[22].decrementer);
                $fdisplay(testfile, "%b", pipeline_wrap.pipeline_0.slct[30].select_bits_next[0]);
                $fdisplay(testfile, "%b", pipeline_wrap.pipeline_0.slct[30].select_bits_next[1]);
                $fdisplay(testfile, "%b", pipeline_wrap.pipeline_0.slct[30].select_bits_next[2]);
                $fdisplay(testfile, "%b", pipeline_wrap.pipeline_0.slct[30].select_bits_next[3]);
            end // if((pipeline_wrap.pipeline_0.slct[22].rotate_bf_ap_idx_in == 10))
        end // if(pipeline_wrap.pipeline_0.select_en_out[0])
        // $fdisplay(testfile, "Decrementer: %d", pipeline_wrap.pipeline_0.slct[30].decrementer);
        
        // if(pipeline_wrap.pipeline_0.slct[22].tau_rx_tx2_en_in) begin
        //     // if((pipeline_wrap.pipeline_0.slct[22].tau_rx_tx2_bf_ap_idx_in == 10)) begin
        //         // $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.rotate_out[22][0]));
        //         // $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.rotate_out[22][1]));
        //         // $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.rotate_out[22][2]));
        //         // $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.rotate_out[22][3]));
        //         // $fdisplay(testfile, "Tail: %d", pipeline_wrap.pipeline_0.slct[22].tail);
        //         $fdisplay(testfile, "%b", pipeline_wrap.pipeline_0.slct[22].tau_rx_tx2_in[0]);
        //         $fdisplay(testfile, "%b", pipeline_wrap.pipeline_0.slct[22].tau_rx_tx2_in[1]);
        //         $fdisplay(testfile, "%b", pipeline_wrap.pipeline_0.slct[22].tau_rx_tx2_in[2]);
        //         $fdisplay(testfile, "%b", pipeline_wrap.pipeline_0.slct[22].tau_rx_tx2_in[3]);
        //     // end // if(pipeline_wrap.pipeline_0.select_en_out[0][0])
        // end // if(pipeline_wrap.pipeline_0.select_en_out[0])

        // if(data_tau_rx_tx2_en_in) begin
        //     if((pipeline_wrap.pipeline_0.slct[22].tau_rx_tx2_bf_ap_idx == 10)) begin
        //         $fdisplay(testfile, "%b", data_tau_rx_tx2_in[0]);
        //         $fdisplay(testfile, "%b", data_tau_rx_tx2_in[1]);
        //         $fdisplay(testfile, "%b", data_tau_rx_tx2_in[2]);
        //         $fdisplay(testfile, "%b", data_tau_rx_tx2_in[3]);
        //     end // if(pipeline_wrap.pipeline_0.select_en_out[0][0])
        // end // if(pipeline_wrap.pipeline_0.select_en_out[0])

        // if(pipeline_wrap.pipeline_0.select_en_out[22]) begin
        //     if(pipeline_wrap.pipeline_0.select_en_out[22][0] && (pipeline_wrap.pipeline_0.select_bf_ap_idx_out[22] == 10)) begin
        //         $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.select_out[22][0]));
        //     end // if(pipeline_wrap.pipeline_0.select_en_out[0][0])
        //     if(pipeline_wrap.pipeline_0.select_en_out[22][1] && (pipeline_wrap.pipeline_0.select_bf_ap_idx_out[22] == 10)) begin
        //         $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.select_out[22][1]));
        //     end // if(pipeline_wrap.pipeline_0.select_en_out[0][1])
        //     if(pipeline_wrap.pipeline_0.select_en_out[22][2] && (pipeline_wrap.pipeline_0.select_bf_ap_idx_out[22] == 10)) begin
        //         $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.select_out[22][2]));
        //     end // if(pipeline_wrap.pipeline_0.select_en_out[0][2])
        //     if(pipeline_wrap.pipeline_0.select_en_out[22][3] && (pipeline_wrap.pipeline_0.select_bf_ap_idx_out[22] == 10)) begin
        //         $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.select_out[22][3]));
        //     end // if(pipeline_wrap.pipeline_0.select_en_out[0][3])
        // end // if(pipeline_wrap.pipeline_0.select_en_out[0])

        // if(pipeline_wrap.pipeline_0.pd[22].regfile_en_in) begin
        //     if(pipeline_wrap.pipeline_0.pd[22].regfile_en_in && (pipeline_wrap.pipeline_0.pd[22].regfile_rd_col_pointer == 11)) begin
        //     // if(pipeline_wrap.pipeline_0.pd[22].regfile_en_in) begin
        //     // if(pipeline_wrap.pipeline_0.apod_en_out[22]) begin
        //         // $fdisplay(testfile, "%b", pipeline_wrap.pipeline_0.pd[22].buffer_next);
        //         $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.pd[22].buffer_next[21:10]));
        //         // $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.pd[22].buffer_next[22:11]));
        //         // $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.pd[22].regfile_in));
        //         // $fdisplay(testfile, "regfile_rd_col_pointer: %d, %d", pipeline_wrap.pipeline_0.pd[22].regfile_rd_col_pointer, $signed(pipeline_wrap.pipeline_0.pd[22].regfile_in));
        //         // $fdisplay(testfile, "%d", pipeline_wrap.pipeline_0.pd[22].rd_col_pointer);
        //     end // if(pipeline_wrap.pipeline_0.apod_en_out[0])
        // end // if(pipeline_wrap.pipeline_0.apod_en_out[0])

        // if(pipeline_wrap.pipeline_0.rgfl[22].regfile_en_out) begin
        //     $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.rgfl[22].regfile_out));
        // end

        // if(pipeline_wrap.pipeline_0.rgfl[22].regfile_en_out && (pipeline_wrap.pipeline_0.rgfl[22].rd_col_pointer_next == 0)) begin
        //     $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.rgfl[22].regfile_out));
        // end

        // if(pipeline_wrap.pipeline_0.sample_en_out[0]) begin
        //     $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.sample_out[0]));
        // end // if(pipeline_wrap.pipeline_0.sample_en_out[0])

        // if(pipeline_wrap.pipeline_0.interp_en_out[0]) begin
        //     $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.interp_out[0][0]));
        //     $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.interp_out[0][1]));
        //     $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.interp_out[0][2]));
        //     $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.interp_out[0][3]));
        // end // if(pipeline_wrap.pipeline_0.interp_en_out[0])

        // if(pipeline_wrap.pipeline_0.rotate_en_side_out[0]) begin
        //     $fdisplay(testfile2, "bf_ap_idx: %d", pipeline_wrap.pipeline_0.rtt[0].bf_ap_idx);
        //     $fdisplay(testfile2, "rotate_en_side_out: %d", pipeline_wrap.pipeline_0.rotate_en_side_out[0]);
        //     $fdisplay(testfile2, "%d", $signed(pipeline_wrap.pipeline_0.rotate_side_out[0][0]));
        //     $fdisplay(testfile2, "%d", $signed(pipeline_wrap.pipeline_0.rotate_side_out[0][1]));
        //     $fdisplay(testfile2, "%d", $signed(pipeline_wrap.pipeline_0.rotate_side_out[0][2]));
        //     $fdisplay(testfile2, "%d", $signed(pipeline_wrap.pipeline_0.rotate_side_out[0][3]));
        // end // if(pipeline_wrap.pipeline_0.rotate_en_side_out[0])

        // if(pipeline_wrap.pipeline_0.rotate_en_out[0]) begin
        //     // $fdisplay(testfile2, "rotate_bf_ap_idx_out: %d", pipeline_wrap.pipeline_0.rotate_bf_ap_idx_out[0]);
        //     // $fdisplay(testfile2, "rotate_en_out: %d", pipeline_wrap.pipeline_0.rotate_en_out[0]);
        //     $fdisplay(testfile2, "%d", $signed(pipeline_wrap.pipeline_0.rotate_out[0][0]));
        //     $fdisplay(testfile2, "%d", $signed(pipeline_wrap.pipeline_0.rotate_out[0][1]));
        //     $fdisplay(testfile2, "%d", $signed(pipeline_wrap.pipeline_0.rotate_out[0][2]));
        //     $fdisplay(testfile2, "%d", $signed(pipeline_wrap.pipeline_0.rotate_out[0][3]));
        // end // if(pipeline_wrap.pipeline_0.rotate_en_out[0])

        // if(pipeline_wrap.pipeline_0.rotate_en_out[23]) begin
        //     if((pipeline_wrap.pipeline_0.slct[23].rotate_bf_ap_idx_in == 11)) begin
        //         $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.rotate_out[23][0]));
        //         $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.rotate_out[23][1]));
        //         $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.rotate_out[23][2]));
        //         $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.rotate_out[23][3]));
        //         // $fdisplay(testfile3, "Head: %d", pipeline_wrap.pipeline_0.slct[22].head);
        //         // $fdisplay(testfile3, "tau_tx1_in: %d", pipeline_wrap.pipeline_0.slct[22].tau_tx1_in);
        //         // $fdisplay(testfile3, "Tail: %d", pipeline_wrap.pipeline_0.slct[22].tail);
        //         // $fdisplay(testfile3, "Decrementer: %d", pipeline_wrap.pipeline_0.slct[22].decrementer);
        //         // $fdisplay(testfile, "%b", pipeline_wrap.pipeline_0.slct[23].select_bits[0]);
        //         // $fdisplay(testfile, "%b", pipeline_wrap.pipeline_0.slct[23].select_bits[1]);
        //         // $fdisplay(testfile, "%b", pipeline_wrap.pipeline_0.slct[23].select_bits[2]);
        //         // $fdisplay(testfile, "%b", pipeline_wrap.pipeline_0.slct[23].select_bits[3]);
        //     end // if((pipeline_wrap.pipeline_0.slct[22].rotate_bf_ap_idx_in == 10))
        // end // if(pipeline_wrap.pipeline_0.select_en_out[0])

        // if(pipeline_wrap.pipeline_0.rotate_en_out[23]) begin
        //     if((pipeline_wrap.pipeline_0.slct[23].rotate_bf_ap_idx_in == 11)) begin
        //         // $fdisplay(testfile2, "%d", $signed(pipeline_wrap.pipeline_0.rotate_out[23][0]));
        //         // $fdisplay(testfile2, "%d", $signed(pipeline_wrap.pipeline_0.rotate_out[23][1]));
        //         // $fdisplay(testfile2, "%d", $signed(pipeline_wrap.pipeline_0.rotate_out[23][2]));
        //         // $fdisplay(testfile2, "%d", $signed(pipeline_wrap.pipeline_0.rotate_out[23][3]));
        //         // $fdisplay(testfile3, "Head: %d", pipeline_wrap.pipeline_0.slct[22].head);
        //         // $fdisplay(testfile3, "tau_tx1_in: %d", pipeline_wrap.pipeline_0.slct[22].tau_tx1_in);
        //         // $fdisplay(testfile3, "Tail: %d", pipeline_wrap.pipeline_0.slct[22].tail);
        //         // $fdisplay(testfile3, "Decrementer: %d", pipeline_wrap.pipeline_0.slct[22].decrementer);
        //         $fdisplay(testfile2, "%b", pipeline_wrap.pipeline_0.slct[23].select_bits[0]);
        //         $fdisplay(testfile2, "%b", pipeline_wrap.pipeline_0.slct[23].select_bits[1]);
        //         $fdisplay(testfile2, "%b", pipeline_wrap.pipeline_0.slct[23].select_bits[2]);
        //         $fdisplay(testfile2, "%b", pipeline_wrap.pipeline_0.slct[23].select_bits[3]);
        //     end // if((pipeline_wrap.pipeline_0.slct[22].rotate_bf_ap_idx_in == 10))
        // end // if(pipeline_wrap.pipeline_0.select_en_out[0])

        // if(pipeline_wrap.pipeline_0.rotate_en_out[23]) begin
        //     if((pipeline_wrap.pipeline_0.slct[23].rotate_bf_ap_idx_in == 17)) begin
        //         // $fdisplay(testfile3, "%d", $signed(pipeline_wrap.pipeline_0.rotate_out[23][0]));
        //         // $fdisplay(testfile3, "%d", $signed(pipeline_wrap.pipeline_0.rotate_out[23][1]));
        //         // $fdisplay(testfile3, "%d", $signed(pipeline_wrap.pipeline_0.rotate_out[23][2]));
        //         // $fdisplay(testfile3, "%d", $signed(pipeline_wrap.pipeline_0.rotate_out[23][3]));
        //         // $fdisplay(testfile3, "Head: %d", pipeline_wrap.pipeline_0.slct[22].head);
        //         // $fdisplay(testfile3, "tau_tx1_in: %d", pipeline_wrap.pipeline_0.slct[22].tau_tx1_in);
        //         // $fdisplay(testfile3, "Tail: %d", pipeline_wrap.pipeline_0.slct[22].tail);
        //         // $fdisplay(testfile3, "Decrementer: %d", pipeline_wrap.pipeline_0.slct[22].decrementer);
        //         $fdisplay(testfile3, "%b", pipeline_wrap.pipeline_0.slct[23].select_bits[0]);
        //         $fdisplay(testfile3, "%b", pipeline_wrap.pipeline_0.slct[23].select_bits[1]);
        //         $fdisplay(testfile3, "%b", pipeline_wrap.pipeline_0.slct[23].select_bits[2]);
        //         $fdisplay(testfile3, "%b", pipeline_wrap.pipeline_0.slct[23].select_bits[3]);
        //     end // if((pipeline_wrap.pipeline_0.slct[22].rotate_bf_ap_idx_in == 10))
        // end // if(pipeline_wrap.pipeline_0.select_en_out[0])
        // if(pipeline_wrap.pipeline_0.select_en_out[30] && (pipeline_wrap.pipeline_0.select_bf_ap_idx_out[30] == 2)) begin
        //     if(pipeline_wrap.pipeline_0.select_en_out[30][0]) begin
        //         $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.select_out[30][0]));
        //     end // if(pipeline_wrap.pipeline_0.select_en_out[22][0])
        //     if(pipeline_wrap.pipeline_0.select_en_out[30][1]) begin
        //         $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.select_out[30][1]));
        //     end // if(pipeline_wrap.pipeline_0.select_en_out[22][1])
        //     if(pipeline_wrap.pipeline_0.select_en_out[30][2]) begin
        //         $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.select_out[30][2]));
        //     end // if(pipeline_wrap.pipeline_0.select_en_out[22][2])
        //     if(pipeline_wrap.pipeline_0.select_en_out[30][3]) begin
        //         $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.select_out[30][3]));
        //     end // if(pipeline_wrap.pipeline_0.select_en_out[22][3])
        // end // if(pipeline_wrap.pipeline_0.select_en_out[22])
        // if(pipeline_wrap.pipeline_0.select_en_out[23] && (pipeline_wrap.pipeline_0.select_bf_ap_idx_out[23] == 11)) begin
        //     if(pipeline_wrap.pipeline_0.select_en_out[23][0]) begin
        //         $fdisplay(testfile2, "%d", $signed(pipeline_wrap.pipeline_0.select_out[23][0]));
        //     end // if(pipeline_wrap.pipeline_0.select_en_out[22][0])
        //     if(pipeline_wrap.pipeline_0.select_en_out[23][1]) begin
        //         $fdisplay(testfile2, "%d", $signed(pipeline_wrap.pipeline_0.select_out[23][1]));
        //     end // if(pipeline_wrap.pipeline_0.select_en_out[22][1])
        //     if(pipeline_wrap.pipeline_0.select_en_out[23][2]) begin
        //         $fdisplay(testfile2, "%d", $signed(pipeline_wrap.pipeline_0.select_out[23][2]));
        //     end // if(pipeline_wrap.pipeline_0.select_en_out[22][2])
        //     if(pipeline_wrap.pipeline_0.select_en_out[23][3]) begin
        //         $fdisplay(testfile2, "%d", $signed(pipeline_wrap.pipeline_0.select_out[23][3]));
        //     end // if(pipeline_wrap.pipeline_0.select_en_out[22][3])
        // end // if(pipeline_wrap.pipeline_0.select_en_out[22])
        // if(pipeline_wrap.pipeline_0.select_en_out[23] && (pipeline_wrap.pipeline_0.select_bf_ap_idx_out[23] == 11)) begin
        //     if(pipeline_wrap.pipeline_0.select_en_out[23][0]) begin
        //         $fdisplay(testfile3, "%d", $signed(pipeline_wrap.pipeline_0.select_out[23][0]));
        //     end // if(pipeline_wrap.pipeline_0.select_en_out[22][0])
        //     if(pipeline_wrap.pipeline_0.select_en_out[23][1]) begin
        //         $fdisplay(testfile3, "%d", $signed(pipeline_wrap.pipeline_0.select_out[23][1]));
        //     end // if(pipeline_wrap.pipeline_0.select_en_out[22][1])
        //     if(pipeline_wrap.pipeline_0.select_en_out[23][2]) begin
        //         $fdisplay(testfile3, "%d", $signed(pipeline_wrap.pipeline_0.select_out[23][2]));
        //     end // if(pipeline_wrap.pipeline_0.select_en_out[22][2])
        //     if(pipeline_wrap.pipeline_0.select_en_out[23][3]) begin
        //         $fdisplay(testfile3, "%d", $signed(pipeline_wrap.pipeline_0.select_out[23][3]));
        //     end // if(pipeline_wrap.pipeline_0.select_en_out[22][3])
        // end // if(pipeline_wrap.pipeline_0.select_en_out[22])

        // if(pipeline_wrap.read_output && pipeline_wrap.data_rd_en_next[0]) begin
        //     $fdisplay(testfile, "%d", pipeline_wrap.data_rd_addr[0]);
        // end // if(data_en_out[22])

        // if(pipeline_wrap.pipeline_0.data_en_out[30]) begin
        //     $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.data_out[30]));
        // end // if(data_en_out[22])

        // if(pipeline_wrap.data_wr_en[0]) begin
        //     $fdisplay(testfile, "%d", $signed(pipeline_wrap.data_wr_data));
        // end // if(pipeline_wrap.input_wr_en)

        // if(run && pipeline_wrap.data_rd_en_saved[0]) begin
        //     $fdisplay(testfile, "%d", $signed(pipeline_wrap.data_rd_en_saved[0]));
        // end // if(pipeline_wrap.input_wr_en)

        // if(pipeline_wrap.data_en) begin
        //     $fdisplay(testfile, "%d", $signed(pipeline_wrap.data[0]));
        // end // if(pipeline_wrap.data_en)
        
        // if(pipeline_wrap.pipeline_0.tau_rx_tx2_en) begin
        //     // $fdisplay(testfile, "%d", pipeline_wrap.pipeline_0.rd_addr);
        //     $fdisplay(testfile, "%b", pipeline_wrap.pipeline_0.tau_rx_tx2[0]);
        //     $fdisplay(testfile, "%b", pipeline_wrap.pipeline_0.tau_rx_tx2[1]);
        //     $fdisplay(testfile, "%b", pipeline_wrap.pipeline_0.tau_rx_tx2[2]);
        //     $fdisplay(testfile, "%b", pipeline_wrap.pipeline_0.tau_rx_tx2[3]);
        // end // if(pipeline_wrap.pipeline_0.tau_rx_tx2_en)

        // if(pipeline_wrap.pipeline_0.pd[22].regfile_en_in && (pipeline_wrap.pipeline_0.pd[22].bf_ap_idx == 19)) begin
        //     // $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.pd[22].buffer_next[IN_OUT_SAMPLE_BIT_WIDTH+11-1:11]));
        //     $fdisplay(testfile, "buffer_next_full:%b, buffer_next:%b, apod_consts:%b, regfile_in:%b", pipeline_wrap.pipeline_0.pd[22].buffer_next, pipeline_wrap.pipeline_0.pd[22].buffer_next[IN_OUT_SAMPLE_BIT_WIDTH+11-1:11], pipeline_wrap.pipeline_0.pd[22].apod_consts[pipeline_wrap.pipeline_0.pd[22].bf_ap_idx], pipeline_wrap.pipeline_0.pd[22].regfile_in[IN_OUT_SAMPLE_BIT_WIDTH-1-2:0]);
        //     // $fdisplay(testfile, "%b", pipeline_wrap.pipeline_0.pd[22].regfile_bf_ap_done_in);
        //     // $fdisplay(testfile, "buffer_next:%b, apod_consts:%b, regfile_in:%b", pipeline_wrap.pipeline_0.pd[22].buffer_next[IN_OUT_SAMPLE_BIT_WIDTH+11-1:11], pipeline_wrap.pipeline_0.pd[22].apod_consts[pipeline_wrap.pipeline_0.pd[22].bf_ap_idx], pipeline_wrap.pipeline_0.pd[22].regfile_in[IN_OUT_SAMPLE_BIT_WIDTH-1-2:0]);
        // end // if(data_en_out[22])

        // if(pipeline_wrap.pipeline_0.pd[22].apod_en_out) begin
        //     $fdisplay(testfile, "%d", $signed(pipeline_wrap.pipeline_0.pd[22].apod_out));
        // end

        // if(pipeline_wrap.pipeline_0.pd[22].apod_bf_ap_done_out) begin
        //     $fdisplay(testfile, "%b", $signed(pipeline_wrap.pipeline_0.pd[22].apod_out));
        // end

        // if(pipeline_wrap.pipeline_0.ccm[22].accum_en_out) begin
        //     $fdisplay(testfile, "%b", $signed(pipeline_wrap.pipeline_0.ccm[22].accum_out));
        // end

        // if(pipeline_wrap.pipeline_0.rd_en) begin
        //     if(pipeline_wrap.pipeline_0.rd_idx == 0) begin
        //         $fdisplay(testfile, "%d, %d", clock_count, pipeline_wrap.pipeline_0.rd_addr);
        //     end // if(pipeline_wrap.pipeline_0.rd_idx == 7)
            // $fdisplay(testfile, "%d", pipeline_wrap.pipeline_0.rd_idx);
        //     $fdisplay(testfile, "Cycles: %d, %d", clock_count, pipeline_wrap.pipeline_0.rd_addr);
        // end // if(pipeline_wrap.pipeline_0.rd_en)

        // if(pipeline_wrap.pipeline_0.wr_en) begin
        //     if(pipeline_wrap.pipeline_0.rd_idx == 0) begin
        //         $fdisplay(testfile, "%d, %d", clock_count, pipeline_wrap.pipeline_0.rd_addr);
        //     end // if(pipeline_wrap.pipeline_0.rd_idx == 7)
        //     $fdisplay(testfile, "%d", pipeline_wrap.pipeline_0.wr_addr);
        // end // if(pipeline_wrap.pipeline_0.rd_en)

        // if(pipeline_wrap.pipeline_0.data_tau_rx_tx2_en_in) begin
        //     $fdisplay(testfile, "%b", pipeline_wrap.pipeline_0.data_tau_rx_tx2_in[0]);
        //     $fdisplay(testfile, "%b", pipeline_wrap.pipeline_0.data_tau_rx_tx2_in[1]);
        //     $fdisplay(testfile, "%b", pipeline_wrap.pipeline_0.data_tau_rx_tx2_in[2]);
        //     $fdisplay(testfile, "%b", pipeline_wrap.pipeline_0.data_tau_rx_tx2_in[3]);
        // end // if(pipeline_wrap.pipeline_0.data_tau_rx_tx2_en_in)

        /* RANDOM Testing END */
    end // always @(posedge clock)

    initial begin

        $set_gate_level_monitoring("on");
        $set_toggle_region(pipeline_wrap);
        $toggle_start;

        // $monitor("Time:%4.0f reset:%b data_in:%d data_out:%d", $time, reset, data_in, data_out);
        ppfile = $fopen("pipeline.out", "w");
        testfile = $fopen("test.out", "w");
        testfile2 = $fopen("test2.out", "w");
        testfile3 = $fopen("test3.out", "w");

        for(int firing_id = LOOP_NUM_FIRINGS_START; firing_id <= LOOP_NUM_FIRINGS_END; firing_id++) begin
            for(int stage_id = LOOP_NUM_STAGES_START; stage_id <= LOOP_NUM_STAGES_END; stage_id++) begin
                for(int slice_id = LOOP_NUM_SLICES_START; slice_id <= LOOP_NUM_SLICES_END; slice_id++) begin
                    $display("\n@@\n@@\n@@\nWORKING ON FIRING %d, STAGE %d, SLICE %d\n@@\n@@\n@@\n", firing_id, stage_id, slice_id);
        
                    // Initialize the tau_rx_tx2 sram read delay (this allows for smaller select buffers)
                    tau_rx_tx2_sram_rd_delay = {$clog2(TAU_TX1_MAX_VAL){1'b1}};

                    // Initialize inputs
                    clock                 <= `SD 1'b0;
                    reset                 <= `SD 1'b0;
                    data_tau_rx_tx2_in    <= `SD 0;
                    data_tau_rx_tx2_en_in <= `SD 0;
                    data_bf_ap_size_in    <= `SD BF_AP_X; // take the lower IN_OUT_SAMPLE_BIT_WIDTH bits as the value
                    data_apod_consts_in   <= `SD 0;


                    // Initialize the FSB packet
                    fsb_packet_in.destid  <= `SD 0;
                    fsb_packet_in.cmd     <= `SD 0;
                    fsb_packet_in.opcode  <= `SD NULL_PACKET;
                    fsb_packet_in.srcid   <= `SD 0;
                    fsb_packet_in.data    <= `SD 0;
                    fsb_packet_valid_in   <= `SD 0;

                    // Tell the pipeline that we're ready to receive data (we aren't blocked)
                    fsb2_ready_in         <= `SD 1;

                    // Pulse the reset signal
                    $display("@@\n@@\nAsserting System reset......\n@@\n@@\n");
                    reset <= `SD 1'b1;

                    @(posedge clock);
                    @(posedge clock);
                    @(posedge clock);
                    
                    $display("@@\n@@\nDeasserting System Reset...Initiating Constant Initialization...... %d Cycles\n@@\n@@\n", clock_count);
                    reset <= `SD 1'b0;

                    // Set the pipeline output location
                    fsb_packet_in.opcode    <= `SD LD_OUTPUT_SELECT;
                    fsb_packet_in.data[3:0] <= `SD ACCUM; // take the lower IN_OUT_SAMPLE_BIT_WIDTH bits as the value
                    fsb_packet_valid_in     <= `SD 1;
                    while(!pipeline_wrapper_ready_out);
                    @(posedge clock);
                    fsb_packet_in.opcode    <= `SD NULL_PACKET;
                    fsb_packet_valid_in     <= `SD 0;

                    // Set the beamforming aperture size
                    fsb_packet_in.opcode                           <= `SD LD_BF_AP_SIZE;
                    fsb_packet_in.data[$clog2(MAX_BF_AP_SIZE)-1:0] <= `SD BF_AP_X; // take the lower IN_OUT_SAMPLE_BIT_WIDTH bits as the value
                    fsb_packet_valid_in                            <= `SD 1;
                    while(!pipeline_wrapper_ready_out);
                    @(posedge clock);
                    fsb_packet_in.opcode                           <= `SD NULL_PACKET;
                    fsb_packet_valid_in                            <= `SD 0;

                    // Set the apodization constants
                    fsb_packet_in.opcode <= `SD LD_APOD_CONSTS;
                    $display("Reading Apodization Constants... %d Cycles", clock_count);
                    $fdisplay(ppfile, "Reading Apodization Constants...");
                    apod_consts_filename = $fopen($psprintf("input_data/sv_stg%0d_apod_consts_fixed_point.data", stage_id), "rb"); // "rb" means reading binary
                    for(int j = 0; j < NUM_CHANNELS_X; j++) begin
                        for(int i = 0; i < (MAX_BF_AP_SIZE/`APOD_CONSTS_FILE_READ_WIDTH); i++) begin
                            for(int k = 0; k < `APOD_CONSTS_FILE_READ_WIDTH; k++) begin
                                code = $fscanf(apod_consts_filename,"%h", input_apod_consts[INPUT_BIT_WIDTH-1:INPUT_BIT_WIDTH/2]);
                                code = $fscanf(apod_consts_filename,"%h", input_apod_consts[(INPUT_BIT_WIDTH-1)/2:0]);
                                input_apod_consts_temp[k] = input_apod_consts[IN_OUT_SAMPLE_BIT_WIDTH-1:0];
                            end // for(int k = 0; k < `APOD_CONSTS_FILE_READ_WIDTH; k++)

                            fsb_packet_valid_in                                                            <= `SD 1;
                            fsb_packet_in.data[(IN_OUT_SAMPLE_BIT_WIDTH*`APOD_CONSTS_FILE_READ_WIDTH)-1:0] <= `SD input_apod_consts_temp;
                            while(!pipeline_wrapper_ready_out);
                            @(posedge clock);
                        end // for(int i = 0; i < MAX_BF_AP_SIZE; i++)
                    end
                    $fclose(apod_consts_filename); // once reading is finished, close the file
                    $display("Done Reading Apodization Constants. %d Cycles\n", clock_count);
                    $fdisplay(ppfile, "Done Reading Apodization Constants.");
                    fsb_packet_in.opcode <= `SD NULL_PACKET;

                    // Set the tau_tx1 constants (initial offsets in index)
                    $display("Reading tau_tx1 Constants... %d Cycles", clock_count);
                    $fdisplay(ppfile, "Reading tau_tx1 Constants...");
                    tau_tx1_filename = $fopen($psprintf("input_data/sv_firing_%0d_stg%0d_tau_tx1_SLICE_%0d.data", firing_id, stage_id, slice_id), "rb"); // "rb" means reading binary
                    fsb_packet_in.opcode <= `SD LD_TAU_TX1;
                    for(int i = 0; i < NUM_CHANNELS_X; i++) begin
                        idx = i + 0; // **NOTE: 22 BECAUSE THAT'S THE PIPELINE PROCESSING SCANLINE 0
                        for(int j = 0; j < (MAX_BF_AP_SIZE/`TAU_TX1_FILE_READ_WIDTH); j++) begin
                            for(int k = 0; k < `TAU_TX1_FILE_READ_WIDTH; k++) begin
                                code = $fscanf(tau_tx1_filename,"%h", input_tau_tx1[INPUT_BIT_WIDTH-1:INPUT_BIT_WIDTH/2]);
                                code = $fscanf(tau_tx1_filename,"%h", input_tau_tx1[(INPUT_BIT_WIDTH-1)/2:0]);
                                input_tau_tx1_temp[k] = input_tau_tx1[$clog2(TAU_TX1_MAX_VAL)-1:0];

                                if(tau_rx_tx2_sram_rd_delay > (input_tau_tx1*4)) begin
                                    tau_rx_tx2_sram_rd_delay = input_tau_tx1*4;
                                end // if(tau_rx_tx2_sram_rd_delay > (input_tau_tx1*4))
                            end // for(int k = 0; k < `TAU_TX1_FILE_READ_WIDTH; k++)

                            fsb_packet_valid_in                                                        <= `SD 1;
                            fsb_packet_in.data[($clog2(TAU_TX1_MAX_VAL)*`TAU_TX1_FILE_READ_WIDTH)-1:0] <= `SD input_tau_tx1_temp;
                            while(!pipeline_wrapper_ready_out);
                            @(posedge clock);
                        end // for(int j = 0; j < MAX_BF_AP_SIZE; j++)
                    end // for(int i = 0; i < NUM_CHANNELS_X; i++)
                    fsb_packet_in.opcode <= `SD NULL_PACKET;
                    fsb_packet_valid_in  <= `SD 0;
                    $fclose(tau_tx1_filename); // once reading is finished, close the file
                    $display("Final tau_rx_tx2 read delay: %d", tau_rx_tx2_sram_rd_delay);
                    $display("Done Reading tau_tx1 Constants. %d Cycles\n", clock_count);
                    $fdisplay(ppfile, "Done Reading tau_tx1 Constants.");

                    // Stream the tau_rx_tx2 bitstream to SRAM
                    $display("Streaming tau_rx_tx2 Constants to SRAM... %d Cycles", clock_count);
                    $fdisplay(ppfile, "Reading tau_rx_tx2 Constants to SRAM...");
                    tau_rx_tx2_filename = $fopen($psprintf("input_data/sv_firing_%0d_stg%0d_tau_rx_tx2.data", firing_id, stage_id), "rb"); // "rb" means reading binary
                    fsb_packet_in.opcode <= `SD LD_TAU_RX_TX2;
                    while(!$feof(tau_rx_tx2_filename)) begin // read line by line until an "end of file" is reached
                        // Read the next TAU_RX_TX2_FILE_READ_WIDTH tau_rx_tx2 values
                        for(int i = 0; i < `TAU_RX_TX2_FILE_READ_WIDTH; i++) begin
                            code = $fscanf(tau_rx_tx2_filename,"%h", input_tau_rx_tx2[i][INPUT_BIT_WIDTH-1:INPUT_BIT_WIDTH/2]);
                            code = $fscanf(tau_rx_tx2_filename,"%h", input_tau_rx_tx2[i][(INPUT_BIT_WIDTH-1)/2:0]);

                            // Assign the values to the signals
                            fsb_packet_in.data[i] <= `SD input_tau_rx_tx2[i][0];
                            // $fdisplay(testfile, "%b", input_tau_rx_tx2[i][0]);
                        end // for(int i = 0; i < `TAU_RX_TX2_FILE_READ_WIDTH; i++)

                        fsb_packet_valid_in <= `SD 1;
                        while(!pipeline_wrapper_ready_out);
                        @(posedge clock);
                    end
                    fsb_packet_in.opcode <= `SD NULL_PACKET;
                    fsb_packet_valid_in  <= `SD 0;
                    $fclose(tau_rx_tx2_filename); // once reading is finished, close the file
                    $display("tau_wr_max_addr: %d", pipeline_wrapper.tau_wr_max_addr);
                    $display("Done Streaming tau_rx_tx2 Constants to SRAM. %d Cycles\n", clock_count);
                    $fdisplay(ppfile, "Done Streaming tau_rx_tx2 Constants to SRAM.");

                    // Set the initial tau_rx_tx2 SRAM read delay
                    fsb_packet_in.opcode                            <= `SD LD_TAU_RX_TX2_INIT_DELAY;
                    fsb_packet_in.data[$clog2(TAU_TX1_MAX_VAL)-1:0] <= `SD tau_rx_tx2_sram_rd_delay;
                    fsb_packet_valid_in                             <= `SD 1;
                    while(!pipeline_wrapper_ready_out);
                    @(posedge clock);
                    fsb_packet_in.opcode                            <= `SD NULL_PACKET;
                    fsb_packet_valid_in                             <= `SD 0;

                    // Set the initial tau_rx_tx2 SRAM read delay
                    fsb_packet_in.opcode                           <= `SD LD_NUM_SHIFTS;
                    fsb_packet_in.data[$clog2(MAX_BF_AP_SIZE)-1:0] <= `SD NUM_SHIFTS-1;
                    fsb_packet_valid_in                            <= `SD 1;
                    while(!pipeline_wrapper_ready_out);
                    @(posedge clock);
                    fsb_packet_in.opcode                           <= `SD NULL_PACKET;
                    fsb_packet_valid_in                            <= `SD 0;

                    $display("Streaming rx_signal Data to SRAM... %d Cycles", clock_count);
                    $fdisplay(ppfile, "Streaming rx_signal Data to SRAM...");
                    rx_signal_filename = $fopen($psprintf("input_data/sv_firing_%0d_stg%0d_input_data_SLICE_%0d.data", firing_id, stage_id, slice_id), "rb"); // "rb" means reading binary
                    fsb_packet_in.opcode <= `SD LD_INPUT_DATA;
                    while(!$feof(rx_signal_filename)) begin // read line by line until an "end of file" is reached
                        for(int i = 0; i < (NUM_CHANNELS_X/`INPUT_DATA_FILE_READ_WIDTH); i++) begin
                            for(int j = 0; j < `INPUT_DATA_FILE_READ_WIDTH; j++) begin
                                // Read the next rx_signal value
                                code = $fscanf(rx_signal_filename,"%h", input_rx_signal[INPUT_BIT_WIDTH-1:INPUT_BIT_WIDTH/2]);
                                code = $fscanf(rx_signal_filename,"%h", input_rx_signal[(INPUT_BIT_WIDTH-1)/2:0]);
                                input_rx_signal_temp[j] = input_rx_signal[IN_OUT_SAMPLE_BIT_WIDTH-1:0];
                                // $display("%h", input_rx_signal);
                            end // for(j = 0; j < `INPUT_DATA_FILE_READ_WIDTH; j++)

                            // Assign the values to the signals
                            fsb_packet_valid_in                                                           <= `SD 1;
                            fsb_packet_in.data[(IN_OUT_SAMPLE_BIT_WIDTH*`INPUT_DATA_FILE_READ_WIDTH)-1:0] <= `SD input_rx_signal_temp; // take the lower IN_OUT_SAMPLE_BIT_WIDTH bits as the value
                            while(!pipeline_wrapper_ready_out);
                            @(posedge clock);
                        end // for(int i = 0; i < NUM_CHANNELS_X; i++)
                    end // while(!(rx_signal_filename))
                    fsb_packet_in.opcode <= `SD NULL_PACKET;
                    fsb_packet_valid_in  <= `SD 0;
                    $fclose(rx_signal_filename); // once reading is finished, close the file
                    $display("Done Streaming rx_signal Data to SRAM. %d Cycles\n", clock_count);
                    $fdisplay(ppfile, "Done Streaming rx_signal Data to SRAM.");

                    // Initialize the pipelines
                    fsb_packet_in.opcode <= `SD INIT_PIPELINES;
                    fsb_packet_valid_in  <= `SD 1;
                    while(!pipeline_wrapper_ready_out);
                    @(posedge clock);
                    fsb_packet_in.opcode <= `SD NULL_PACKET;
                    fsb_packet_valid_in  <= `SD 0;

                    $display("@@\n@@\nConstant Initialization Complete...Initiating Data Stream...... %d Cycles\n@@\n@@\n", clock_count);

                    // Run the pipelines
                    fsb_packet_in.opcode <= `SD RUN_PIPELINES;
                    fsb_packet_valid_in  <= `SD 1;
                    while(!pipeline_wrapper_ready_out);
                    @(posedge clock);
                    fsb_packet_in.opcode <= `SD NULL_PACKET;
                    fsb_packet_valid_in  <= `SD 0;

                    fsb2_ready_in <= `SD 1;
                    while(~fsb_packet_valid_out) begin
                        @(posedge clock);
                    end // while(~fsb_packet_valid_out)

                    $display("@@\n@@\nData Stream Complete...Initiating Data Read...... %d Cycles\n@@\n@@\n", clock_count);

                    while(fsb_packet_valid_out) begin
                        if(fsb_packet_out.opcode == READ_OUTPUT) begin
                            // $fdisplay(testfile, "%d", $signed(fsb_packet_out.data[IN_OUT_SAMPLE_BIT_WIDTH-1:0]));
                        end // if(fsb_packet_out.opcode == READ_OUTPUT)
                        @(posedge clock);
                    end // while(fsb_packet_valid_out)

                    $display("output_wr_max_addr: %d", pipeline_wrapper.output_wr_max_addr[0]);
                    $display("output_rd_max_addr: %d", pipeline_wrapper.output_rd_max_addr[0]);
                    
                    $display("@@\n@@\nData Read Complete...... %d Cycles\n@@\n@@\n", clock_count);

                    @(posedge clock);

                    $display("\n@@@\nOutput should be invalid from this point\n@@@\n");

                    // The output shouldn't be valid after this point
                    for(int i = 0; i < 5; i++) begin
                        @(posedge clock);
                    end

                    for(int i = 0; i < MAX_BF_AP_SIZE; i++) begin
                        $display("apod[23].consts[%d]: %d", i, pipeline_wrap.pipeline_0.pd[23].apod_consts[i]);
                    end // for(int i = 0; i < BF_AP_X; i++)

                    for(int i = 0; i < NUM_CHANNELS_X; i++) begin
                        $display("output_wr_max_addr[%d]: %d", i, pipeline_wrapper.output_wr_max_addr[i]);
                    end // for(int i = 0; i < NUM_CHANNELS_X; i++)

                    $display("rotate[22].bf_ap_size: %d", pipeline_wrap.pipeline_0.rtt[22].bf_ap_size);
                    $display("select[22].bf_ap_size: %d", pipeline_wrap.pipeline_0.slct[22].bf_ap_size);
                    $display("regfile[22].bf_ap_size: %d", pipeline_wrap.pipeline_0.rgfl[22].bf_ap_size);
                end // for(int firing_id = LOOP_NUM_FIRINGS_START; firing_id <= LOOP_NUM_FIRINGS_END; firing_id++) begin
            end // for(int stage_id = LOOP_NUM_STAGES_START; stage_id <= LOOP_NUM_STAGES_END; stage_id++)
        end // for(int firing_id = LOOP_NUM_SLICES_START; firing_id <= LOOP_NUM_SLICES_END; firing_id++)

        // Finished
        $toggle_stop;
        $toggle_report("WRAPPER.saif", 1.0e-9, pipeline_wrap);
        
        $fclose(ppfile);
        $fclose(testfile);
        $fclose(testfile2);
        $fclose(testfile3);
        $display("@@@DONE: %d Cycles", clock_count);
        $finish;

    end

endmodule
