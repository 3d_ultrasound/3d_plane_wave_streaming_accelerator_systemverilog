/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  testbench.sv                                        //
//                                                                     //
//  Description :  Testbench for beamforming pipeline; this reads      //
//                 data files and distributes data to various modules. //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

`timescale 1ns/100ps

// Add your testbench here

// extern void print_header(string str);
// extern void print_cycles();
// extern void print_stage(string div, int inst, int npc, int valid_inst);
// extern void print_reg(int wb_reg_wr_select_out_hi, int wb_reg_wr_select_out_lo,
//                       int wb_reg_wr_idx_out, int wb_reg_wr_en_out);
// extern void print_membus(int proc2mem_command, int mem2proc_response,
//                          int proc2mem_addr_hi, int proc2mem_addr_lo,
//                          int proc2mem_data_hi, int proc2mem_data_lo);
// extern void print_close();

module testbench;

    // Parameters
    parameter SAMPLE_BIT_WIDTH = 32;
    parameter MAX_BF_AP_SIZE = 32;
    parameter REG_FILE_X = 8;
    parameter REG_FILE_Y = 4;
    parameter NUM_BANKS = 4;
    parameter NUM_BANK_ENTRIES = ((REG_FILE_X*REG_FILE_Y)/NUM_BANKS);
    parameter BITSTREAM_BUFFER_LEN = 64;
    parameter TAU_TX1_MAX_VAL = BITSTREAM_BUFFER_LEN-4;

    // Testbench Signals
    logic        clock;
    logic        reset;
    logic [31:0] clock_count;

    // Module Signals
    // Inputs
    logic [SAMPLE_BIT_WIDTH-1:0]                     regfile_in;
    logic                                            regfile_en_in;
    logic                                            regfile_bf_ap_done_in;
    logic [MAX_BF_AP_SIZE-1:0][SAMPLE_BIT_WIDTH-1:0] apod_consts_in;

    // Outputs
    logic [SAMPLE_BIT_WIDTH-1:0] apod_out;
    logic                        apod_en_out;
    logic                        apod_bf_ap_done_out;



    logic [63:0] chk_value;
    logic [15:0] test_counter;

    // Instantiate the isr units
    apod #(.SAMPLE_BIT_WIDTH(SAMPLE_BIT_WIDTH), .MAX_BF_AP_SIZE(MAX_BF_AP_SIZE)) pd (
        // Inputs
        .clock(clock),
        .reset(reset),
        .regfile_in(regfile_in),
        .regfile_en_in(regfile_en_in),
        .regfile_bf_ap_done_in(regfile_bf_ap_done_in),
        .apod_consts_in(apod_consts_in),

        // Outputs
        .apod_out(apod_out),
        .apod_en_out(apod_en_out),
        .apod_bf_ap_done_out(apod_bf_ap_done_out)
    );

    // Generate System Clock
    always begin
        #(`VERILOG_CLOCK_PERIOD/2.0);
        clock = ~clock;
    end

    // Count the number of posedges and number of instructions completed
    // till simulation ends
    always @(posedge clock) begin
        if(reset) begin
            clock_count <= `SD 0;
            // instr_count <= `SD 0;
        end else begin
            clock_count <= `SD (clock_count + 1);
            // instr_count <= `SD (instr_count + pipeline_completed_insts);
        end
    end

    // Print the output
    always @(posedge clock) begin
        // if(rotate_en_in) begin
        //     $display("Cycles: %d, Input EN: %d, Val: %d", clock_count, rotate_en_in, rotate_in);
        // end
        // if(select_en_out) begin
        //     $display("Cycles: %d, Output NUM: %d, Val: %d, %d, %d, %d", clock_count, select_num_out, select_out[0], select_out[1], select_out[2], select_out[3]);
        //     // $display("%d", select_out[0]);
        //     // $display("%d", select_out[1]);
        //     // $display("%d", select_out[2]);
        //     // $display("%d", select_out[3]);

        //     // data_check[0] = (datum[0]/4) + (datum[1]*3/4);
        //     // data_check[1] = (datum[0]/2) + (datum[1]/2);
        //     // data_check[2] = (datum[0]*3/4) + (datum[1]/4);
        //     // data_check[3] = datum[0];

        //     $display("tau_rx_tx2_next: %b", slct.tau_rx_tx2_next);
        //     // result_check(datum, data_check, select_out);
        // end
        // $display("Cycles: %d, bf_ap_idx: %d, col_tail: %d, reg_val_en_in: %b, wr_bank_idx[0]: %d, wr_idx[0]: %d", clock_count, rgfl.bf_ap_idx, rgfl.col_tail[0], rgfl.reg_val_en_in, rgfl.wr_bank_idx[0], rgfl.wr_idx[0]);
        // $display("Cycles: %d, bf_ap_idx: %d, reg_val_en_in: %b, col_tail[0]: %d", clock_count, rgfl.bf_ap_idx, rgfl.reg_val_en_in, rgfl.col_tail[0]);
        // $display("wr_bank_idx_temp[0]:%d", rgfl.wr_bank_idx_temp[0]);
        // $display("wr_bank_idx_temp[1]:%d", rgfl.wr_bank_idx_temp[1]);
        // $display("wr_bank_idx_temp[2]:%d", rgfl.wr_bank_idx_temp[2]);
        // $display("wr_bank_idx_temp[3]:%d", rgfl.wr_bank_idx_temp[3]);
        // $display("wr_en[0]:%d, wr_idx[0]:%d, wr_data[0]:%d", rgfl.wr_en[0], rgfl.wr_idx[0], rgfl.wr_data[0]);
        // $display("wr_en[1]:%d, wr_idx[1]:%d, wr_data[1]:%d", rgfl.wr_en[1], rgfl.wr_idx[1], rgfl.wr_data[1]);
        // $display("wr_en[2]:%d, wr_idx[2]:%d, wr_data[2]:%d", rgfl.wr_en[2], rgfl.wr_idx[2], rgfl.wr_data[2]);
        // $display("wr_en[3]:%d, wr_idx[3]:%d, wr_data[3]:%d", rgfl.wr_en[3], rgfl.wr_idx[3], rgfl.wr_data[3]);
        $display("Cycles: %d, regfile_en_in:%d, regfile_in:%d, regfile_bf_ap_done_in:%d, bf_ap_idx:%d", clock_count, regfile_en_in, regfile_in, regfile_bf_ap_done_in, pd.bf_ap_idx);
        $display("Cycles: %d, apod_en_out:%d, apod_out:%d, apod_bf_ap_done_out:%d", clock_count, apod_en_out, apod_out, apod_bf_ap_done_out);
    end

    initial begin
        // $dumpvars(0,rgfl);
        // $dumpfile("test.vcd");
        // $dumpon;

        // $monitor("Time:%4.0f reset:%b rotate_in:%d select_out:%d", $time, reset, rotate_in, select_out);
        
        // Initialize inputs
        clock <= `SD 1'b0;
        reset <= `SD 1'b0;

        regfile_in     <= `SD 1;
        regfile_en_in  <= `SD 0;
        regfile_bf_ap_done_in <= `SD 0;
        for(int i = 0; i < MAX_BF_AP_SIZE; i++) begin
            apod_consts_in[i] <= `SD i;
        end // for(int i = 0; i < MAX_BF_AP_SIZE; i++)

        // Pulse the reset signal
        // $display("@@\n@@\n@@  %t  Asserting System reset......", $realtime);
        $display("@@\n@@\n@@Asserting System reset......");
        reset <= `SD 1'b1;
        @(posedge clock);
        @(posedge clock);
        
        // This reset is at an odd time to avoid the pos & neg clock edges
        reset <= `SD 1'b0;

        // reg_val_in[0]  <= `SD 1;
        // reg_val_in[1]  <= `SD 2;
        // reg_val_in[2]  <= `SD 3;
        // reg_val_in[3]  <= `SD 4;
        // reg_val_en_in  <= `SD 4'b0111;
        // bf_ap_idx      <= `SD 0;
        
        @(posedge clock);

        regfile_en_in     <= `SD 1;

        for(int i = 0; i < 10; i++) begin
            // regfile_in     <= `SD i;
            regfile_bf_ap_done_in <= `SD (((i + 1) % 3) == 0) ? 1 : 0;
            @(posedge clock);
        end

        // for(int i = 0; i < 5; i++) begin
        //     @(posedge clock);
        // end

        // reg_val_in[0]  <= `SD 5;
        // reg_val_in[1]  <= `SD 6;
        // reg_val_in[2]  <= `SD 7;
        // reg_val_in[3]  <= `SD 8;
        // reg_val_en_in  <= `SD 4'b0111;

        // for(int i = 0; i < bf_ap_size_in; i++) begin
        //     bf_ap_idx <= `SD i;
        //     @(posedge clock);
        // end

        // reg_val_en_in  <= `SD 4'b0000;

        for(int i = 0; i < 10; i++) begin
            // bf_ap_idx <= `SD i;
            @(posedge clock);
        end

        @(posedge clock);

        for(int i = 0; i < 10; i++) begin
            @(posedge clock);
        end

        // tau_rx_tx2_en_in <= `SD 1;
        // tau_rx_tx2_in <= `SD 4'b0110;
        // tau_rx_tx2_in <= `SD 4'b1111;

        @(posedge clock);
        @(posedge clock);


        $display("@@@DONE\n");
        
        $finish;
    end

endmodule
