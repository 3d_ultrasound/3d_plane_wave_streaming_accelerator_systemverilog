/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  apod_v2_wrap.sv                                     //
//                                                                     //
//  Description :  Apodizes selected samples.                          //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module apod_v2_wrap #(parameter SAMPLE_BIT_WIDTH = 12, parameter PIPELINE_SAMPLE_BIT_WIDTH = 10, parameter MAX_BF_AP_SIZE = 32, parameter NUM_BANKS = 4) (

        // Inputs
        input                                            clock,
        input                                            reset,
        input [3:0][PIPELINE_SAMPLE_BIT_WIDTH-1:0]       select_in,
        input [3:0]                                      select_en_in,
        input [$clog2(MAX_BF_AP_SIZE)-1:0]               select_bf_ap_idx_in,
        input [MAX_BF_AP_SIZE-1:0][SAMPLE_BIT_WIDTH-1:0] apod_consts_in,

        // Outputs
        output logic [3:0][PIPELINE_SAMPLE_BIT_WIDTH-1:0] apod_v2_wrap_out,
        output logic [3:0]                                apod_v2_wrap_en_out,
        output logic [$clog2(MAX_BF_AP_SIZE)-1:0]         apod_v2_wrap_bf_ap_idx_out

    );

    // Local variables
    logic [MAX_BF_AP_SIZE-1:0][SAMPLE_BIT_WIDTH-1:0] apod_consts; // holds the bf aperture size
    logic [3:0][PIPELINE_SAMPLE_BIT_WIDTH-1:0]       select_saved, select_saved_next; // holds the apodized value; output logic is PIPELINE_SAMPLE_BIT_WIDTH+11 bits, then lose 11 LSB
    logic [3:0]                                      select_en_saved, select_en_saved_next; // holds the apodized value's enable bit
    logic [$clog2(MAX_BF_AP_SIZE)-1:0]               select_bf_ap_idx_saved, select_bf_ap_idx_saved_next;

    logic [3:0][PIPELINE_SAMPLE_BIT_WIDTH-1:0] apod_v2_wrap, apod_v2_wrap_next;
    logic [3:0]                                apod_v2_wrap_en, apod_v2_wrap_en_next;
    logic [$clog2(MAX_BF_AP_SIZE)-1:0]         apod_v2_wrap_bf_ap_idx, apod_v2_wrap_bf_ap_idx_next, apod_v2_wrap_bf_ap_idx_2, apod_v2_wrap_bf_ap_idx_2_next, apod_v2_wrap_bf_ap_idx_3, apod_v2_wrap_bf_ap_idx_3_next;
    logic [SAMPLE_BIT_WIDTH-1:0]               current_apod_const, current_apod_const_next;

    // Assign outputs
    assign apod_v2_wrap_out           = apod_v2_wrap;
    assign apod_v2_wrap_en_out        = apod_v2_wrap_en;
    assign apod_v2_wrap_bf_ap_idx_out = apod_v2_wrap_bf_ap_idx_3;

    apod_v2 #(.SAMPLE_BIT_WIDTH(SAMPLE_BIT_WIDTH), .PIPELINE_SAMPLE_BIT_WIDTH(PIPELINE_SAMPLE_BIT_WIDTH), .MAX_BF_AP_SIZE(MAX_BF_AP_SIZE)) apd_0 [NUM_BANKS-1:0] (

        // Inputs
        .clock(clock),
        .reset(reset),
        .regfile_in(select_saved),
        .regfile_en_in(select_en_saved),
        .select_bf_ap_idx_in(select_bf_ap_idx_saved),
        .current_apod_const_in(current_apod_const),

        // Outputs
        .apod_v2_out(apod_v2_wrap_next),
        .apod_v2_en_out(apod_v2_wrap_en_next)

    );

    always_comb begin
        select_saved_next             = select_in;
        select_en_saved_next          = select_en_in;
        select_bf_ap_idx_saved_next   = select_bf_ap_idx_in;
        apod_v2_wrap_bf_ap_idx_next   = select_bf_ap_idx_saved;
        apod_v2_wrap_bf_ap_idx_2_next = apod_v2_wrap_bf_ap_idx;
        apod_v2_wrap_bf_ap_idx_3_next = apod_v2_wrap_bf_ap_idx_2;

        current_apod_const_next       = apod_consts[select_bf_ap_idx_in];
    end // always_comb

    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(reset) begin
            apod_consts              <= `SD apod_consts_in; // initialize the apodization constant array
            select_saved             <= `SD 0;
            select_en_saved          <= `SD 0;
            select_bf_ap_idx_saved   <= `SD 0;
            apod_v2_wrap             <= `SD 0;
            apod_v2_wrap_en          <= `SD 0;
            apod_v2_wrap_bf_ap_idx   <= `SD 0;
            apod_v2_wrap_bf_ap_idx_2 <= `SD 0;
            apod_v2_wrap_bf_ap_idx_3 <= `SD 0;
            
            current_apod_const       <= `SD 0;
        end else begin
            select_saved             <= `SD select_saved_next;
            select_en_saved          <= `SD select_en_saved_next;
            select_bf_ap_idx_saved   <= `SD select_bf_ap_idx_saved_next;
            apod_v2_wrap             <= `SD apod_v2_wrap_next;
            apod_v2_wrap_en          <= `SD apod_v2_wrap_en_next;
            apod_v2_wrap_bf_ap_idx   <= `SD apod_v2_wrap_bf_ap_idx_next;
            apod_v2_wrap_bf_ap_idx_2 <= `SD apod_v2_wrap_bf_ap_idx_2_next;
            apod_v2_wrap_bf_ap_idx_3 <= `SD apod_v2_wrap_bf_ap_idx_3_next;

            current_apod_const       <= `SD current_apod_const_next;
        end // if(reset) begin
    end // always_ff @(posedge clock)
endmodule // apod_v2_wrap
