/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  apod.sv                                             //
//                                                                     //
//  Description :  Apodizes selected samples.                          //
//                                                                     //
//  FIX :  If constant array access is pipelined, can meet 0.78ns      //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module apod #(parameter SAMPLE_BIT_WIDTH = 12, parameter MAX_BF_AP_SIZE = 32) (

        // Inputs
        input                                            clock,
        input                                            reset,
        input [SAMPLE_BIT_WIDTH-1:0]                     regfile_in,
        input                                            regfile_en_in,
        input                                            regfile_bf_ap_done_in,
        input [MAX_BF_AP_SIZE-1:0][SAMPLE_BIT_WIDTH-1:0] apod_consts_in,

        // Outputs
        output logic [SAMPLE_BIT_WIDTH-1:0] apod_out,
        output logic                        apod_en_out,
        output logic                        apod_bf_ap_done_out

    );

    // Local variables
    logic [SAMPLE_BIT_WIDTH+11-1:0]                  buffer, buffer_next; // holds the apodized value; output logic is SAMPLE_BIT_WIDTH+11 bits, then lose 11 LSB
    logic                                            buffer_en, buffer_en_next; // holds the apodized value's enable bit
    logic [$clog2(MAX_BF_AP_SIZE)-1:0]               bf_ap_idx, bf_ap_idx_next; // holds the output value after accumulation
    logic                                            bf_ap_done, bf_ap_done_next; // signals that the output value is ready
    logic [MAX_BF_AP_SIZE-1:0][SAMPLE_BIT_WIDTH-1:0] apod_consts_arr; // holds the bf aperture size
    logic [SAMPLE_BIT_WIDTH-1:0]                     apod_const; // holds the bf aperture size

    // Assign outputs
    assign apod_out            = buffer[SAMPLE_BIT_WIDTH+11-1:11];
    assign apod_en_out         = buffer_en;
    assign apod_bf_ap_done_out = bf_ap_done;

    always_comb begin
        buffer_next     = buffer;
        bf_ap_idx_next  = bf_ap_idx;
        bf_ap_done_next = regfile_bf_ap_done_in;
        buffer_en_next  = 0;

        // Add sample to the accumulator
        if(regfile_en_in) begin
            // buffer_next    = apod_consts_arr[bf_ap_idx] * regfile_in;
            buffer_next    = $signed(apod_const) * $signed(regfile_in);
            // buffer_next    = $signed(regfile_in) * $signed(regfile_in);
            // buffer_next    = $signed($signed(apod_consts_arr[bf_ap_idx]) * $signed(regfile_in));
            bf_ap_idx_next = bf_ap_idx + 1;

            buffer_en_next = 1;
        end // if(regfile_en_in)

        if(regfile_bf_ap_done_in) begin
            bf_ap_idx_next = 0;
        end // if(regfile_bf_ap_done_in)
    end // always_comb

    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(reset) begin
            buffer          <= `SD 0;
            bf_ap_idx       <= `SD 0;
            bf_ap_done      <= `SD 0;
            buffer_en       <= `SD 0;
            apod_consts_arr <= `SD apod_consts_in; // initialize the apodization constant array
        end else begin
            bf_ap_idx       <= `SD bf_ap_idx_next;
            buffer          <= `SD buffer_next;
            buffer_en       <= `SD buffer_en_next;
            bf_ap_done      <= `SD bf_ap_done_next;
            
            apod_const      <= `SD apod_consts_arr[bf_ap_idx_next];
        end // if(reset) begin
    end // always_ff @(posedge clock)
endmodule // apod
