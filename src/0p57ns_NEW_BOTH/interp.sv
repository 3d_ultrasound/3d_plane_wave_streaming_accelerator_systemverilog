/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  interp.sv                                           //
//                                                                     //
//  Description :  Performs a 4x linear interpolation using latest     //
//                 two samples.                                        //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module interp #(parameter SAMPLE_BIT_WIDTH = 12) (

        // Inputs
        input                        clock,
        input                        reset,
        input [SAMPLE_BIT_WIDTH-1:0] sample_in,
        input                        sample_en_in,

        // Outputs
        output logic [3:0][SAMPLE_BIT_WIDTH-1:0] interp_out,
        output logic                             interp_en_out

    );

    // Local variables
    logic [3:0][SAMPLE_BIT_WIDTH-1:0] buffer, buffer_next; // holds the current value
    logic [3:0][SAMPLE_BIT_WIDTH-1:0] buffer_en, buffer_en_next; // holds the current value
    logic [3:0][SAMPLE_BIT_WIDTH-1:0] old_datum, old_datum_next; // holds the previous value

    // Assign outputs
    assign interp_out[0] = buffer[0] + old_datum[2]; // (sample_in * 1/4) + (old_sample * 3/4)
    assign interp_out[1] = buffer[1] + old_datum[1]; // (sample_in * 1/2) + (old_sample * 1/2)
    assign interp_out[2] = buffer[2] + old_datum[0]; // (sample_in * 3/4) + (old_sample * 1/4)
    assign interp_out[3] = buffer[3]; // sample_in
    assign interp_en_out = buffer_en;

    always_comb begin
        buffer_next    = buffer;
        buffer_en_next = sample_en_in;
        old_datum_next = old_datum;

        if(sample_en_in) begin
            // Load samples into the buffer
            // buffer_next[0] = (sample_in >>> 2); // sample_in * 1/4
            // buffer_next[1] = (sample_in >>> 1); // sample_in * 1/2
            // buffer_next[2] = ((sample_in + 1) >>> 2) + (sample_in >>> 1); // sample_in * 3/4 (rounded DOWN) - NOTE: this is the critical path for clock period (3 serial operations)
            buffer_next[0] = ($signed(sample_in) >>> 2); // sample_in * 1/4
            buffer_next[1] = ($signed(sample_in) >>> 1); // sample_in * 1/2
            buffer_next[2] = (($signed(sample_in) + 1) >>> 2) + ($signed(sample_in) >>> 1); // sample_in * 3/4 (rounded DOWN) - NOTE: this is the critical path for clock period (3 serial operations)
            // buffer_next[2] = sample_in - (sample_in >> 2); // sample_in * 3/4 (rounded UP)
            buffer_next[3] = sample_in; // sample_in

            // Save the last set of values
            old_datum_next = buffer;
        end // if(sample_en_in)
    end // always_comb

    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(reset) begin
            buffer    <= `SD 0;
            buffer_en <= `SD 0;
            old_datum <= `SD 0;
        end else begin
            buffer    <= `SD buffer_next;
            buffer_en <= `SD buffer_en_next;
            old_datum <= `SD old_datum_next;
        end // if(reset) begin
    end // always_ff @(posedge clock)
endmodule // interp
