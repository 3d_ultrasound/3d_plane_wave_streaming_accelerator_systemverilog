/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  pipeline.sv                                         //
//                                                                     //
//  Description :  Top-level module of the beamforming pipeline;       //
//                 this instantiates and connects the 6 components of  //
//                 the beamforming pipeline togeather.                 //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module pipeline #(parameter SAMPLE_BIT_WIDTH = 12, parameter BITSTREAM_BUFFER_LEN = 1024, parameter TAU_TX1_MAX_VAL = BITSTREAM_BUFFER_LEN-4, parameter MAX_BF_AP_SIZE = 32, parameter REG_FILE_X = 32, parameter REG_FILE_Y = 32, parameter NUM_BANKS = 4, parameter NUM_BANK_ENTRIES = ((REG_FILE_X*REG_FILE_Y)/NUM_BANKS), parameter NUM_STAGES = 8, parameter NUM_CHANNELS_X = 32) (

        // Inputs
        input                                                                clock,
        input                                                                reset,
        input [NUM_CHANNELS_X-1:0]                                           data_en_in,
        input [NUM_CHANNELS_X-1:0][SAMPLE_BIT_WIDTH-1:0]                     data_in,
        input [NUM_CHANNELS_X-1:0][$clog2(TAU_TX1_MAX_VAL)-1:0]              data_tau_tx1_in,
        input [NUM_CHANNELS_X-1:0][3:0]                                      data_tau_rx_tx2_in,
        input [NUM_CHANNELS_X-1:0]                                           data_tau_rx_tx2_en_in,
        input [NUM_CHANNELS_X-1:0][$clog2(MAX_BF_AP_SIZE)-1:0]               data_bf_ap_size_in,
        input [NUM_CHANNELS_X-1:0][MAX_BF_AP_SIZE-1:0][SAMPLE_BIT_WIDTH-1:0] data_apod_consts_in,

        // Outputs
        output logic [NUM_CHANNELS_X-1:0][SAMPLE_BIT_WIDTH-1:0] data_out,
        output logic [NUM_CHANNELS_X-1:0]                       data_en_out

        // testing hooks (these must be exported so we can test
        // the synthesized version) data is tested by looking at
        // the final values in memory

    );


    // Signals

    // Sample -> Interp Signals
    logic [NUM_CHANNELS_X-1:0][SAMPLE_BIT_WIDTH-1:0] sample_out;
    logic [NUM_CHANNELS_X-1:0]                       sample_en_out;

    // Interp -> Rotate Signals
    logic [NUM_CHANNELS_X-1:0][3:0][SAMPLE_BIT_WIDTH-1:0] interp_out;
    logic [NUM_CHANNELS_X-1:0]                            interp_en_out;

    // Rotate Signals
    logic [NUM_CHANNELS_X-1:0][3:0][SAMPLE_BIT_WIDTH-1:0]  rotate_out;
    logic [NUM_CHANNELS_X-1:0]                             rotate_en_out;
    logic [NUM_CHANNELS_X-1:0][3:0][SAMPLE_BIT_WIDTH-1:0]  rotate_side_out;
    logic [NUM_CHANNELS_X-1:0]                             rotate_en_side_out;
    logic [NUM_CHANNELS_X-1:0][$clog2(MAX_BF_AP_SIZE)-1:0] rotate_bf_ap_idx_out;

    // Select Signals
    logic [NUM_CHANNELS_X-1:0][$clog2(MAX_BF_AP_SIZE)-1:0] select_bf_ap_idx_out;
    logic [NUM_CHANNELS_X-1:0][3:0][SAMPLE_BIT_WIDTH-1:0]  select_out;
    logic [NUM_CHANNELS_X-1:0][3:0]                        select_en_out;

    // Regfile Signals
    logic [NUM_CHANNELS_X-1:0][SAMPLE_BIT_WIDTH-1:0] regfile_out;
    logic [NUM_CHANNELS_X-1:0]                       regfile_en_out;
    logic [NUM_CHANNELS_X-1:0]                       regfile_bf_ap_done_out;

    // Apod Signals
    logic [NUM_CHANNELS_X-1:0][SAMPLE_BIT_WIDTH-1:0] apod_out;
    logic [NUM_CHANNELS_X-1:0]                       apod_en_out;
    logic [NUM_CHANNELS_X-1:0]                       apod_bf_ap_done_out;



    // Sample Module
    sample #(.SAMPLE_BIT_WIDTH(SAMPLE_BIT_WIDTH)) smpl [(NUM_CHANNELS_X - 1):0] (

        // Inputs
        .clock(clock),
        .reset(reset),
        .adc_in(data_in),
        .adc_en_in(data_en_in),

        // Outputs
        .sample_out(sample_out),
        .sample_en_out(sample_en_out)

    );


    // Interpolate Module
    interp #(.SAMPLE_BIT_WIDTH(SAMPLE_BIT_WIDTH)) ntrp [(NUM_CHANNELS_X - 1):0] (

        // Inputs
        .clock(clock),
        .reset(reset),
        .sample_in(sample_out),
        .sample_en_in(sample_en_out),

        // Outputs
        .interp_out(interp_out),
        .interp_en_out(interp_en_out)

    );


    // Rotate Module
    rotate #(.SAMPLE_BIT_WIDTH(SAMPLE_BIT_WIDTH), .MAX_BF_AP_SIZE(MAX_BF_AP_SIZE)) rtt [(NUM_CHANNELS_X - 1):0] (

        // Inputs
        .clock(clock),
        .reset(reset),
        .interp_in(interp_out),
        .interp_en_in(interp_en_out),
        .rotate_side_in({rotate_side_out[0],rotate_side_out[NUM_CHANNELS_X-1:1]}),
        .rotate_en_side_in({rotate_en_side_out[0],rotate_en_side_out[NUM_CHANNELS_X-1:1]}),
        // .rotate_side_in({rotate_side_out[NUM_CHANNELS_X-2:0],rotate_side_out[NUM_CHANNELS_X-1]}),
        // .rotate_en_side_in({rotate_en_side_out[NUM_CHANNELS_X-2:0],rotate_en_side_out[NUM_CHANNELS_X-1]}),
        .bf_ap_size_in(data_bf_ap_size_in),

        // Outputs
        .rotate_side_out(rotate_side_out),
        .rotate_en_side_out(rotate_en_side_out),
        .rotate_out(rotate_out),
        .rotate_en_out(rotate_en_out),
        .rotate_bf_ap_idx_out(rotate_bf_ap_idx_out)

    );


    // Select Module
    select #(.SAMPLE_BIT_WIDTH(SAMPLE_BIT_WIDTH), .MAX_BF_AP_SIZE(MAX_BF_AP_SIZE), .BITSTREAM_BUFFER_LEN(BITSTREAM_BUFFER_LEN), .TAU_TX1_MAX_VAL(TAU_TX1_MAX_VAL)) slct [(NUM_CHANNELS_X - 1):0] (

        // Inputs
        .clock(clock),
        .reset(reset),
        .rotate_in(rotate_out),
        .rotate_en_in(rotate_en_out),
        .tau_tx1_in(data_tau_tx1_in),
        .tau_rx_tx2_in(data_tau_rx_tx2_in),
        .tau_rx_tx2_en_in(data_tau_rx_tx2_en_in),
        .rotate_bf_ap_idx_in(rotate_bf_ap_idx_out),

        // Outputs
        .select_out(select_out),
        .select_en_out(select_en_out),
        .select_bf_ap_idx_out(select_bf_ap_idx_out)

    );


    // Regfile
    regfile #(.SAMPLE_BIT_WIDTH(SAMPLE_BIT_WIDTH), .MAX_BF_AP_SIZE(MAX_BF_AP_SIZE), .REG_FILE_X(REG_FILE_X), .REG_FILE_Y(REG_FILE_Y), .NUM_BANKS(NUM_BANKS), .NUM_BANK_ENTRIES(NUM_BANK_ENTRIES)) rgfl [(NUM_CHANNELS_X - 1):0] (

        // Inputs
        .clock(clock),
        .reset(reset),
        .select_in(select_out),
        .select_en_in(select_en_out),
        .select_bf_ap_idx_in(select_bf_ap_idx_out),
        .bf_ap_size_in(data_bf_ap_size_in),

        // Outputs
        .regfile_out(regfile_out),
        .regfile_en_out(regfile_en_out),
        .regfile_bf_ap_done_out(regfile_bf_ap_done_out)

    );


    // Apodize Module
    apod #(.SAMPLE_BIT_WIDTH(SAMPLE_BIT_WIDTH), .MAX_BF_AP_SIZE(MAX_BF_AP_SIZE)) pd [(NUM_CHANNELS_X - 1):0] (
    // apod #(.SAMPLE_BIT_WIDTH(SAMPLE_BIT_WIDTH), .MAX_BF_AP_SIZE(MAX_BF_AP_SIZE), .NUM_STAGES(NUM_STAGES)) pd [(NUM_CHANNELS_X - 1):0] (

        // Inputs
        .clock(clock),
        .reset(reset),
        .regfile_in(regfile_out),
        .regfile_en_in(regfile_en_out),
        .regfile_bf_ap_done_in(regfile_bf_ap_done_out),
        .apod_consts_in(data_apod_consts_in),

        // Outputs
        .apod_out(apod_out),
        .apod_en_out(apod_en_out),
        .apod_bf_ap_done_out(apod_bf_ap_done_out)

    );


    // Accumulate Module
    accum #(.SAMPLE_BIT_WIDTH(SAMPLE_BIT_WIDTH), .MAX_BF_AP_SIZE(MAX_BF_AP_SIZE)) ccm [(NUM_CHANNELS_X - 1):0] (

        // Inputs
        .clock(clock),
        .reset(reset),
        .apod_in(apod_out),
        .apod_en_in(apod_en_out),
        .apod_bf_ap_done_in(apod_bf_ap_done_out),

        // Outputs
        .accum_out(data_out),
        .accum_en_out(data_en_out)

    );


    // Output?

endmodule // pipeline
