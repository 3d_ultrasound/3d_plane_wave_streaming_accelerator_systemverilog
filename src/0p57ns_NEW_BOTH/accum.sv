/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  accum.sv                                            //
//                                                                     //
//  Description :  Accumulates samples after apodization.              //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module accum #(parameter SAMPLE_BIT_WIDTH = 12, parameter MAX_BF_AP_SIZE = 32) (

        // Inputs
        input                              clock,
        input                              reset,
        input [SAMPLE_BIT_WIDTH-1:0]       apod_in,
        input                              apod_en_in,
        input                              apod_bf_ap_done_in,

        // Outputs
        output logic [SAMPLE_BIT_WIDTH-1:0] accum_out,
        output logic                        accum_en_out

    );

    // Local variables
    logic [SAMPLE_BIT_WIDTH-1:0] buffer, buffer_next; // holds the value during accumulation
    logic [SAMPLE_BIT_WIDTH-1:0] out_val; // holds the output value after accumulation
    logic                        out_val_en; // signals that the output value is ready

    // Assign outputs
    assign accum_out    = out_val;
    assign accum_en_out = out_val_en;

    always_comb begin
        buffer_next = buffer;

        // Add sample to the accumulator
        if(apod_en_in) begin
            buffer_next = $signed(buffer) + $signed(apod_in);
        end // if(apod_en_in)
    end // always_comb

    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(reset) begin
            out_val    <= `SD 0;
            out_val_en <= `SD 0;

            buffer     <= `SD 0;
        end else begin
            if(apod_bf_ap_done_in) begin
                out_val    <= `SD  buffer_next;
                out_val_en <= `SD 1;

                buffer     <= `SD 0;
            end else begin
                out_val    <= `SD 0;
                out_val_en <= `SD 0;

                buffer     <= `SD buffer_next;
            end // if(apod_bf_ap_done_in)
        end // if(reset) begin
    end // always_ff @(posedge clock)
endmodule // accum
