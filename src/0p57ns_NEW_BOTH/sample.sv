/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  sample.sv                                           //
//                                                                     //
//  Description :  Samples the incoming ADC signal.                    //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module sample #(parameter SAMPLE_BIT_WIDTH = 12) (

        // Inputs
        input                        clock,
        input                        reset,
        input [SAMPLE_BIT_WIDTH-1:0] adc_in,
        input                        adc_en_in,

        // Outputs
        output logic [SAMPLE_BIT_WIDTH-1:0] sample_out,
        output logic                        sample_en_out

    );

    // Local variables
    logic [SAMPLE_BIT_WIDTH-1:0] buffer, buffer_next;
    logic                        buffer_en, buffer_en_next;

    // Assign outputs
    assign sample_out    = buffer;
    assign sample_en_out = buffer_en;

    always_comb begin
        buffer_next    = buffer;
        buffer_en_next = buffer_en;

        // Load samples into the buffer
        buffer_next    = adc_in;
        buffer_en_next = adc_en_in;
    end // always_comb

    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(reset) begin
            buffer    <= `SD 0;
            buffer_en <= `SD 0;
        end else begin
            buffer    <= `SD buffer_next;
            buffer_en <= `SD buffer_en_next;
        end // if(reset) begin
    end // always_ff @(posedge clock)
endmodule // sample
