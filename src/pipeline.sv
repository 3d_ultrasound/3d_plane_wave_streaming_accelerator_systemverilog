/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  pipeline.sv                                         //
//                                                                     //
//  Description :  Top-level module of the beamforming pipeline;       //
//                 this instantiates and connects the 6 components of  //
//                 the beamforming pipeline togeather.                 //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module pipeline #(parameter IN_OUT_SAMPLE_BIT_WIDTH = 12, parameter PIPELINE_SAMPLE_BIT_WIDTH = 10, parameter BITSTREAM_BUFFER_LEN = 64, parameter TAU_TX1_MAX_VAL = 1024, parameter MAX_BF_AP_SIZE = 32, parameter REG_FILE_X = 32, parameter REG_FILE_Y = 32, parameter NUM_BANKS = 4, parameter NUM_BANK_ENTRIES = ((REG_FILE_X*REG_FILE_Y)/NUM_BANKS), parameter MAC_BUFFER_ENTRIES = 96, parameter NUM_STAGES = 8, parameter NUM_CHANNELS_X = 32) (

        // Inputs
        input                                                                       clock,
        input                                                                       reset,
        input [NUM_CHANNELS_X-1:0][IN_OUT_SAMPLE_BIT_WIDTH-1:0]                     sample_in,
        input                                                                       sample_en_in,
        input [NUM_CHANNELS_X-1:0][MAX_BF_AP_SIZE-1:0][IN_OUT_SAMPLE_BIT_WIDTH-1:0] apod_consts_in,
        input [3:0]                                                                 tau_rx_tx2_in,
        input                                                                       tau_rx_tx2_en_in,
        input [NUM_CHANNELS_X-1:0][MAX_BF_AP_SIZE-1:0][$clog2(TAU_TX1_MAX_VAL)-1:0] data_tau_tx1_in,
        input [$clog2(MAX_BF_AP_SIZE)-1:0]                                          bf_ap_size_in,
        input [3:0]                                                                 output_select_in,

        // Outputs
        output logic [NUM_CHANNELS_X-1:0][IN_OUT_SAMPLE_BIT_WIDTH-1:0] data_out,
        output logic [NUM_CHANNELS_X-1:0]                              data_en_out

    );

    // Signals

    // Sample -> Interp Signals
    logic [NUM_CHANNELS_X-1:0][IN_OUT_SAMPLE_BIT_WIDTH-1:0] sample_out;
    logic [NUM_CHANNELS_X-1:0]                              sample_en_out;

    // Interp -> Rotate Signals
    logic [NUM_CHANNELS_X-1:0][3:0][PIPELINE_SAMPLE_BIT_WIDTH-1:0] interp_out;
    logic [NUM_CHANNELS_X-1:0]                                     interp_en_out;

    // Rotate Signals
    logic [NUM_CHANNELS_X-1:0][3:0][PIPELINE_SAMPLE_BIT_WIDTH-1:0] rotate_out;
    logic [NUM_CHANNELS_X-1:0]                                     rotate_en_out;
    logic [NUM_CHANNELS_X-1:0][3:0][PIPELINE_SAMPLE_BIT_WIDTH-1:0] rotate_side_out;
    logic [NUM_CHANNELS_X-1:0]                                     rotate_en_side_out;
    logic [NUM_CHANNELS_X-1:0][$clog2(MAX_BF_AP_SIZE)-1:0]         rotate_bf_ap_idx_out;

    // Select Signals
    logic [NUM_CHANNELS_X-1:0][$clog2(MAX_BF_AP_SIZE)-1:0]         select_bf_ap_idx_out;
    logic [NUM_CHANNELS_X-1:0][3:0][PIPELINE_SAMPLE_BIT_WIDTH-1:0] select_out;
    logic [NUM_CHANNELS_X-1:0][3:0]                                select_en_out;

    // Regfile Signals
    logic [NUM_CHANNELS_X-1:0][PIPELINE_SAMPLE_BIT_WIDTH-1:0] regfile_out;
    logic [NUM_CHANNELS_X-1:0]                                regfile_en_out;
    logic [NUM_CHANNELS_X-1:0]                                regfile_bf_ap_done_out;
    logic [NUM_CHANNELS_X-1:0][$clog2(MAX_BF_AP_SIZE)-1:0]    regfile_bf_ap_idx_out;

    // Apod Signals
    logic [NUM_CHANNELS_X-1:0][PIPELINE_SAMPLE_BIT_WIDTH-1:0] apod_out;
    logic [NUM_CHANNELS_X-1:0]                                apod_en_out;
    logic [NUM_CHANNELS_X-1:0]                                apod_bf_ap_done_out;

    // Accum Signals
    logic [NUM_CHANNELS_X-1:0][IN_OUT_SAMPLE_BIT_WIDTH-1:0] accum_out;
    logic [NUM_CHANNELS_X-1:0]                              accum_en_out;
    logic [NUM_CHANNELS_X-1:0][IN_OUT_SAMPLE_BIT_WIDTH-1:0] data_out_next;
    logic [NUM_CHANNELS_X-1:0]                              data_en_out_next;

    // MAC Signals
    logic [NUM_CHANNELS_X-1:0][3:0][PIPELINE_SAMPLE_BIT_WIDTH-1:0] mac_stg1_out;
    logic [NUM_CHANNELS_X-1:0][3:0]                                mac_stg1_en_out;
    logic [NUM_CHANNELS_X-1:0][$clog2(MAX_BF_AP_SIZE)-1:0]         mac_stg1_bf_ap_idx_out;

    always_comb begin
        data_out_next    = 0;
        data_en_out_next = 0;

        case(output_select_in)
            ACCUM : begin
                data_out_next    = accum_out;
                data_en_out_next = accum_en_out;
            end
            APOD : begin
                data_out_next    = apod_out;
                data_en_out_next = apod_en_out;
            end
            REGFILE : begin
                data_out_next    = regfile_out;
                data_en_out_next = regfile_en_out;
            end
            SELECT : begin
                data_out_next    = select_out;
                data_en_out_next = select_en_out;
            end
            ROTATE_3 : begin
                for(int i = 0; i < NUM_CHANNELS_X; i++) begin
                    data_out_next[i] = rotate_out[i][3];
                end // for(int i = 0; i < NUM_CHANNELS_X; i++)
                data_en_out_next = rotate_en_out;
            end
            ROTATE_2 : begin
                for(int i = 0; i < NUM_CHANNELS_X; i++) begin
                    data_out_next[i] = rotate_out[i][2];
                end // for(int i = 0; i < NUM_CHANNELS_X; i++)
                data_en_out_next = rotate_en_out;
            end
            ROTATE_1 : begin
                for(int i = 0; i < NUM_CHANNELS_X; i++) begin
                    data_out_next[i] = rotate_out[i][1];
                end // for(int i = 0; i < NUM_CHANNELS_X; i++)
                data_en_out_next = rotate_en_out;
            end
            ROTATE_0 : begin
                for(int i = 0; i < NUM_CHANNELS_X; i++) begin
                    data_out_next[i] = rotate_out[i][0];
                end // for(int i = 0; i < NUM_CHANNELS_X; i++)
                data_en_out_next = rotate_en_out;
            end
            INTERP_3 : begin
                for(int i = 0; i < NUM_CHANNELS_X; i++) begin
                    data_out_next[i] = interp_out[i][3];
                end // for(int i = 0; i < NUM_CHANNELS_X; i++)
                data_en_out_next = interp_en_out;
            end
            INTERP_2 : begin
                for(int i = 0; i < NUM_CHANNELS_X; i++) begin
                    data_out_next[i] = interp_out[i][2];
                end // for(int i = 0; i < NUM_CHANNELS_X; i++)
                data_en_out_next = interp_en_out;
            end
            INTERP_1 : begin
                for(int i = 0; i < NUM_CHANNELS_X; i++) begin
                    data_out_next[i] = interp_out[i][1];
                end // for(int i = 0; i < NUM_CHANNELS_X; i++)
                data_en_out_next = interp_en_out;
            end
            INTERP_0 : begin
                for(int i = 0; i < NUM_CHANNELS_X; i++) begin
                    data_out_next[i] = interp_out[i][0];
                end // for(int i = 0; i < NUM_CHANNELS_X; i++)
                data_en_out_next = interp_en_out;
            end
            SAMPLE : begin
                data_out_next    = sample_out;
                data_en_out_next = sample_en_out;
            end
            INPUT : begin
                data_out_next    = sample_in;
                for(int i = 0; i < NUM_CHANNELS_X; i++) begin
                    data_en_out_next[i] = sample_en_in;
                end // for(int i = 0; i < NUM_CHANNELS_X; i++)
            end
            default : begin
                data_out_next    = accum_out;
                data_en_out_next = accum_en_out;
            end
        endcase // output_select_in
    end // always_comb

    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(reset) begin
            data_out    <= `SD 0;
            data_en_out <= `SD 0;
        end else begin
            data_out    <= `SD data_out_next;
            data_en_out <= `SD data_en_out_next;
        end // if(reset)
    end // always_ff @(posedge clock)


    // Sample Module
    sample #(.SAMPLE_BIT_WIDTH(IN_OUT_SAMPLE_BIT_WIDTH)) smpl [NUM_CHANNELS_X-1:0] (

        // Inputs
        .clock(clock),
        .reset(reset),
        .adc_in(sample_in),
        .adc_en_in(sample_en_in),

        // Outputs
        .sample_out(sample_out),
        .sample_en_out(sample_en_out)

    );


    // Interpolate Module
    interp #(.SAMPLE_BIT_WIDTH(IN_OUT_SAMPLE_BIT_WIDTH), .PIPELINE_SAMPLE_BIT_WIDTH(PIPELINE_SAMPLE_BIT_WIDTH)) ntrp [NUM_CHANNELS_X-1:0] (

        // Inputs
        .clock(clock),
        .reset(reset),
        .sample_in(sample_out),
        .sample_en_in(sample_en_out),

        // Outputs
        .interp_out(interp_out),
        .interp_en_out(interp_en_out)

    );


    // Rotate Module
    rotate #(.SAMPLE_BIT_WIDTH(PIPELINE_SAMPLE_BIT_WIDTH), .MAX_BF_AP_SIZE(MAX_BF_AP_SIZE)) rtt [NUM_CHANNELS_X-1:0] (

        // Inputs
        .clock(clock),
        .reset(reset),
        .interp_in(interp_out),
        .interp_en_in(interp_en_out),
        .rotate_side_in({rotate_side_out[0],rotate_side_out[NUM_CHANNELS_X-1:1]}),
        .rotate_en_side_in({rotate_en_side_out[0],rotate_en_side_out[NUM_CHANNELS_X-1:1]}),
        .bf_ap_size_in(bf_ap_size_in),

        // Outputs
        .rotate_side_out(rotate_side_out),
        .rotate_en_side_out(rotate_en_side_out),
        .rotate_out(rotate_out),
        .rotate_en_out(rotate_en_out),
        .rotate_bf_ap_idx_out(rotate_bf_ap_idx_out)

    );


    // Select Module
    select #(.SAMPLE_BIT_WIDTH(PIPELINE_SAMPLE_BIT_WIDTH), .MAX_BF_AP_SIZE(MAX_BF_AP_SIZE), .BITSTREAM_BUFFER_LEN(BITSTREAM_BUFFER_LEN), .TAU_TX1_MAX_VAL(TAU_TX1_MAX_VAL)) slct [NUM_CHANNELS_X-1:0] (

        // Inputs
        .clock(clock),
        .reset(reset),
        .rotate_in(rotate_out),
        .rotate_en_in(rotate_en_out),
        .tau_tx1_in(data_tau_tx1_in),
        .tau_rx_tx2_in(tau_rx_tx2_in),
        .tau_rx_tx2_en_in(tau_rx_tx2_en_in),
        .rotate_bf_ap_idx_in(rotate_bf_ap_idx_out),
        .bf_ap_size_in(bf_ap_size_in),

        // Outputs
        .select_out(select_out),
        .select_en_out(select_en_out),
        .select_bf_ap_idx_out(select_bf_ap_idx_out)

    );


    `ifdef USE_REGFILE
    // Regfile
    regfile #(.SAMPLE_BIT_WIDTH(PIPELINE_SAMPLE_BIT_WIDTH), .MAX_BF_AP_SIZE(MAX_BF_AP_SIZE), .REG_FILE_X(REG_FILE_X), .REG_FILE_Y(REG_FILE_Y), .NUM_BANKS(NUM_BANKS), .NUM_BANK_ENTRIES(NUM_BANK_ENTRIES)) rgfl [NUM_CHANNELS_X-1:0] (

        // Inputs
        .clock(clock),
        .reset(reset),
        .select_in(select_out),
        .select_en_in(select_en_out),
        .select_bf_ap_idx_in(select_bf_ap_idx_out),
        .bf_ap_size_in(bf_ap_size_in),

        // Outputs
        .regfile_out(regfile_out),
        .regfile_en_out(regfile_en_out),
        .regfile_bf_ap_done_out(regfile_bf_ap_done_out),
        .regfile_bf_ap_idx_out(regfile_bf_ap_idx_out)

    );


    // Apodize Module
    apod #(.SAMPLE_BIT_WIDTH(IN_OUT_SAMPLE_BIT_WIDTH), .PIPELINE_SAMPLE_BIT_WIDTH(PIPELINE_SAMPLE_BIT_WIDTH), .MAX_BF_AP_SIZE(MAX_BF_AP_SIZE)) pd [NUM_CHANNELS_X-1:0] (

        // Inputs
        .clock(clock),
        .reset(reset),
        .regfile_in(regfile_out),
        .regfile_en_in(regfile_en_out),
        .regfile_bf_ap_done_in(regfile_bf_ap_done_out),
        .apod_consts_in(apod_consts_in),

        // Outputs
        .apod_out(apod_out),
        .apod_en_out(apod_en_out),
        .apod_bf_ap_done_out(apod_bf_ap_done_out)

    );


    // Accumulate Module
    accum #(.SAMPLE_BIT_WIDTH(IN_OUT_SAMPLE_BIT_WIDTH), .PIPELINE_SAMPLE_BIT_WIDTH(PIPELINE_SAMPLE_BIT_WIDTH), .MAX_BF_AP_SIZE(MAX_BF_AP_SIZE)) ccm [NUM_CHANNELS_X-1:0] (

        // Inputs
        .clock(clock),
        .reset(reset),
        .apod_in(apod_out),
        .apod_en_in(apod_en_out),
        .apod_bf_ap_done_in(apod_bf_ap_done_out),

        // Outputs=
        .accum_out(accum_out),
        .accum_en_out(accum_en_out)

    );
    `else
    // // MAC Module
    // mac #(.SAMPLE_BIT_WIDTH(IN_OUT_SAMPLE_BIT_WIDTH), .PIPELINE_SAMPLE_BIT_WIDTH(PIPELINE_SAMPLE_BIT_WIDTH), .MAX_BF_AP_SIZE(MAX_BF_AP_SIZE), .MAC_BUFFER_ENTRIES(MAC_BUFFER_ENTRIES)) mc [NUM_CHANNELS_X-1:0] (

    //     // Inputs
    //     .clock(clock),
    //     .reset(reset),
    //     .select_in(select_out),
    //     .select_en_in(select_en_out),
    //     .select_bf_ap_idx_in(select_bf_ap_idx_out),
    //     .apod_consts_in(apod_consts_in),
    //     .bf_ap_size_in(bf_ap_size_in),

    //     // Outputs
    //     .mac_out(accum_out),
    //     .mac_en_out(accum_en_out)

    // );
    
    // // MAC Stage 1 Module - this wrapper instantiates multiple apod units (4x 1-wide instead of 1x 4-wide)
    // apod_v2_wrap #(.SAMPLE_BIT_WIDTH(IN_OUT_SAMPLE_BIT_WIDTH), .PIPELINE_SAMPLE_BIT_WIDTH(PIPELINE_SAMPLE_BIT_WIDTH), .MAX_BF_AP_SIZE(MAX_BF_AP_SIZE)) pd_v2_wrp [NUM_CHANNELS_X-1:0] (

    //     // Inputs
    //     .clock(clock),
    //     .reset(reset),
    //     .select_in(select_out),
    //     .select_en_in(select_en_out),
    //     .select_bf_ap_idx_in(select_bf_ap_idx_out),
    //     .apod_consts_in(apod_consts_in),

    //     // Outputs
    //     .apod_v2_wrap_out(mac_stg1_out),
    //     .apod_v2_wrap_en_out(mac_stg1_en_out),
    //     .apod_v2_wrap_bf_ap_idx_out(mac_stg1_bf_ap_idx_out)

    // );
    // MAC Stage 1 Module
    mac_stg1 #(.SAMPLE_BIT_WIDTH(IN_OUT_SAMPLE_BIT_WIDTH), .PIPELINE_SAMPLE_BIT_WIDTH(PIPELINE_SAMPLE_BIT_WIDTH), .MAX_BF_AP_SIZE(MAX_BF_AP_SIZE), .MAC_BUFFER_ENTRIES(MAC_BUFFER_ENTRIES)) mc_stg1 [NUM_CHANNELS_X-1:0] (

        // Inputs
        .clock(clock),
        .reset(reset),
        .select_in(select_out),
        .select_en_in(select_en_out),
        .select_bf_ap_idx_in(select_bf_ap_idx_out),
        .apod_consts_in(apod_consts_in),

        // Outputs
        .mac_stg1_out(mac_stg1_out),
        .mac_stg1_en_out(mac_stg1_en_out),
        .mac_stg1_bf_ap_idx_out(mac_stg1_bf_ap_idx_out)

    );
    `ifdef USE_MAC_BANK
    // MAC Stage 2 Module
    mac_stg2_bank #(.SAMPLE_BIT_WIDTH(IN_OUT_SAMPLE_BIT_WIDTH), .PIPELINE_SAMPLE_BIT_WIDTH(PIPELINE_SAMPLE_BIT_WIDTH), .MAX_BF_AP_SIZE(MAX_BF_AP_SIZE), .MAC_BUFFER_ENTRIES(MAC_BUFFER_ENTRIES)) mc_stg2 [NUM_CHANNELS_X-1:0] (

        // Inputs
        .clock(clock),
        .reset(reset),
        .mac_stg1_in(mac_stg1_out),
        .mac_stg1_en_in(mac_stg1_en_out),
        .mac_stg1_bf_ap_idx_in(mac_stg1_bf_ap_idx_out),
        .bf_ap_size_in(bf_ap_size_in),

        // Outputs
        .mac_stg2_out(accum_out),
        .mac_stg2_en_out(accum_en_out)

    );
    `else
    // MAC Stage 2 Module
    mac_stg2 #(.SAMPLE_BIT_WIDTH(IN_OUT_SAMPLE_BIT_WIDTH), .PIPELINE_SAMPLE_BIT_WIDTH(PIPELINE_SAMPLE_BIT_WIDTH), .MAX_BF_AP_SIZE(MAX_BF_AP_SIZE), .MAC_BUFFER_ENTRIES(MAC_BUFFER_ENTRIES)) mc_stg2 [NUM_CHANNELS_X-1:0] (

        // Inputs
        .clock(clock),
        .reset(reset),
        .mac_stg1_in(mac_stg1_out),
        .mac_stg1_en_in(mac_stg1_en_out),
        .mac_stg1_bf_ap_idx_in(mac_stg1_bf_ap_idx_out),
        .bf_ap_size_in(bf_ap_size_in),

        // Outputs
        .mac_stg2_out(accum_out),
        .mac_stg2_en_out(accum_en_out)

    );
    `endif
    `endif

endmodule // pipeline
