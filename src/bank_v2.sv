/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  bank.sv                                             //
//                                                                     //
//  Description :  This module creates a single bank of the register   //
//                 file.                                               //
//                                                                     //
/////////////////////////////////////////////////////////////////////////


// `timescale 1ns/100ps


module bank #(parameter SAMPLE_BIT_WIDTH = 12, parameter NUM_BANK_ENTRIES = 256) (

        // Inputs
        input                                clock,
        input                                wr_clr_idx, // clear the write index
        input [$clog2(NUM_BANK_ENTRIES)-1:0] rd_idx, // read index
        input [$clog2(NUM_BANK_ENTRIES)-1:0] wr_idx, // write index
        input [SAMPLE_BIT_WIDTH-1:0]         wr_data, // write data
        input                                wr_en,

        // Outputs
        output logic [SAMPLE_BIT_WIDTH-1:0] rd_out // read data

    );

    logic [NUM_BANK_ENTRIES-1:0][SAMPLE_BIT_WIDTH-1:0] registers; // registers


    wire [SAMPLE_BIT_WIDTH-1:0] temp_value = registers[wr_idx] + wr_data;

    wire [SAMPLE_BIT_WIDTH-1:0] rd_reg = registers[rd_idx];
    
    //
    // Read port
    //
    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin=
        // if(wr_en && (wr_idx == rd_idx)) begin
        //     rd_out <= `SD wr_data;  // internal forwarding
        // end else begin
        //     rd_out <= `SD rd_reg;
        // end // if(wr_en && (wr_idx == rd_idx))
        rd_out <= `SD rd_reg;
    end // always_ff @(posedge clock)

    //
    // Write port
    //
    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(wr_en) begin
            if(wr_clr_idx) begin
                registers[wr_idx] <= `SD 0;
            end else begin
                registers[wr_idx] <= `SD temp_value;
            end // if(wr_clr_idx)
        end // if(wr_en)
    end // always_ff @(posedge clock)

endmodule // bank
