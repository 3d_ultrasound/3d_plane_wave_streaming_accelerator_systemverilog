/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  pipeline_wrapper.sv                                 //
//                                                                     //
//  Description :  Wrapper for top-level module of the beamforming     //
//                 pipeline; this holds all data required for a single //
//                 pipeline slice and feeds the pipeline to simulate   //
//                 high-bandwidth/parallel data streaming.             //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module pipeline_wrapper #(parameter IN_OUT_SAMPLE_BIT_WIDTH = 12, parameter PIPELINE_SAMPLE_BIT_WIDTH = 10, parameter BITSTREAM_BUFFER_LEN = 64, parameter TAU_TX1_MAX_VAL = 1024, parameter MAX_BF_AP_SIZE = 32, parameter REG_FILE_X = 32, parameter REG_FILE_Y = 32, parameter NUM_BANKS = 4, parameter NUM_SRAM_BANKS = 8, parameter NUM_BANK_ENTRIES = ((REG_FILE_X*REG_FILE_Y)/NUM_BANKS), parameter MAC_BUFFER_ENTRIES = 64, parameter NUM_STAGES = 8, parameter NUM_CHANNELS_X = 32) (

        // Inputs
        // Control signals - can change every cycle
        input                        clock,
        input                        reset,
        // Input packet
        input bsg_fsb_pkt_s          fsb_packet_in,
        input                        fsb_packet_valid_in,
        // Output buffer not full
        input                        fsb2_ready_in,

        // Outputs
        // Output packet
        output bsg_fsb_pkt_s         fsb_packet_out,
        output logic                 fsb_packet_valid_out,
        // Input packet accepted
        output logic                 pipeline_wrapper_ready_out

    );

    // Signals
    
    // Initialization Constants
    logic                                                                                                                                     reset_pipelines;
    logic [$clog2(MAX_BF_AP_SIZE)-1:0]                                                                                                        bf_ap_size, bf_ap_size_next;
    logic [NUM_CHANNELS_X-1:0][(MAX_BF_AP_SIZE/`APOD_CONSTS_FILE_READ_WIDTH)-1:0][(IN_OUT_SAMPLE_BIT_WIDTH*`APOD_CONSTS_FILE_READ_WIDTH)-1:0] apod_consts, apod_consts_next;
    logic [$clog2(NUM_CHANNELS_X)-1:0]                                                                                                        apod_consts_channel_idx, apod_consts_channel_idx_next;
    logic [$clog2(MAX_BF_AP_SIZE/`APOD_CONSTS_FILE_READ_WIDTH)-1:0]                                                                           apod_consts_idx, apod_consts_idx_next;
    logic [$clog2(TAU_TX1_MAX_VAL)-1:0]                                                                                                       tau_rx_tx2_rd_delay, tau_rx_tx2_rd_delay_next;
    logic [`TAU_RX_TX2_FILE_READ_WIDTH-1:0]                                                                                                   tau_rx_tx2_sram_tau_wr_data, tau_rx_tx2_sram_tau_wr_data_next;
    logic                                                                                                                                     tau_rx_tx2_sram_tau_wr_en, tau_rx_tx2_sram_tau_wr_en_next;
    logic [NUM_CHANNELS_X-1:0][(MAX_BF_AP_SIZE/`TAU_TX1_FILE_READ_WIDTH)-1:0][($clog2(TAU_TX1_MAX_VAL)*`TAU_TX1_FILE_READ_WIDTH)-1:0]         tau_tx1_consts, tau_tx1_consts_next;
    logic [$clog2(NUM_CHANNELS_X)-1:0]                                                                                                        tau_tx1_consts_channel_idx, tau_tx1_consts_channel_idx_next;
    logic [$clog2(MAX_BF_AP_SIZE/`TAU_TX1_FILE_READ_WIDTH)-1:0]                                                                               tau_tx1_consts_idx, tau_tx1_consts_idx_next;
    logic [`INPUT_DATA_FILE_READ_WIDTH-1:0][IN_OUT_SAMPLE_BIT_WIDTH-1:0]                                                                      data_sram_wr_data, data_sram_wr_data_next;
    logic                                                                                                                                     data_sram_wr_en, data_sram_wr_en_next;
    logic [$clog2(MAX_BF_AP_SIZE)-1:0]                                                                                                        data_num_shifts, data_num_shifts_next; // number of cycles between data reads
    logic [3:0]                                                                                                                               output_select, output_select_next;
    logic [NUM_CHANNELS_X-1:0][IN_OUT_SAMPLE_BIT_WIDTH-1:0]                                                                                   data_out;
    logic [NUM_CHANNELS_X-1:0]                                                                                                                data_en_out;
    
    bsg_fsb_pkt_s                                                                                                                             fsb_packet;
    logic                                                                                                                                     fsb_packet_valid;
    logic                                                                                                                                     done, done_next;
    logic                                                                                                                                     run, run_next;
    logic                                                                                                                                     fsb_packet_valid_out_first_transition;

    assign pipeline_wrapper_ready_out = ~run; // We don't want new packets while we're processing...they might overwrite stuff



//////////////////////////////////////////////
//                                          //
//              BITSTREAM SRAM              //
//                                          //
//////////////////////////////////////////////
    // tau_rx_tx2 SRAM *WRITE* Signals
    logic [NUM_SRAM_BANKS-1:0]                                                                 tau_wr_en, tau_wr_en_next;
    logic [(`TAU_SRAM_WIDTH/`TAU_RX_TX2_FILE_READ_WIDTH)-1:0][`TAU_RX_TX2_FILE_READ_WIDTH-1:0] tau_wr_data, tau_wr_data_next;
    logic [$clog2(`TAU_SRAM_WIDTH/`TAU_RX_TX2_FILE_READ_WIDTH)-1:0]                            tau_wr_idx, tau_wr_idx_next;
    logic [$clog2(NUM_SRAM_BANKS)-1:0]                                                         tau_wr_bank_idx, tau_wr_bank_idx_next;
    logic [$clog2(`TAU_SRAM_NUM_ENTRIES)-1:0]                                                  tau_wr_addr, tau_wr_addr_next;
    logic [$clog2(`TAU_SRAM_NUM_ENTRIES)-1:0]                                                  tau_wr_max_addr;

    // tau_rx_tx2 SRAM *READ* Signals
    logic [$clog2(TAU_TX1_MAX_VAL)-1:0]                                                        rd_init_delay, rd_init_delay_next;
    logic [NUM_SRAM_BANKS-1:0]                                                                 tau_rd_en, tau_rd_en_next, tau_rd_en_saved, tau_rd_en_saved_next, tau_rd_en_saved2, tau_rd_en_saved2_next;
    logic [NUM_SRAM_BANKS-1:0][(`TAU_SRAM_WIDTH/4)-1:0][3:0]                                   tau_rd_data, tau_rd_data_next;
    logic [$clog2(`TAU_SRAM_WIDTH/4)-1:0]                                                      tau_rd_idx, tau_rd_idx_next, tau_rd_idx_saved, tau_rd_idx_saved_next, tau_rd_idx_saved2, tau_rd_idx_saved2_next;
    logic [$clog2(NUM_SRAM_BANKS)-1:0]                                                         tau_rd_bank_idx, tau_rd_bank_idx_next, tau_rd_bank_idx_saved, tau_rd_bank_idx_saved_next, tau_rd_bank_idx_saved2, tau_rd_bank_idx_saved2_next;
    logic [$clog2(`TAU_SRAM_NUM_ENTRIES)-1:0]                                                  tau_rd_addr, tau_rd_addr_next;
    logic [3:0]                                                                                tau_rd_data_saved, tau_rd_data_saved_next;
    logic [$clog2(MAX_BF_AP_SIZE)-1:0]                                                         tau_shift_count, tau_shift_count_next; // number of cycles between data reads

    // tau_rx_tx2 Output Signals
    logic [3:0]                                                                                tau_rx_tx2;
    logic                                                                                      tau_rx_tx2_en;

    `ifdef USE_SRAM_45
    // IBM 45nm SOI
    sram_2p_hs_v60_uvtsvt_tau64 tau_rx_tx2_sram [NUM_SRAM_BANKS-1:0] (

        // Inputs
        .CLKA(clock), // clock for port A - ADD "~" FOR SIMULATION; REMOVE FOR SYNTHESIS
        .CENA(~tau_rd_en_next), // chip enable (clock gating) (if read and write enable, or them and invert)...active LOW, tie to 0
        .AA(tau_rd_addr), // address A
        .CLKB(clock), // clock for port B
        .CENB(~tau_wr_en), // chip enable (clock gating) (if read and write enable, or them and invert)...active LOW, tie to 0
        .AB(tau_wr_addr), // address B
        .DB(tau_wr_data), // data B
        .EMAA(3'b000), // extra margin adjustment A (tunable delay signal); some constant (user guide?); Austin uses 2 in TSMC16
        .EMASA(1'b0), // extra margin ajustment - tie to 0
        .EMAB(3'b000), // extra margin adjustment B (tunable delay signal); some constant (user guide?); Austin uses 2 in TSMC16
        .EMAWB(2'b01), // tie to 1
        .NWA(1'b0), // try setting to 1?
        .TENA(1'b1), // test mode enable - tie to 1
        .BENA(1'b1), // try setting to 1?
        .TCENA(1'b1), // chip enable test input, active LOW - tie to 1
        .TAA({$clog2(`TAU_SRAM_NUM_ENTRIES){1'b0}}), // address test input - tie to 0
        .TQA({`TAU_SRAM_WIDTH{1'b0}}), // test mode data input - tie to 0
        .TENB(1'b1), // test mode enable - tie to 1
        .TCENB(1'b1), // chip enable test input, active LOW - tie to 1
        .TAB({$clog2(`TAU_SRAM_NUM_ENTRIES){1'b0}}), // address test input - tie to 0
        .TDB({`TAU_SRAM_WIDTH{1'b0}}), // test mode data input - tie to 0
        .STOVA(1'b0), // self timed override - tie to 0
        .STOVB(1'b0), // tie to 0

        // Outputs
        .CENYA(), // chip enable multiplexer output
        .AYA(), // address multiplexer output
        .CENYB(), // chip enable multiplexer output
        .AYB(), // no idea
        .DYB(), // no idea
        .QA(tau_rd_data_next) // output data for A

    );
    `elsif USE_SRAM_16
    // TSMC 16nm
    sram_2p_uhde_tau tau_rx_tx2_sram [NUM_SRAM_BANKS-1:0] (

        // Inputs
        .CLK(clock), // clock - ADD "~" FOR SIMULATION; REMOVE FOR SYNTHESIS
        .CENA(~tau_rd_en_next), // read port chip enable
        .CENB(~tau_wr_en), // write port chip enable
        .AA(tau_rd_addr), // read port address
        .AB(tau_wr_addr), // write port address
        .DB(tau_wr_data), // write port data
        .STOV(1'b0), // When the STOV and STOVAB pins are HIGH, the internal clock pulse for port A is generated directly from the high phase of the external clock input
        .STOVAB(1'b0), // When the STOV pin is HIGH and the STOVAB pin is LOW, the internal clock pulse for port B is generated directly from the high phase of the external clock input.
        .EMA(3'b010), // extra margin adjustment - Austin uses 010; try 010 (default)
        .EMAW(2'b01), // similar to EMA; try 01 (default)
        .EMAS(1'b0), // default is LOW
        .EMAP(1'b0), // default is LOW
        .RET1N(1'b1), // retention mode; try 1?

        // Outputs
        .QA(tau_rd_data_next) // output data for A

    );
    `else
    // Instantiate a bank array of size NUM_BANKS
    bank #(.SAMPLE_BIT_WIDTH(64), .NUM_BANK_ENTRIES(512)) tau_rx_tx2_array [NUM_SRAM_BANKS-1:0] (

        // Inputs
        .clock(clock),
        .rd_idx(tau_rd_addr), // read index
        .wr_idx(tau_wr_addr), // write index
        .wr_data(tau_wr_data), // write data
        .wr_en(tau_wr_en), // write enable

        // Outputs
        .rd_out(tau_rd_data_next) // read data

    );
    `endif

    assign tau_rx_tx2_en = tau_rd_en_saved ? 1 : 0;
    assign tau_rx_tx2    = tau_rd_en_saved ? tau_rd_data[tau_rd_bank_idx_saved2][tau_rd_idx_saved2] : 0;

    // Read the SRAM
    always_comb begin
        tau_rd_idx_saved_next       = tau_rd_idx;
        tau_rd_data_saved_next      = 0;
        tau_rd_en_saved_next        = tau_rd_en;
        rd_init_delay_next          = rd_init_delay;
        tau_rd_idx_next             = tau_rd_idx;
        tau_rd_en_next              = 0;
        tau_rd_addr_next            = tau_rd_addr;
        tau_rd_bank_idx_next        = tau_rd_bank_idx;
        tau_rd_bank_idx_saved_next  = tau_rd_bank_idx;
        tau_rd_bank_idx_saved2_next = tau_rd_bank_idx_saved;
        tau_rd_idx_saved2_next      = tau_rd_idx_saved;
        tau_rd_en_saved2_next       = tau_rd_en_saved;
        tau_shift_count_next        = tau_shift_count;

        if(run) begin
            if((rd_init_delay_next == 0)) begin
                // if(tau_rd_addr <= tau_wr_max_addr) begin
                if((tau_rd_addr <= tau_wr_max_addr) && (tau_shift_count <= bf_ap_size)) begin
                    tau_rd_en_next[tau_rd_bank_idx] = 1;

                    // Increment buffer idx
                    tau_rd_idx_next = tau_rd_idx + 1;

                    if(tau_rd_idx_next == 0) begin
                        tau_rd_bank_idx_next = tau_rd_bank_idx + 1;

                        if(tau_rd_bank_idx_next == 0) begin
                            tau_rd_addr_next = tau_rd_addr + 1;
                        end // if(tau_rd_bank_idx_next == 0)
                    end // if(tau_rd_idx_next == 0)
                end // if(tau_rd_addr <= tau_wr_max_addr)

                tau_shift_count_next = tau_shift_count + 1;
                // $display("TEST");

                if(tau_shift_count == data_num_shifts) begin
                    tau_shift_count_next = 0;
                end // if(data_shift_count == 0)
            end else begin
                rd_init_delay_next = rd_init_delay - 1;
            end // if(rd_init_delay_next == 0)
        end // if(run)
    end // always_comb

    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(reset) begin
            tau_rd_bank_idx_saved  <= `SD 0;
            tau_rd_addr            <= `SD 0;
            tau_rd_bank_idx        <= `SD 0;
            tau_rd_en              <= `SD 0;
            tau_rd_idx_saved       <= `SD 0;
            tau_rd_idx             <= `SD 0;
            tau_rd_data_saved      <= `SD 0;
            tau_rd_en_saved        <= `SD 0;
            tau_rd_data            <= `SD 0;
            tau_rd_bank_idx_saved2 <= `SD 0;
            tau_rd_idx_saved2      <= `SD 0;
            tau_rd_en_saved2       <= `SD 0;
            rd_init_delay          <= `SD 0;
            tau_shift_count        <= `SD 0;
        end else if(reset_pipelines) begin
            tau_rd_bank_idx_saved  <= `SD 0;
            tau_rd_addr            <= `SD 0;
            tau_rd_bank_idx        <= `SD 0;
            tau_rd_en              <= `SD 0;
            tau_rd_idx_saved       <= `SD 0;
            tau_rd_idx             <= `SD 0;
            tau_rd_data_saved      <= `SD 0;
            tau_rd_en_saved        <= `SD 0;
            tau_rd_data            <= `SD 0;
            tau_rd_bank_idx_saved2 <= `SD 0;
            tau_rd_idx_saved2      <= `SD 0;
            tau_rd_en_saved2       <= `SD 0;
            rd_init_delay          <= `SD tau_rx_tx2_rd_delay;
            $display("rd_init_delay: %d", tau_rx_tx2_rd_delay);
            tau_shift_count        <= `SD 0;
        end else begin
            if(rd_init_delay) begin
                $display("rd_init_delay: %d", rd_init_delay);
            end // if(rd_init_delay)
            tau_rd_bank_idx_saved  <= `SD tau_rd_bank_idx_saved_next;
            tau_rd_addr            <= `SD tau_rd_addr_next;
            tau_rd_bank_idx        <= `SD tau_rd_bank_idx_next;
            tau_rd_en              <= `SD tau_rd_en_next;
            tau_rd_idx_saved       <= `SD tau_rd_idx_saved_next;
            tau_rd_idx             <= `SD tau_rd_idx_next;
            tau_rd_data_saved      <= `SD tau_rd_data_saved_next;
            tau_rd_en_saved        <= `SD tau_rd_en_saved_next;
            tau_rd_data            <= `SD tau_rd_data_next;
            tau_rd_bank_idx_saved2 <= `SD tau_rd_bank_idx_saved2_next;
            tau_rd_idx_saved2      <= `SD tau_rd_idx_saved2_next;
            tau_rd_en_saved2       <= `SD tau_rd_en_saved2_next;
            rd_init_delay          <= `SD rd_init_delay_next;
            tau_shift_count        <= `SD tau_shift_count_next;
        end // (reset)
    end // always_ff @(posedge clock)

    // Fill the SRAM
    always_comb begin
        tau_wr_en_next       = 0;
        tau_wr_data_next     = tau_wr_data;
        tau_wr_idx_next      = tau_wr_idx;
        tau_wr_addr_next     = tau_wr_addr;
        tau_wr_bank_idx_next = tau_wr_bank_idx;

        // Write data
        if(tau_rx_tx2_sram_tau_wr_en) begin
            tau_wr_data_next[tau_wr_idx] = tau_rx_tx2_sram_tau_wr_data;

            // Increment nibble pointer
            tau_wr_idx_next              = tau_wr_idx + 1;

            // Write the value when the buffer idx overflows
            if(tau_wr_idx_next == 0) begin
                // Enable the write
                tau_wr_en_next[tau_wr_bank_idx] = 1;
            end // if(tau_wr_idx_next == 0)

            // Increment the bank idx when a write occurs
            if(tau_wr_en) begin
                // Increment the bank idx
                tau_wr_bank_idx_next = tau_wr_bank_idx + 1;

                // Increment the address when the bank idx overflows
                if(tau_wr_bank_idx_next == 0) begin
                    // Increment the address
                    tau_wr_addr_next = tau_wr_addr + 1;
                end // if(tau_wr_bank_idx_next == 0)
            end // if(tau_wr_en)
        end // if(tau_rx_tx2_sram_tau_wr_en)
    end // always_comb

    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(tau_rx_tx2_sram_tau_wr_en) begin
            tau_wr_bank_idx <= `SD tau_wr_bank_idx_next;
            tau_wr_en       <= `SD tau_wr_en_next;
            tau_wr_data     <= `SD tau_wr_data_next;
            tau_wr_idx      <= `SD tau_wr_idx_next;
            tau_wr_addr     <= `SD tau_wr_addr_next;
            tau_wr_max_addr <= `SD tau_wr_addr_next;
        end else begin
            tau_wr_bank_idx <= `SD 0;
            tau_wr_en       <= `SD 0;
            tau_wr_data     <= `SD 0;
            tau_wr_idx      <= `SD 0;
            tau_wr_addr     <= `SD 0;
        end // (reset)
    end // always_ff @(posedge clock)



//////////////////////////////////////////////
//                                          //
//            INPUT CHANNEL SRAM            //
//                                          //
//////////////////////////////////////////////
    // Input Data SRAM Signals
    logic [NUM_CHANNELS_X-1:0]                                          data_rd_en, data_rd_en_next, data_read_rd_en_next, data_rd_en_saved, data_rd_en_saved_next;
    logic [NUM_CHANNELS_X-1:0][IN_OUT_SAMPLE_BIT_WIDTH-1:0]             data_rd_data, data_rd_data_next;
    logic [NUM_CHANNELS_X-1:0][$clog2(`DATA_SRAM_NUM_ENTRIES)-1:0]      data_rd_addr, data_rd_addr_next, data_read_rd_addr, data_read_rd_addr_next;
    logic                                                               data_en, data_en_next, data_en_saved;
    logic [NUM_CHANNELS_X-1:0][IN_OUT_SAMPLE_BIT_WIDTH-1:0]             data, data_next, data_rd_data_saved, data_rd_data_saved_next;
    logic [$clog2(MAX_BF_AP_SIZE)-1:0]                                  data_shift_count, data_shift_count_next; // number of cycles between data reads
    logic [NUM_CHANNELS_X-1:0][$clog2(`DATA_SRAM_NUM_ENTRIES)-1:0]      data_rd_max_addr;

    logic [(NUM_CHANNELS_X-1):0]                                        data_wr_en, data_wr_en_next;
    logic [NUM_CHANNELS_X-1:0][IN_OUT_SAMPLE_BIT_WIDTH-1:0]             data_wr_data, data_wr_data_next;
    logic [$clog2(NUM_CHANNELS_X)-1:0]                                  data_sram_arr_wr_idx, data_sram_arr_wr_idx_next;
    logic [`INPUT_DATA_FILE_READ_WIDTH-1:0][$clog2(NUM_CHANNELS_X)-1:0] data_sram_arr_wr_idx_b;
    logic [NUM_CHANNELS_X-1:0][$clog2(`DATA_SRAM_NUM_ENTRIES)-1:0]      data_wr_addr, data_wr_addr_next;
    logic [NUM_CHANNELS_X-1:0][$clog2(`DATA_SRAM_NUM_ENTRIES)-1:0]      data_wr_max_addr, output_wr_max_addr;

    logic [NUM_CHANNELS_X-1:0]                                          output_read_rd_en, output_read_rd_en_next, output_read_rd_en_saved, output_read_rd_en_saved_next;
    logic [NUM_CHANNELS_X-1:0][IN_OUT_SAMPLE_BIT_WIDTH-1:0]             output_read_rd_data, output_read_rd_data_next;
    logic [$clog2(NUM_CHANNELS_X)-1:0]                                  output_read_arr_rd_idx, output_read_arr_rd_idx_next;
    logic [NUM_CHANNELS_X-1:0][$clog2(`DATA_SRAM_NUM_ENTRIES)-1:0]      output_read_rd_addr, output_read_rd_addr_next;
    logic [NUM_CHANNELS_X-1:0][$clog2(`DATA_SRAM_NUM_ENTRIES)-1:0]      output_rd_max_addr, output_rd_max_addr_saved, output_rd_max_addr_saved_next, output_rd_max_addr_saved2, output_rd_max_addr_saved_next2;

    `ifdef USE_SRAM_45
    // IBM 45nm SOI
    sram_2p_hs_v60_uvtsvt_input data_channel_sram [NUM_CHANNELS_X-1:0] (

        // datas
        .CLKA(clock), // clock for port A - ADD "~" FOR SIMULATION; REMOVE FOR SYNTHESIS
        .CENA(~data_rd_en_next), // chip enable (clock gating) (if read and write enable, or them and invert)...active LOW, tie to 0
        .AA(data_rd_addr), // address A
        .CLKB(clock), // clock for port B
        .CENB(~data_wr_en_next), // chip enable (clock gating) (if read and write enable, or them and invert)...active LOW, tie to 0
        .AB(data_wr_addr), // address B
        .DB(data_wr_data_next), // data B
        .EMAA(3'b000), // extra margin adjustment A (tunable delay signal); some constant (user guide?); Austin uses 2 in TSMC16
        .EMASA(1'b0), // extra margin ajustment - tie to 0
        .EMAB(3'b000), // extra margin adjustment B (tunable delay signal); some constant (user guide?); Austin uses 2 in TSMC16
        .EMAWB(2'b01), // tie to 1
        .NWA(1'b0), // try setting to 1?
        .TENA(1'b1), // test mode enable - tie to 1
        .BENA(1'b1), // try setting to 1?
        .TCENA(1'b1), // chip enable test data, active LOW - tie to 1
        .TAA(12'b0), // address test data - tie to 0
        .TQA(12'b0), // test mode data data - tie to 0
        .TENB(1'b1), // test mode enable - tie to 1
        .TCENB(1'b1), // chip enable test data, active LOW - tie to 1
        .TAB(12'b0), // address test data - tie to 0
        .TDB(12'b0), // test mode data data - tie to 0
        .STOVA(1'b0), // self timed override - tie to 0
        .STOVB(1'b0), // tie to 0

        // Outputs
        .CENYA(), // chip enable multiplexer output
        .AYA(), // address multiplexer output
        .CENYB(), // chip enable multiplexer output
        .AYB(), // no idea
        .DYB(), // no idea
        .QA(data_rd_data_next) // output data for A

    );
    `elsif USE_SRAM_16
    // TSMC 16nm
    sram_2p_uhde_in data_channel_sram [NUM_CHANNELS_X-1:0] (

        // Inputs
        .CLK(clock), // clock - ADD "~" FOR SIMULATION; REMOVE FOR SYNTHESIS
        .CENA(~data_rd_en_next), // read port chip enable
        .CENB(~data_wr_en_next), // write port chip enable
        .AA(data_rd_addr), // read port address
        .AB(data_wr_addr), // write port address
        .DB(data_wr_data_next), // write port data
        .STOV(1'b0), // When the STOV and STOVAB pins are HIGH, the internal clock pulse for port A is generated directly from the high phase of the external clock input
        .STOVAB(1'b0), // When the STOV pin is HIGH and the STOVAB pin is LOW, the internal clock pulse for port B is generated directly from the high phase of the external clock input.
        .EMA(3'b010), // extra margin adjustment - Austin uses 010; try 010 (default)
        .EMAW(2'b01), // similar to EMA; try 01 (default)
        .EMAS(1'b0), // default is LOW
        .EMAP(1'b0), // default is LOW
        .RET1N(1'b1), // retention mode; try 1?

        // Outputs
        .QA(data_rd_data_next) // output data for A

    );
    `else
    // Instantiate a bank array of size NUM_BANKS
    bank #(.SAMPLE_BIT_WIDTH(12), .NUM_BANK_ENTRIES(4096)) data_channel_array [NUM_CHANNELS_X-1:0] (

        // Inputs
        .clock(clock),
        .rd_idx(data_rd_addr), // read index
        .wr_idx(data_wr_addr), // write index
        .wr_data(data_wr_data), // write data
        .wr_en(data_wr_en), // write enable

        // Outputs
        .rd_out(data_rd_data_next) // read data

    );
    `endif

    assign data_en   = data_rd_en_saved[0];
    assign data      = data_rd_en_saved[0] ? data_rd_data_saved : 0;

    assign fsb_packet_out.opcode                            = output_read_rd_en_saved[output_read_arr_rd_idx]                                                 ? READ_OUTPUT                                 : NULL_PACKET;
    assign fsb_packet_out.data[IN_OUT_SAMPLE_BIT_WIDTH-1:0] = output_read_rd_en_saved[output_read_arr_rd_idx]                                                 ? output_read_rd_data[output_read_arr_rd_idx] : 0;
    assign fsb_packet_valid_out                             = ((output_rd_max_addr != output_wr_max_addr) || output_read_rd_en_saved[output_read_arr_rd_idx]) ? done : 0;

    // Read the SRAM
    always_comb begin
        data_rd_en_next              = 0;
        data_read_rd_en_next         = 0;
        output_read_rd_en_next       = 0;
        data_rd_en_saved_next        = data_rd_en;
        data_rd_addr_next            = data_rd_addr;
        data_shift_count_next        = data_shift_count;
        data_read_rd_addr_next       = data_read_rd_addr;
        output_read_rd_en_saved_next = output_read_rd_en;
        output_read_rd_addr_next     = output_read_rd_addr;
        output_read_arr_rd_idx_next  = output_read_arr_rd_idx;

        // Read input data
        if(run) begin
            for(int i = 0; i < NUM_CHANNELS_X; i++) begin
                if((data_read_rd_addr[i] < data_wr_max_addr[i]) && (data_shift_count == data_num_shifts)) begin
                    data_rd_en_next[i]        = 1;
                    data_read_rd_en_next[i]   = 1;
                    data_rd_addr_next[i]      = data_rd_addr[i] + 1;
                    data_read_rd_addr_next[i] = data_read_rd_addr[i] + 1;
                end // if((data_rd_addr[i] < data_wr_max_addr[i]) && (data_shift_count == data_num_shifts))
            end // for(i = 0; i < NUM_CHANNELS_X; i++)

            data_shift_count_next = data_shift_count - 1;

            if(data_shift_count == 0) begin
                data_shift_count_next = data_num_shifts;
            end // if(data_shift_count == 0)
        end // if(run)

        // Read output data
        if(done && fsb_packet_valid_out_first_transition && fsb2_ready_in) begin
            if(output_read_rd_addr[output_read_arr_rd_idx] < output_wr_max_addr[output_read_arr_rd_idx]) begin
                data_rd_en_next[output_read_arr_rd_idx]          = 1;
                data_rd_addr_next[output_read_arr_rd_idx]        = output_read_rd_addr[output_read_arr_rd_idx] + 1;
                output_read_rd_en_next[output_read_arr_rd_idx]   = 1;
                output_read_rd_addr_next[output_read_arr_rd_idx] = output_read_rd_addr[output_read_arr_rd_idx] + 1;
            end // if(output_read_rd_addr[output_read_arr_rd_idx] < output_wr_max_addr[output_read_arr_rd_idx])

            // Increment address pointer after all channels have been written
            if((output_read_rd_addr[output_read_arr_rd_idx] == output_wr_max_addr[output_read_arr_rd_idx]) && ~output_read_rd_en_saved[output_read_arr_rd_idx]) begin
                output_read_arr_rd_idx_next = output_read_arr_rd_idx + 1;
            end // if((output_read_rd_addr[output_read_arr_rd_idx] == output_wr_max_addr[output_read_arr_rd_idx]) && ~output_read_rd_en_saved[output_read_arr_rd_idx])
        end // if(done && fsb_packet_valid_out_first_transition && fsb2_ready_in)
    end // always_comb

    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(reset | reset_pipelines) begin
            data_rd_en                            <= `SD 0;
            data_rd_addr                          <= `SD 0;
            data_read_rd_addr                     <= `SD 0;
            data_rd_data_saved                    <= `SD 0;
            data_rd_en_saved                      <= `SD 0;
            data_shift_count                      <= `SD data_num_shifts;

            output_read_rd_data                   <= `SD 0;
            output_read_rd_en_saved               <= `SD 0;
            output_read_rd_en                     <= `SD 0;
            output_read_rd_addr                   <= `SD 0;
            output_read_arr_rd_idx                <= `SD 0;

            // Reset the max address
            data_rd_max_addr                      <= `SD 0;
            output_rd_max_addr                    <= `SD 0;

            fsb_packet_valid_out_first_transition <= `SD 0;
        end else begin
            data_rd_en              <= `SD data_read_rd_en_next;
            data_rd_addr            <= `SD data_rd_addr_next;
            data_read_rd_addr       <= `SD data_read_rd_addr_next;
            data_rd_data_saved      <= `SD data_rd_data_next;
            data_rd_en_saved        <= `SD data_rd_en_saved_next;
            data_shift_count        <= `SD data_shift_count_next;

            output_read_rd_data     <= `SD data_rd_data_next;
            output_read_rd_en_saved <= `SD output_read_rd_en_saved_next;
            output_read_rd_en       <= `SD output_read_rd_en_next;
            output_read_rd_addr     <= `SD output_read_rd_addr_next;
            output_read_arr_rd_idx  <= `SD output_read_arr_rd_idx_next;

            // Save the new max address
            output_rd_max_addr      <= `SD output_read_rd_addr_next;

            // Save the new max address until we finish streaming the data
            if(~done) begin
                data_rd_max_addr                      <= `SD data_rd_addr_next;
            end else if(fsb_packet_valid_out & ~fsb_packet_valid_out_first_transition) begin
                data_rd_addr                          <= `SD 0;
                fsb_packet_valid_out_first_transition <= `SD 1;
            end // if(~done)
        end // (reset)
    end // always_ff @(posedge clock)

    // Fill the SRAM
    always_comb begin
        data_wr_en_next           = 0;
        data_wr_data_next         = 0;
        data_wr_addr_next         = data_wr_addr;
        data_sram_arr_wr_idx_next = data_sram_arr_wr_idx;
        for(int i = 0; i < `INPUT_DATA_FILE_READ_WIDTH; i++) begin
            data_sram_arr_wr_idx_b[i] = data_sram_arr_wr_idx + i;
        end // for(int i = 0; i < `INPUT_DATA_FILE_READ_WIDTH; i++)

        // Write input data
        if(data_sram_wr_en) begin
            for(int i = 0; i < `INPUT_DATA_FILE_READ_WIDTH; i++) begin
                data_wr_data_next[data_sram_arr_wr_idx_b[i]] = data_sram_wr_data[i];
                data_wr_en_next[data_sram_arr_wr_idx_b[i]]   = 1;
            end // for(int i = 0; i < `INPUT_DATA_FILE_READ_WIDTH; i++)

            data_sram_arr_wr_idx_next = data_sram_arr_wr_idx + `INPUT_DATA_FILE_READ_WIDTH;

            // Increment address pointer after all channels have been written
            if(data_sram_arr_wr_idx_b[`INPUT_DATA_FILE_READ_WIDTH-1] == (NUM_CHANNELS_X-1)) begin
                for(int i = 0; i < NUM_CHANNELS_X; i++) begin
                    data_wr_addr_next[i] = data_wr_addr[i] + 1;
                end // for(int i = 0; i < NUM_CHANNELS_X; i++)
            end // if(data_sram_arr_wr_idx == (NUM_CHANNELS_X-1))
        end // if(data_sram_wr_en)

        // Write output data
        for(int i = 0; i < NUM_CHANNELS_X; i++) begin
            if(data_en_out[i]) begin
                data_wr_data_next[i] = data_out[i];
                data_wr_en_next[i]   = data_en_out[i];

                data_wr_addr_next[i] = data_wr_addr[i] + 1;
            end // if(data_en_out)
        end // for(int i = 0; i < NUM_CHANNELS_X; i++)
    end // always_comb

    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(reset) begin
            data_wr_en           <= `SD 0;
            data_wr_data         <= `SD 0;
            data_wr_addr         <= `SD 0;
            data_sram_arr_wr_idx <= `SD 0;

            // Reset the max address
            data_wr_max_addr     <= `SD 0;
            output_wr_max_addr   <= `SD 0;
        end else if(data_sram_wr_en) begin
            data_wr_en           <= `SD data_wr_en_next;
            data_wr_data         <= `SD data_wr_data_next;
            data_wr_addr         <= `SD data_wr_addr_next;
            data_sram_arr_wr_idx <= `SD data_sram_arr_wr_idx_next;

            // Save the new max address
            data_wr_max_addr     <= `SD data_wr_addr_next;
        end else if(run) begin
            data_wr_en           <= `SD data_wr_en_next;
            data_wr_data         <= `SD data_wr_data_next;
            data_wr_addr         <= `SD data_wr_addr_next;
            data_sram_arr_wr_idx <= `SD data_sram_arr_wr_idx_next;

            // Save the new max address
            output_wr_max_addr   <= `SD data_wr_addr_next;
        end else begin
            data_wr_en           <= `SD 0;
            data_wr_data         <= `SD 0;
            data_wr_addr         <= `SD 0;
            data_sram_arr_wr_idx <= `SD 0;
        end // (reset)
    end // always_ff @(posedge clock)



//////////////////////////////////////////////
//                                          //
//     PRELOAD INITIALIZATION CONSTANTS     //
//                                          //
//////////////////////////////////////////////
    always_comb begin
        // Default values
        tau_rx_tx2_sram_tau_wr_data_next = 0;
        tau_rx_tx2_sram_tau_wr_en_next   = 0;
        data_sram_wr_data_next           = 0;
        data_sram_wr_en_next             = 0;
        reset_pipelines                  = 0;
        bf_ap_size_next                  = bf_ap_size;
        apod_consts_next                 = apod_consts;
        apod_consts_channel_idx_next     = apod_consts_channel_idx;
        apod_consts_idx_next             = apod_consts_idx;
        tau_rx_tx2_rd_delay_next         = tau_rx_tx2_rd_delay;
        tau_tx1_consts_next              = tau_tx1_consts;
        tau_tx1_consts_channel_idx_next  = tau_tx1_consts_channel_idx;
        tau_tx1_consts_idx_next          = tau_tx1_consts_idx;
        output_select_next               = output_select;
        run_next                         = run;
        done_next                        = done;

        if(run && (data_rd_max_addr == data_wr_max_addr)) begin
            run_next  = 0;
            done_next = 1;
        end // if(run && (data_rd_max_addr == data_wr_max_addr))

        if(fsb_packet_valid) begin
            // Actions based on opcode
            case(fsb_packet.opcode)
                NULL_PACKET : begin
                end
                RESET_PIPELINE_WRAPPER : begin
                end
                LD_BF_AP_SIZE : begin
                    bf_ap_size_next = fsb_packet.data[$clog2(MAX_BF_AP_SIZE)-1:0];
                end
                LD_APOD_CONSTS : begin
                    apod_consts_next[apod_consts_channel_idx][apod_consts_idx] = fsb_packet.data[(IN_OUT_SAMPLE_BIT_WIDTH*`APOD_CONSTS_FILE_READ_WIDTH)-1:0];
                    apod_consts_idx_next                                       = apod_consts_idx + 1;
                    if(apod_consts_idx == ((MAX_BF_AP_SIZE/`APOD_CONSTS_FILE_READ_WIDTH)-1)) begin
                        apod_consts_idx_next         = 0;
                        apod_consts_channel_idx_next = apod_consts_channel_idx + 1;
                    end // if(apod_consts_idx == ((MAX_BF_AP_SIZE/`APOD_CONSTS_FILE_READ_WIDTH)-1))
                end
                LD_TAU_TX1 : begin
                    tau_tx1_consts_next[tau_tx1_consts_channel_idx][tau_tx1_consts_idx] = fsb_packet.data[($clog2(TAU_TX1_MAX_VAL)*`TAU_TX1_FILE_READ_WIDTH)-1:0];
                    tau_tx1_consts_idx_next                                             = tau_tx1_consts_idx + 1;
                    if(tau_tx1_consts_idx == ((MAX_BF_AP_SIZE/`TAU_TX1_FILE_READ_WIDTH)-1)) begin
                        tau_tx1_consts_idx_next         = 0;
                        tau_tx1_consts_channel_idx_next = tau_tx1_consts_channel_idx + 1;
                    end // if(tau_tx1_consts_idx == ((MAX_BF_AP_SIZE/`TAU_TX1_FILE_READ_WIDTH)-1))
                end
                LD_TAU_RX_TX2 : begin
                    tau_rx_tx2_sram_tau_wr_data_next = fsb_packet.data[`TAU_RX_TX2_FILE_READ_WIDTH-1:0];
                    tau_rx_tx2_sram_tau_wr_en_next   = 1;
                end
                LD_TAU_RX_TX2_INIT_DELAY : begin
                    tau_rx_tx2_rd_delay_next = fsb_packet.data[$clog2(TAU_TX1_MAX_VAL)-1:0];
                end
                LD_NUM_SHIFTS : begin
                    data_num_shifts_next = fsb_packet.data[$clog2(MAX_BF_AP_SIZE)-1:0];
                end
                LD_INPUT_DATA : begin
                    data_sram_wr_data_next = fsb_packet.data[(IN_OUT_SAMPLE_BIT_WIDTH*`INPUT_DATA_FILE_READ_WIDTH)-1:0];
                    data_sram_wr_en_next   = 1;
                end
                LD_OUTPUT_SELECT : begin
                    output_select_next = fsb_packet.data[3:0];
                end
                INIT_PIPELINES : begin
                    reset_pipelines = 1;
                end
                RUN_PIPELINES : begin
                    run_next = 1;
                end
            endcase // fsb_packet.opcode
        end // if(fsb_packet_valid)
    end // always_comb

    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(reset) begin
            bf_ap_size                  <= `SD 0;
            apod_consts                 <= `SD 0;
            apod_consts_channel_idx     <= `SD 0;
            apod_consts_idx             <= `SD 0;
            tau_rx_tx2_rd_delay         <= `SD 0;
            tau_rx_tx2_sram_tau_wr_data <= `SD 0;
            tau_rx_tx2_sram_tau_wr_en   <= `SD 0;
            tau_tx1_consts              <= `SD 0;
            tau_tx1_consts_channel_idx  <= `SD 0;
            tau_tx1_consts_idx          <= `SD 0;
            data_num_shifts             <= `SD 0;
            data_sram_wr_data           <= `SD 0;
            data_sram_wr_en             <= `SD 0;
            output_select               <= `SD ACCUM;
            fsb_packet                  <= `SD 0;
            fsb_packet_valid            <= `SD 0;
            run                         <= `SD 0;
            done                        <= `SD 0;
        end else begin
            bf_ap_size                  <= `SD bf_ap_size_next;
            apod_consts                 <= `SD apod_consts_next;
            apod_consts_channel_idx     <= `SD apod_consts_channel_idx_next;
            apod_consts_idx             <= `SD apod_consts_idx_next;
            tau_rx_tx2_rd_delay         <= `SD tau_rx_tx2_rd_delay_next;
            tau_rx_tx2_sram_tau_wr_data <= `SD tau_rx_tx2_sram_tau_wr_data_next;
            tau_rx_tx2_sram_tau_wr_en   <= `SD tau_rx_tx2_sram_tau_wr_en_next;
            tau_tx1_consts              <= `SD tau_tx1_consts_next;
            tau_tx1_consts_channel_idx  <= `SD tau_tx1_consts_channel_idx_next;
            tau_tx1_consts_idx          <= `SD tau_tx1_consts_idx_next;
            data_num_shifts             <= `SD data_num_shifts_next;
            data_sram_wr_data           <= `SD data_sram_wr_data_next;
            data_sram_wr_en             <= `SD data_sram_wr_en_next;
            output_select               <= `SD output_select_next;
            run                         <= `SD run_next;
            done                        <= `SD done_next;

            if(pipeline_wrapper_ready_out & fsb_packet_valid_in) begin
                fsb_packet       <= `SD fsb_packet_in;
                fsb_packet_valid <= `SD 1;
            end else begin
                fsb_packet       <= `SD 0;
                fsb_packet_valid <= `SD 0;
            end// if(pipeline_wrapper_ready_out & fsb_packet_valid_in)
        end // if(reset)
    end // always_ff @(posedge clock)



//////////////////////////////////////////////
//                                          //
//           INSTANTIATE PIPELINE           //
//                                          //
//////////////////////////////////////////////
    // Instantiate the pipeline
    pipeline #(.IN_OUT_SAMPLE_BIT_WIDTH(IN_OUT_SAMPLE_BIT_WIDTH), .PIPELINE_SAMPLE_BIT_WIDTH(PIPELINE_SAMPLE_BIT_WIDTH), .BITSTREAM_BUFFER_LEN(BITSTREAM_BUFFER_LEN), .TAU_TX1_MAX_VAL(TAU_TX1_MAX_VAL), .MAX_BF_AP_SIZE(MAX_BF_AP_SIZE), .REG_FILE_X(REG_FILE_X), .REG_FILE_Y(REG_FILE_Y), .NUM_BANKS(NUM_BANKS), .NUM_BANK_ENTRIES(NUM_BANK_ENTRIES), .MAC_BUFFER_ENTRIES(MAC_BUFFER_ENTRIES), .NUM_STAGES(NUM_STAGES), .NUM_CHANNELS_X(NUM_CHANNELS_X)) pipeline_0 (

        // Inputs
        .clock(clock),
        .reset(reset_pipelines),
        .output_select_in(output_select),
        .sample_in(data),
        .sample_en_in(data_en),
        .data_tau_tx1_in(tau_tx1_consts),
        .tau_rx_tx2_in(tau_rx_tx2),
        .tau_rx_tx2_en_in(tau_rx_tx2_en),
        .bf_ap_size_in(bf_ap_size),
        .apod_consts_in(apod_consts),

        // Outputs
        .data_out(data_out),
        .data_en_out(data_en_out)

    );

endmodule // pipeline
