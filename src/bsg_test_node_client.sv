//==========================================================================
// bsg_test_node_client
//==========================================================================
// This is a test node client that contains a 16-bit GCD unit.
//
// This bsg_design was created by Cornell during the DARPA Craft project
// to better understand bsg_designs and their testing infrastructure.
//
// Date   : January 14, 2017
// Author : Christopher Torng (clt67@cornell.edu)
//
//`define MEM_470

module  bsg_test_node_client import bsg_fsb_pkg::*; #(parameter ring_width_p="inv", parameter master_id_p="inv", parameter client_id_p="inv") (
        input clk_i,
        input reset_i,

        // control
        input en_i,

        // input channel
        input  v_i,
        input [ring_width_p-1:0] data_i,
        output ready_o,

        // output channel
        output v_o,
        output [ring_width_p-1:0] data_o,
        input yumi_i   // late
    );

   localparam debug_lp=1;

   // synopsys translate_off
   if (debug_lp)
     begin
        always @(negedge clk_i)
          if (v_i & ready_o)
            $display("## bsg_test_node_client received %x",data_i);

        always @(negedge clk_i)
          if (v_o & yumi_i)
            $display("## bsg_test_node_client sent %x",data_o);
     end
   // synopsys translate_on

   // the default interface gives all design
   // control to the switch: you have to say
   // ahead of time if you can receive data
   // and it won't tell you until the last minute
   // if it took your data.

   // we reverse the situation by having an
   // input and output fifo. these
   // are not required, but make the hw
   // design easier at the cost of some
   // area and latency.

   logic                   in_fifo_v;
   bsg_fsb_pkt_s           in_fifo_data;
   logic                   in_fifo_yumi;

   logic                   out_fifo_ready;
   bsg_fsb_pkt_s           out_fifo_data;
   logic                   out_fifo_v;

   logic                   outerspace_top_ack;

    //---------------------------------------------------------------------
    // Input FIFO
    //---------------------------------------------------------------------
    bsg_two_fifo #(.width_p(ring_width_p)) fifo_in (
        .clk_i(clk_i),
        .reset_i(reset_i),

        .v_i(v_i),
        .data_i(data_i),

        .v_o(in_fifo_v),
        .data_o(in_fifo_data),
        .ready_o(ready_o),

        .yumi_i(in_fifo_yumi)
    );


    assign in_fifo_yumi = in_fifo_v & en_i & outerspace_top_ack;

    outerspace_top OUTERSPACE_TOP (
        .clock(clk_i),
        .reset(reset_i),

        // Input packet
        .fsb2fsb_ctrl_data(in_fifo_data),
        .fsb2fsb_ctrl_valid(in_fifo_v),
        // Output buffer not full
        .fsb2fsb_ctrl_ready(out_fifo_ready),
        // Output packet
        .fsb_ctrl2fsb_data(out_fifo_data),
        .fsb_ctrl2fsb_valid(out_fifo_v),
        // Input packet accepted
        .fsb_ctrl2fsb_yumi(outerspace_top_ack)
    );

    pipeline_wrapper #(.IN_OUT_SAMPLE_BIT_WIDTH(IN_OUT_SAMPLE_BIT_WIDTH), .PIPELINE_SAMPLE_BIT_WIDTH(PIPELINE_SAMPLE_BIT_WIDTH), .BITSTREAM_BUFFER_LEN(BITSTREAM_BUFFER_LEN), .TAU_TX1_MAX_VAL(TAU_TX1_MAX_VAL), .MAX_BF_AP_SIZE(MAX_BF_AP_SIZE), .REG_FILE_X(REG_FILE_X), .REG_FILE_Y(REG_FILE_Y), .NUM_BANKS(NUM_BANKS), .NUM_SRAM_BANKS(NUM_SRAM_BANKS), .NUM_BANK_ENTRIES(NUM_BANK_ENTRIES), .NUM_STAGES(NUM_STAGES), .NUM_CHANNELS_X(NUM_CHANNELS_X)) pipeline_wrap (

        // Inputs
        .clock(clk_i),
        .reset(reset_i),
        .run(run), // ***REMOVE THIS***
        .read_output(read_output),
        // Input packet
        .fsb_packet_in(in_fifo_data),
        .fsb_packet_valid_in(in_fifo_v),
        // Output buffer not full
        .fsb2fsb_ctrl_ready(out_fifo_ready),

        // Outputs
        .done(done), // ***REMOVE THIS***
        .read_done(read_done), // ***REMOVE THIS***
        // Output packet
        .fsb_packet_out(out_fifo_data),
        .fsb_packet_valid_out(out_fifo_v),
        // Input packet accepted
        .fsb_ctrl2fsb_yumi(pipeline_wrapper_top_ack)

    );

    //---------------------------------------------------------------------
    // Output FIFO
    //---------------------------------------------------------------------
    bsg_two_fifo #(.width_p(ring_width_p)) fifo_out (
        .clk_i(clk_i),
        .reset_i(reset_i),

        .v_i(out_fifo_v),
        .data_i(out_fifo_data),

        .v_o(v_o),
        .data_o(data_o),
        .ready_o(out_fifo_ready),

        .yumi_i(yumi_i)
    );

endmodule // bsg_test_node_client
