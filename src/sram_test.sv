/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  sram_test.sv                                        //
//                                                                     //
//  Description :  Instantiates a single 512kb SRAM.                   //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module sram_test #(parameter SAMPLE_BIT_WIDTH = 10, parameter MAX_BF_AP_SIZE = 32, parameter BITSTREAM_BUFFER_LEN = 64, parameter TAU_TX1_MAX_VAL = 1024) (

        // Inputs
        input                                                   clock,
        input                                                   rd_en,
        input                                                   wr_en,
        input [12:0]                                            rd_addr,
        input [12:0]                                            wr_addr,
        input [63:0]                                            wr_data,

        // Outputs
        output logic [63:0] rd_data

    );

    sram_512kilobit (

        // Inputs
        .CLK(clock), // clock - ADD "~" FOR SIMULATION; REMOVE FOR SYNTHESIS
        .CENA(~rd_en), // read port chip enable
        .CENB(~wr_en), // write port chip enable
        .AA(rd_addr), // read port address
        .AB(wr_addr), // write port address
        .DB(wr_data), // write port data
        .STOV(1'b0), // When the STOV and STOVAB pins are HIGH, the internal clock pulse for port A is generated directly from the high phase of the external clock input
        .STOVAB(1'b0), // When the STOV pin is HIGH and the STOVAB pin is LOW, the internal clock pulse for port B is generated directly from the high phase of the external clock input.
        .EMA(3'b010), // extra margin adjustment - Austin uses 010; try 010 (default)
        .EMAW(2'b01), // similar to EMA; try 01 (default)
        .EMAS(1'b0), // default is LOW
        .EMAP(1'b0), // default is LOW
        .RET1N(1'b1), // retention mode; try 1?

        // Outputs
        .QA(rd_data) // output data for A

    );
endmodule // sram_test
