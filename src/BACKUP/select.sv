/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  select.sv                                           //
//                                                                     //
//  Description :  Reads delay bitstream and selects samples from the  //
//                 rotate buffer.                                      //
//                                                                     //
//  NOTE #1: TAU_TX1_MAX_VAL cannot exceed (BITSTREAM_BUFFER_LEN-4),   //
//           and BITSTREAM_BUFFER_LEN must be evenly divisible by 4.   //
//                                                                     //
//  NOTE #2: tau_rx_tx2 head must be >= 4 before rotate_en_in first    //
//           becomes valid.                                            //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

`timescale 1ns/100ps

module select #(parameter SAMPLE_BIT_WIDTH = 12, parameter MAX_BF_AP_SIZE = 32, parameter BITSTREAM_BUFFER_LEN = 1024, parameter TAU_TX1_MAX_VAL = BITSTREAM_BUFFER_LEN-4) (

        // Inputs
        input                               clock,
        input                               reset,
        input [3:0][SAMPLE_BIT_WIDTH-1:0]   rotate_in,
        input                               rotate_en_in,
        input [$clog2(TAU_TX1_MAX_VAL)-1:0] tau_tx1_in,
        input [3:0]                         tau_rx_tx2_in,
        input                               tau_rx_tx2_en_in,
        input [$clog2(MAX_BF_AP_SIZE)-1:0]  rotate_bf_ap_idx_in,

        // Outputs
        output logic [3:0][SAMPLE_BIT_WIDTH-1:0]  select_out,
        output logic [3:0]                        select_en_out,
        output logic [$clog2(MAX_BF_AP_SIZE)-1:0] select_bf_ap_idx_out

    );

    // Local variables
    logic [3:0][SAMPLE_BIT_WIDTH-1:0]        buffer, buffer_next;
    logic                                    buffer_en, buffer_en_next;
    logic [BITSTREAM_BUFFER_LEN-1:0]         tau_rx_tx2, tau_rx_tx2_next;
    logic [$clog2(BITSTREAM_BUFFER_LEN)-1:0] head, head_next;
    logic [$clog2(BITSTREAM_BUFFER_LEN)-1:0] tail, tail_next;
    logic [$clog2(BITSTREAM_BUFFER_LEN)-1:0] tail_1, tail_2, tail_3;
    logic [3:0]                              select_bits;
    logic [3:0]                              select_en, select_en_next;
    logic [$clog2(4)-1:0]                    num_outputs; // used to pack the outputs
    logic [$clog2(MAX_BF_AP_SIZE)-1:0]       bf_ap_idx, bf_ap_idx_next;

    // Assign outputs
    assign select_out[0]        = buffer[0];
    assign select_out[1]        = buffer[1];
    assign select_out[2]        = buffer[2];
    assign select_out[3]        = buffer[3];
    assign select_en_out        = (buffer_en & ~reset) ? select_en : 0;
    assign select_bf_ap_idx_out = bf_ap_idx;

    always_comb begin
        tau_rx_tx2_next = tau_rx_tx2;
        head_next       = head;
        tail_next       = tail;
        tail_1          = 0;
        tail_2          = 0;
        tail_3          = 0;
        select_bits     = tau_rx_tx2[head+:4];
        select_en_next  = 0;
        bf_ap_idx_next  = 0; // ADDED
        buffer_next     = 0;
        buffer_en_next  = 0;
        num_outputs     = 0;

        // Select the appropriate samples
        if(rotate_en_in) begin
            if(select_bits[0] == 1'b1) begin
                buffer_next[num_outputs]    = rotate_in[0];
                select_en_next[num_outputs] = 1;
                num_outputs++;
            end
            if(select_bits[1] == 1'b1) begin
                buffer_next[num_outputs]    = rotate_in[1];
                select_en_next[num_outputs] = 1;
                num_outputs++;
            end
            if(select_bits[2] == 1'b1) begin
                buffer_next[num_outputs]    = rotate_in[2];
                select_en_next[num_outputs] = 1;
                num_outputs++;
            end
            if(select_bits[3] == 1'b1) begin
                buffer_next[num_outputs]    = rotate_in[3];
                select_en_next[num_outputs] = 1;
                num_outputs++;
            end

            // Increment the head pointer by 4
            head_next = head + 4;
            // if((head + 4) >= BITSTREAM_BUFFER_LEN) begin
            //     head_next = 0;
            // end else begin
            //     head_next = head + 4;
            // end // if((head + 4) >= BITSTREAM_BUFFER_LEN)

            // Save bf_ap_idx
            bf_ap_idx_next = rotate_bf_ap_idx_in;
        end // if(rotate_en_in)

        // ***********************************CHECK THIS***********************************
        buffer_en_next = rotate_en_in;

        // Add to the tau_rx_tx2 buffer
        if(tau_rx_tx2_en_in) begin
            // Add the first bit
            tau_rx_tx2_next[tail]   = tau_rx_tx2_in[0];

            // Add the second bit
            tail_1                  = tail + 1;
            tau_rx_tx2_next[tail_1] = tau_rx_tx2_in[1];
            // if((tail + 1) >= BITSTREAM_BUFFER_LEN) begin
            //     tau_rx_tx2_next[tail + 1 - BITSTREAM_BUFFER_LEN] = tau_rx_tx2_in[1];
            // end else begin
            //     tau_rx_tx2_next[tail + 1] = tau_rx_tx2_in[1];
            // end // if((tail + 1) >= BITSTREAM_BUFFER_LEN)

            // Add the third bit
            tail_2                  = tail + 2;
            tau_rx_tx2_next[tail_2] = tau_rx_tx2_in[2];
            // if((tail + 2) >= BITSTREAM_BUFFER_LEN) begin
            //     tau_rx_tx2_next[tail + 2 - BITSTREAM_BUFFER_LEN] = tau_rx_tx2_in[2];
            // end else begin
            //     tau_rx_tx2_next[tail + 2] = tau_rx_tx2_in[2];
            // end // if((tail + 2) >= BITSTREAM_BUFFER_LEN)

            // Add the fourth bit
            tail_3                  = tail + 3;
            tau_rx_tx2_next[tail_3] = tau_rx_tx2_in[3];
            // if((tail + 3) >= BITSTREAM_BUFFER_LEN) begin
            //     tau_rx_tx2_next[tail + 3 - BITSTREAM_BUFFER_LEN] = tau_rx_tx2_in[3];
            // end else begin
            //     tau_rx_tx2_next[tail + 3] = tau_rx_tx2_in[3];
            // end // if((tail + 3) >= BITSTREAM_BUFFER_LEN)

            // Increment the tail pointer by 4
            tail_next               = tail + 4;
            // if((tail + 4) >= BITSTREAM_BUFFER_LEN) begin
            //     tail_next = tail + 4 - BITSTREAM_BUFFER_LEN;
            // end else begin
            //     tail_next = tail + 4;
            // end // if((tail + 4) >= BITSTREAM_BUFFER_LEN)
        end // if(tau_rx_tx2_en_in)
    end // always_comb

    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(reset) begin
            buffer      <= `SD 0;
            buffer_en   <= `SD 0;
            tau_rx_tx2  <= `SD 0;
            head        <= `SD 0;
            tail        <= `SD tau_tx1_in; // initialize tail to tau_tx1 for offset
            select_en   <= `SD 0;
            bf_ap_idx   <= `SD 0;
        end else begin
            buffer      <= `SD buffer_next;
            buffer_en   <= `SD buffer_en_next;
            tau_rx_tx2  <= `SD tau_rx_tx2_next;
            head        <= `SD head_next;
            tail        <= `SD tail_next;
            select_en   <= `SD select_en_next;
            bf_ap_idx   <= `SD bf_ap_idx_next;
        end // if(reset) begin
    end // always_ff @(posedge clock)
endmodule // select
