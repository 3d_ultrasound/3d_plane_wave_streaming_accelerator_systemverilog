/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  apod.sv                                             //
//                                                                     //
//  Description :  Apodizes selected samples.                          //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

`timescale 1ns/100ps

module apod #(parameter SAMPLE_BIT_WIDTH = 32, parameter MAX_BF_AP_SIZE = 32, parameter NUM_STAGES = 8) (

        // Inputs
        input                                            clock,
        input                                            reset,
        input [SAMPLE_BIT_WIDTH-1:0]                     regfile_in,
        input                                            regfile_en_in,
        input                                            regfile_bf_ap_done_in,
        input [MAX_BF_AP_SIZE-1:0][SAMPLE_BIT_WIDTH-1:0] apod_consts_in,

        // Outputs
        output logic [SAMPLE_BIT_WIDTH-1:0] apod_out,
        output logic                        apod_en_out,
        output logic                        apod_bf_ap_done_out

    );

    // Local variables
    logic [SAMPLE_BIT_WIDTH-1:0]                     mult_result; // holds the apodized value
    logic                                            mult_done; // holds the apodized value
    logic [NUM_STAGES-1:0]                           bf_ap_done, bf_ap_done_next; // signals that the output value is ready
    logic [$clog2(MAX_BF_AP_SIZE)-1:0]               bf_ap_idx, bf_ap_idx_next; // holds the output value after accumulation
    logic [MAX_BF_AP_SIZE-1:0][SAMPLE_BIT_WIDTH-1:0] apod_consts; // holds the bf aperture size

    // Assign outputs
    assign apod_out            = mult_done ? mult_result : 0;
    assign apod_en_out         = mult_done;
    assign apod_bf_ap_done_out = bf_ap_done[0];

    always_comb begin
        bf_ap_done_next[NUM_STAGES-2:0] = bf_ap_done[NUM_STAGES-1:1];
        bf_ap_done_next[NUM_STAGES-1]   = regfile_bf_ap_done_in;
        bf_ap_idx_next                  = bf_ap_idx;

        // Add sample to the accumulator
        if(regfile_en_in) begin
            bf_ap_idx_next = bf_ap_idx + 1;
        end // if(regfile_en_in)

        if(regfile_bf_ap_done_in) begin
            bf_ap_idx_next = 0;
        end // if(regfile_bf_ap_done_in)
    end // always_comb

    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(reset) begin
            bf_ap_idx   <= `SD 0;
            bf_ap_done  <= `SD 0;
            apod_consts <= `SD apod_consts_in; // initialize the apodization constant array
        end else begin
            bf_ap_idx  <= `SD bf_ap_idx_next;
            bf_ap_done <= `SD bf_ap_done_next;
        end // if(reset) begin
    end // always_ff @(posedge clock)

    //
    // Instantiate the multiplication pipeline
    //
    mult #(.SAMPLE_BIT_WIDTH(SAMPLE_BIT_WIDTH), .NUM_STAGES(NUM_STAGES)) pipe_mult_0 (

        // Inputs
        .clock(clock),
        .reset(reset),
        .mcand(apod_consts[bf_ap_idx]),
        .mplier(regfile_in),
        .start(regfile_en_in),

        // Outputs
        .product(mult_result),
        .done(mult_done)

    );

endmodule // apod