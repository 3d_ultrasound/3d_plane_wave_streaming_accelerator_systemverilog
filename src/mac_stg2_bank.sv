/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  mac_stg2.sv                                         //
//                                                                     //
//  Description :  Performs Multiply-and-Accumulate operations; stores //
//                 partially-accumulated values in buffer, which is a  //
//                 virtual 2D register file.                           //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module mac_stg2_bank #(parameter SAMPLE_BIT_WIDTH = 12, parameter PIPELINE_SAMPLE_BIT_WIDTH = 10, parameter MAX_BF_AP_SIZE = 32, parameter MAC_BUFFER_ENTRIES = 128, parameter NUM_WAYS = 4, parameter NUM_BANKS = 4, parameter NUM_BANK_ENTRIES = (MAC_BUFFER_ENTRIES/NUM_BANKS)) (

        // Inputs
        input                                               clock,
        input                                               reset,
        input [NUM_WAYS-1:0][PIPELINE_SAMPLE_BIT_WIDTH-1:0] mac_stg1_in,
        input [NUM_WAYS-1:0]                                mac_stg1_en_in,
        input [$clog2(MAX_BF_AP_SIZE)-1:0]                  mac_stg1_bf_ap_idx_in,
        input [$clog2(MAX_BF_AP_SIZE)-1:0]                  bf_ap_size_in,

        // Outputs
        output logic [SAMPLE_BIT_WIDTH-1:0] mac_stg2_out,
        output logic                        mac_stg2_en_out

    );

    // Local variables
    logic [$clog2(MAX_BF_AP_SIZE):0] bf_ap_size;

    // Saved variables
    logic [NUM_WAYS-1:0][PIPELINE_SAMPLE_BIT_WIDTH-1:0] mac_stg1_saved, mac_stg1_saved_next;
    logic [NUM_WAYS-1:0]                                mac_stg1_en_saved, mac_stg1_en_saved_next; // saved input for second cycle accumulation of apodized values


    // Variables for writing data
    logic [NUM_BANKS-1:0][$clog2(NUM_BANKS)-1:0]        wr_bank_idx_temp; // bank index for each write value; used as an index into wr_addr
    logic [NUM_BANKS-1:0][$clog2(NUM_BANK_ENTRIES)-1:0] wr_addr, wr_addr_next; // index within each bank for the write values
    logic [NUM_BANKS-1:0][SAMPLE_BIT_WIDTH-1:0]         wr_data, wr_data_next; // data to be written to associated addresses
    logic [NUM_BANKS-1:0]                               wr_en, wr_en_next; // write enable bits for each bank

    // Variables for reading data
    logic [$clog2(MAX_BF_AP_SIZE)-1:0]                  rd_col_pointer, rd_col_pointer_next;
    logic [$clog2(NUM_BANKS)-1:0]                       rd_bank_idx_test, rd_bank_idx, rd_bank_idx_saved; // bank index for the single read value
    logic [$clog2(NUM_BANK_ENTRIES)-1:0]                rd_addr; // index within the bank for the single read value
    logic [NUM_BANKS-1:0][SAMPLE_BIT_WIDTH-1:0]         rd_data; // data read from associated addresses
    logic [SAMPLE_BIT_WIDTH-1:0]                        rd_data_saved; // index within the bank for each write value
    logic                                               rd_en, rd_en_next, rd_en_saved, rd_en_saved_next;
    logic                                               bf_ap_done, bf_ap_done_next, bf_ap_done_saved, bf_ap_done_saved_next;


    // MAC buffer variables
    logic [MAC_BUFFER_ENTRIES-1:0][SAMPLE_BIT_WIDTH-1:0]       mac_buffer, mac_buffer_next; // holds the values staged for output
    logic [MAC_BUFFER_ENTRIES-1:0][$clog2(MAX_BF_AP_SIZE):0]   mac_count, mac_count_next; // holds the current number of accumulated values per mac buffer entry
    logic [$clog2(MAC_BUFFER_ENTRIES)-1:0]                     col_head, col_head_next; // SHARED head (points to mac buffer index at "head")
    logic [MAX_BF_AP_SIZE-1:0][$clog2(MAC_BUFFER_ENTRIES)-1:0] col_tail, col_tail_next; // holds number of values stored per bf_ap_idx (tail of virtual columns)
    logic [NUM_WAYS-1:0][$clog2(MAC_BUFFER_ENTRIES)-1:0]       temp_tail, temp_tail_next; // used to index into mac buffer
    logic [SAMPLE_BIT_WIDTH-1:0]                               buffer, buffer_next; // holds the values staged for output
    logic                                                      buffer_en, buffer_en_next; // holds the enable bit staged for output

    logic [$clog2(MAX_BF_AP_SIZE)-1:0]                         mac_stg1_bf_ap_idx_saved, mac_stg1_bf_ap_idx_saved_next;
    logic [NUM_WAYS-1:0]                                       mac_stg1_en_saved_2, mac_stg1_en_saved_2_next;
    logic [NUM_WAYS-1:0][PIPELINE_SAMPLE_BIT_WIDTH-1:0]        mac_stg1_saved_2, mac_stg1_saved_2_next;
    logic [NUM_WAYS-1:0][SAMPLE_BIT_WIDTH-1:0]                 add_value, add_value_next;
    logic [NUM_WAYS-1:0][$clog2(MAC_BUFFER_ENTRIES)-1:0]       add_tail, add_tail_next; // used to index into mac buffer
    logic [NUM_WAYS-1:0]                                       add_en, add_en_next; // used to index into mac buffer


    `ifdef USE_MAC_SRAM
    `ifdef USE_SRAM_14
    // GF 14nm
    rf_2p_hsc_12b bank_array [NUM_BANKS-1:0] (

        // Inputs
        .CLKA(clock), // clock - ADD "~" FOR SIMULATION; REMOVE FOR SYNTHESIS
        .CENA(~rd_en_next), // read port chip enable
        .CENB(~wr_en), // write port chip enable
        .CLKB(clock), // clock - ADD "~" FOR SIMULATION; REMOVE FOR SYNTHESIS
        .AA(rd_addr), // read port address
        .AB(wr_addr), // write port address
        .DB(wr_data), // write port data
        .STOV(1'b0), // When the STOV and STOVAB pins are HIGH, the internal clock pulse for port A is generated directly from the high phase of the external clock input
        .EMAA(3'b010), // extra margin adjustment - Austin uses 010; try 010 (default)
        .EMAB(3'b010), // similar to EMAA; try 010 (default)
        .EMASA(1'b0), // default is LOW
        .RET1N(1'b1), // retention mode; try 1?

        // Outputs
        .QA(rd_data) // output data for A

    );
    `endif
    `else
    // Instantiate a bank array of size NUM_BANKS
    bank #(.SAMPLE_BIT_WIDTH(SAMPLE_BIT_WIDTH), .NUM_BANK_ENTRIES(NUM_BANK_ENTRIES)) bank_array [NUM_BANKS-1:0] (

        // Inputs
        .clock(clock),
        .rd_idx(rd_addr), // read index
        .wr_idx(wr_addr), // write index
        .wr_data(wr_data), // write data
        .wr_en(wr_en), // write enable

        // Outputs
        .rd_out(rd_data) // read data

    );
    `endif

    // Assign outputs
    assign mac_stg2_out    = buffer;
    assign mac_stg2_en_out = buffer_en;

    // Write Address Calculation
    always_comb begin
        // Set default values for this cycle
        mac_buffer_next               = mac_buffer;
        mac_count_next                = mac_count;
        col_head_next                 = col_head;
        col_tail_next                 = col_tail;
        mac_stg1_saved_next           = mac_stg1_in;
        mac_stg1_en_saved_next        = mac_stg1_en_in;
        buffer_next                   = 0;
        buffer_en_next                = 0;
        temp_tail_next                = 0;

        mac_stg1_bf_ap_idx_saved_next = mac_stg1_bf_ap_idx_in;
        mac_stg1_en_saved_2_next      = mac_stg1_en_saved;
        mac_stg1_saved_2_next         = mac_stg1_saved;
        add_value_next                = 0;
        add_tail_next                 = temp_tail;
        add_en_next                   = mac_stg1_en_saved_2;

        // Perform the accumulate operation
        for(int i = 0; i < NUM_WAYS; i++) begin
            if(mac_stg1_en_saved[i]) begin
                // Set the tail which will be used to index into the mac buffer next cycle
                if((col_tail[mac_stg1_bf_ap_idx_saved] + i) >= MAC_BUFFER_ENTRIES) begin
                    temp_tail_next[i] = col_tail[mac_stg1_bf_ap_idx_saved] + i - MAC_BUFFER_ENTRIES;
                end else begin
                    temp_tail_next[i] = col_tail[mac_stg1_bf_ap_idx_saved] + i;
                end

                // Increment the tail pointer for this virtual column by the number of valid samples being saved
                if((col_tail[mac_stg1_bf_ap_idx_saved] + i + 1) >= MAC_BUFFER_ENTRIES) begin
                    col_tail_next[mac_stg1_bf_ap_idx_saved] = col_tail[mac_stg1_bf_ap_idx_saved] + i + 1 - MAC_BUFFER_ENTRIES;
                end else begin
                    col_tail_next[mac_stg1_bf_ap_idx_saved] = col_tail[mac_stg1_bf_ap_idx_saved] + i + 1;
                end
            end // if(mac_stg1_en_saved[i])

            add_value_next[i] = $signed(mac_buffer[temp_tail[i]]) + $signed(mac_stg1_saved_2[i]);
            for(int j = 0; j < NUM_WAYS; j++) begin
                if(mac_stg1_en_saved_2[i]) begin
                    if(add_en[j]) begin
                        if(temp_tail[i] == add_tail[j]) begin
                            add_value_next[i] = $signed(add_value[j]) + $signed(mac_stg1_saved_2[i]);
                        end // if(temp_tail[i] == add_tail[j])
                    end // if(add_en[j])
                end // if(mac_stg1_en_saved_2[i])
            end // for(int j = 0; j < NUM_WAYS; j++)

            // Store the accumulated values in the buffer
            if(add_en[i]) begin
                // Add the multiplication result to the mac buffer entry
                mac_buffer_next[add_tail[i]] = add_value[i];

                // Increment mac buffer entry count
                mac_count_next[add_tail[i]]  = mac_count[add_tail[i]] + 1;
            end // if(add_en[i])
        end // for(int i = 0; i < NUM_WAYS; i++)

        buffer_next = mac_buffer[col_head];
        // Read completed voxels
        if(mac_count[col_head] == bf_ap_size) begin
            buffer_en_next            = 1;

            mac_buffer_next[col_head] = 0;

            mac_count_next[col_head]  = 0;

            if((col_head + 1) >= MAC_BUFFER_ENTRIES) begin
                col_head_next = col_head + 1 - MAC_BUFFER_ENTRIES;
            end else begin
                col_head_next = col_head + 1;
            end
        end // if(mac_count[col_head] == bf_ap_size)
    end // always_comb

    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(reset) begin
            bf_ap_size               <= `SD bf_ap_size_in+1; // +1 because we're counting things, and zero is empty; TODO: make this cleaner
            mac_buffer               <= `SD 0;
            mac_count                <= `SD 0;
            col_head                 <= `SD 0;
            col_tail                 <= `SD 0;
            buffer                   <= `SD 0;
            buffer_en                <= `SD 0;
            temp_tail                <= `SD 0;
            mac_stg1_saved           <= `SD 0;
            mac_stg1_en_saved        <= `SD 0;

            mac_stg1_bf_ap_idx_saved <= `SD 0;
            mac_stg1_en_saved_2      <= `SD 0;
            mac_stg1_saved_2         <= `SD 0;
            add_value                <= `SD 0;
            add_tail                 <= `SD 0;
            add_en                   <= `SD 0;
        end else begin
            mac_buffer               <= `SD mac_buffer_next;
            mac_count                <= `SD mac_count_next;
            col_head                 <= `SD col_head_next;
            col_tail                 <= `SD col_tail_next;
            buffer                   <= `SD buffer_next;
            buffer_en                <= `SD buffer_en_next;
            temp_tail                <= `SD temp_tail_next;
            mac_stg1_saved           <= `SD mac_stg1_saved_next;
            mac_stg1_en_saved        <= `SD mac_stg1_en_saved_next;

            mac_stg1_bf_ap_idx_saved <= `SD mac_stg1_bf_ap_idx_saved_next;
            mac_stg1_en_saved_2      <= `SD mac_stg1_en_saved_2_next;
            mac_stg1_saved_2         <= `SD mac_stg1_saved_2_next;
            add_value                <= `SD add_value_next;
            add_tail                 <= `SD add_tail_next;
            add_en                   <= `SD add_en_next;
        end // if(reset) begin
    end // always_ff @(posedge clock)
endmodule // mac_stg2_bank
