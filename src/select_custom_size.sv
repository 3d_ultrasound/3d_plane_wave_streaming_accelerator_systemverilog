/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  select.sv                                           //
//                                                                     //
//  Description :  Reads delay bitstream and selects samples from the  //
//                 rotate buffer.                                      //
//                                                                     //
//  NOTE #1: TAU_TX1_MAX_VAL cannot exceed (BITSTREAM_BUFFER_LEN-4),   //
//           and BITSTREAM_BUFFER_LEN must be evenly divisible by 4.   //
//                                                                     //
//  NOTE #2: tau_rx_tx2 head must be >= 4 before rotate_en_in first    //
//           becomes valid.                                            //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module select #(parameter SAMPLE_BIT_WIDTH = 10, parameter MAX_BF_AP_SIZE = 32, parameter BITSTREAM_BUFFER_LEN = 64, parameter TAU_TX1_MAX_VAL = 1024) (

        // Inputs
        input                                                   clock,
        input                                                   reset,
        input [3:0][SAMPLE_BIT_WIDTH-1:0]                       rotate_in,
        input                                                   rotate_en_in,
        input [MAX_BF_AP_SIZE-1:0][$clog2(TAU_TX1_MAX_VAL)-1:0] tau_tx1_in,
        input [3:0]                                             tau_rx_tx2_in,
        input                                                   tau_rx_tx2_en_in,
        input [$clog2(MAX_BF_AP_SIZE)-1:0]                      rotate_bf_ap_idx_in,
        input [$clog2(MAX_BF_AP_SIZE)-1:0]                      bf_ap_size_in,

        // Outputs
        output logic [3:0][SAMPLE_BIT_WIDTH-1:0]  select_out,
        output logic [3:0]                        select_en_out,
        output logic [$clog2(MAX_BF_AP_SIZE)-1:0] select_bf_ap_idx_out

    );

    // Local variables
    logic [$clog2(MAX_BF_AP_SIZE)-1:0]                           bf_ap_size;
    logic [3:0][SAMPLE_BIT_WIDTH-1:0]                            buffer, buffer_next;
    logic [MAX_BF_AP_SIZE-1:0][BITSTREAM_BUFFER_LEN-1:0]         tau_rx_tx2, tau_rx_tx2_next;
    logic [BITSTREAM_BUFFER_LEN-1:0]                             tau_rx_tx2_col, tau_rx_tx2_col_next;
    logic [MAX_BF_AP_SIZE-1:0][$clog2(BITSTREAM_BUFFER_LEN)-1:0] head, head_next;
    logic [$clog2(BITSTREAM_BUFFER_LEN)-1:0]                     head_temp, head_temp_next;
    logic [MAX_BF_AP_SIZE-1:0][$clog2(BITSTREAM_BUFFER_LEN)-1:0] tail, tail_next;
    logic [$clog2(BITSTREAM_BUFFER_LEN)-1:0]                     tail_temp;
    logic [$clog2(BITSTREAM_BUFFER_LEN)-1:0]                     head_0, head_1, head_2, head_3, head_0_next, head_1_next, head_2_next, head_3_next;
    logic [$clog2(BITSTREAM_BUFFER_LEN)-1:0]                     tail_0, tail_1, tail_2, tail_3;
    logic [3:0]                                                  select_bits, select_bits_next;
    logic [3:0]                                                  select_en, select_en_next;
    logic [$clog2(4)-1:0]                                        num_outputs; // used to pack the outputs
    logic [$clog2(MAX_BF_AP_SIZE)-1:0]                           bf_ap_idx, bf_ap_idx_next, bf_ap_idx_saved, bf_ap_idx_saved_next;
    logic [$clog2(MAX_BF_AP_SIZE)-1:0]                           tau_rx_tx2_bf_ap_idx, tau_rx_tx2_bf_ap_idx_next;
    logic [MAX_BF_AP_SIZE-1:0][$clog2(TAU_TX1_MAX_VAL)-1-2:0]    decrementer, decrementer_next; // holds the tau_tx1 offset values; data in sets of 4 -> lower two bits dropped
    logic [$clog2(TAU_TX1_MAX_VAL)-1-2:0]                        decrementer_temp;
    logic [3:0][SAMPLE_BIT_WIDTH-1:0]                            rotate_saved, rotate_saved_next;
    logic                                                        rotate_en_saved, rotate_en_saved_next;

    logic [3:0][SAMPLE_BIT_WIDTH-1:0]                            rotate_saved_2, rotate_saved_2_next;
    logic                                                        rotate_en_saved_2, rotate_en_saved_2_next;
    logic [3:0]                                                  tau_rx_tx2_saved, tau_rx_tx2_saved_next;
    logic                                                        tau_rx_tx2_en_saved, tau_rx_tx2_en_saved_next;
    logic [$clog2(MAX_BF_AP_SIZE)-1:0]                           rotate_bf_ap_idx_saved, rotate_bf_ap_idx_saved_next;


    logic [3:0][SAMPLE_BIT_WIDTH-1:0]                            rotate_saved_3, rotate_saved_3_next;
    logic                                                        rotate_en_saved_3, rotate_en_saved_3_next;
    logic [$clog2(MAX_BF_AP_SIZE)-1:0]                           rotate_bf_ap_idx_saved_2, rotate_bf_ap_idx_saved_2_next;

    // Assign outputs
    assign select_out           = buffer;
    assign select_en_out        = select_en;
    assign select_bf_ap_idx_out = bf_ap_idx_saved;

    always_comb begin
        // Set default values for this cycle
        tau_rx_tx2_next             = tau_rx_tx2;
        head_next                   = head;
        tail_next                   = tail;
        if((tail[tau_rx_tx2_bf_ap_idx]) >= BITSTREAM_BUFFER_LEN) begin
            tail_0 = tail[tau_rx_tx2_bf_ap_idx] - BITSTREAM_BUFFER_LEN;
        end else begin
            tail_0 = tail[tau_rx_tx2_bf_ap_idx];
        end
        if((tail[tau_rx_tx2_bf_ap_idx] + 1) >= BITSTREAM_BUFFER_LEN) begin
            tail_1 = tail[tau_rx_tx2_bf_ap_idx] + 1 - BITSTREAM_BUFFER_LEN;
        end else begin
            tail_1 = tail[tau_rx_tx2_bf_ap_idx] + 1;
        end
        if((tail[tau_rx_tx2_bf_ap_idx] + 2) >= BITSTREAM_BUFFER_LEN) begin
            tail_2 = tail[tau_rx_tx2_bf_ap_idx] + 2 - BITSTREAM_BUFFER_LEN;
        end else begin
            tail_2 = tail[tau_rx_tx2_bf_ap_idx] + 2;
        end
        if((tail[tau_rx_tx2_bf_ap_idx] + 3) >= BITSTREAM_BUFFER_LEN) begin
            tail_3 = tail[tau_rx_tx2_bf_ap_idx] + 3 - BITSTREAM_BUFFER_LEN;
        end else begin
            tail_3 = tail[tau_rx_tx2_bf_ap_idx] + 3;
        end
        if((head[rotate_bf_ap_idx_saved]) >= BITSTREAM_BUFFER_LEN) begin
            head_0_next = head[rotate_bf_ap_idx_saved] - BITSTREAM_BUFFER_LEN;
        end else begin
            head_0_next = head[rotate_bf_ap_idx_saved];
        end
        if((head[rotate_bf_ap_idx_saved] + 1) >= BITSTREAM_BUFFER_LEN) begin
            head_1_next = head[rotate_bf_ap_idx_saved] + 1 - BITSTREAM_BUFFER_LEN;
        end else begin
            head_1_next = head[rotate_bf_ap_idx_saved] + 1;
        end
        if((head[rotate_bf_ap_idx_saved] + 2) >= BITSTREAM_BUFFER_LEN) begin
            head_2_next = head[rotate_bf_ap_idx_saved] + 2 - BITSTREAM_BUFFER_LEN;
        end else begin
            head_2_next = head[rotate_bf_ap_idx_saved] + 2;
        end
        if((head[rotate_bf_ap_idx_saved] + 3) >= BITSTREAM_BUFFER_LEN) begin
            head_3_next = head[rotate_bf_ap_idx_saved] + 3 - BITSTREAM_BUFFER_LEN;
        end else begin
            head_3_next = head[rotate_bf_ap_idx_saved] + 3;
        end
        select_bits_next[0]         = tau_rx_tx2_col[head_0];
        select_bits_next[1]         = tau_rx_tx2_col[head_1];
        select_bits_next[2]         = tau_rx_tx2_col[head_2];
        select_bits_next[3]         = tau_rx_tx2_col[head_3];
        decrementer_next            = decrementer;
        tau_rx_tx2_bf_ap_idx_next   = tau_rx_tx2_bf_ap_idx;
        select_en_next              = 0;
        bf_ap_idx_next              = 0;
        buffer_next                 = 0;
        num_outputs                 = 0;
        bf_ap_idx_saved_next        = bf_ap_idx;
        decrementer_temp            = decrementer[rotate_bf_ap_idx_saved_2];
        if((head[rotate_bf_ap_idx_saved] + 4) >= BITSTREAM_BUFFER_LEN) begin
            head_temp_next = head[rotate_bf_ap_idx_saved] + 4 - BITSTREAM_BUFFER_LEN;
        end else begin
            head_temp_next = head[rotate_bf_ap_idx_saved] + 4;;
        end
        if((tail[tau_rx_tx2_bf_ap_idx] + 4) >= BITSTREAM_BUFFER_LEN) begin
            tail_temp = tail[tau_rx_tx2_bf_ap_idx] + 4 - BITSTREAM_BUFFER_LEN;
        end else begin
            tail_temp = tail[tau_rx_tx2_bf_ap_idx] + 4;;
        end

        rotate_saved_next           = rotate_in;
        rotate_en_saved_next        = rotate_en_in;

        rotate_saved_2_next         = rotate_saved;
        rotate_en_saved_2_next      = rotate_en_saved;
        tau_rx_tx2_saved_next       = tau_rx_tx2_in;
        tau_rx_tx2_en_saved_next    = tau_rx_tx2_en_in;
        rotate_bf_ap_idx_saved_next = rotate_bf_ap_idx_in;

        rotate_saved_3_next           = rotate_saved_2;
        rotate_en_saved_3_next        = rotate_en_saved_2;
        rotate_bf_ap_idx_saved_2_next = rotate_bf_ap_idx_saved;

        tau_rx_tx2_col_next = tau_rx_tx2[rotate_bf_ap_idx_saved];

        // Decrement the decrementer for the current data channel
        if(rotate_en_saved_2 && (decrementer_temp > 0)) begin
            decrementer_next[rotate_bf_ap_idx_saved_2] = decrementer[rotate_bf_ap_idx_saved_2] - 1;
        end

        // Save the inputs and increment head
        if(rotate_en_saved_2 && (decrementer_temp == 0)) begin
            rotate_saved_3_next                 = rotate_saved_2;
            rotate_en_saved_3_next              = rotate_en_saved_2;

            // Increment the head pointer by 4
            head_next[rotate_bf_ap_idx_saved_2] = head_temp;

            // Save bf_ap_idx
            bf_ap_idx_next                      = rotate_bf_ap_idx_saved_2;
        end // if(rotate_en_saved_2)

        // Select the appropriate samples
        if(rotate_en_saved_2) begin
            if(select_bits[0] == 1'b1) begin
                buffer_next[num_outputs]    = rotate_saved_3[0];
                select_en_next[num_outputs] = 1;
                num_outputs++;
            end
            if(select_bits[1] == 1'b1) begin
                buffer_next[num_outputs]    = rotate_saved_3[1];
                select_en_next[num_outputs] = 1;
                num_outputs++;
            end
            if(select_bits[2] == 1'b1) begin
                buffer_next[num_outputs]    = rotate_saved_3[2];
                select_en_next[num_outputs] = 1;
                num_outputs++;
            end
            if(select_bits[3] == 1'b1) begin
                buffer_next[num_outputs]    = rotate_saved_3[3];
                select_en_next[num_outputs] = 1;
                num_outputs++;
            end
        end // if(rotate_en_saved_3)

        // Add to the tau_rx_tx2 buffer
        if(tau_rx_tx2_en_saved) begin
            // Add the first bit
            tau_rx_tx2_next[tau_rx_tx2_bf_ap_idx][tail_0] = tau_rx_tx2_saved[0];

            // Add the second bit
            tau_rx_tx2_next[tau_rx_tx2_bf_ap_idx][tail_1] = tau_rx_tx2_saved[1];

            // Add the third bit
            tau_rx_tx2_next[tau_rx_tx2_bf_ap_idx][tail_2] = tau_rx_tx2_saved[2];

            // Add the fourth bit
            tau_rx_tx2_next[tau_rx_tx2_bf_ap_idx][tail_3] = tau_rx_tx2_saved[3];

            // Increment the tail pointer by 4
            tail_next[tau_rx_tx2_bf_ap_idx]               = tail_temp;

            // Handle wrapping of the tau_rx_tx2 bf_ap_idx pointer
            if(tau_rx_tx2_bf_ap_idx == bf_ap_size) begin
                tau_rx_tx2_bf_ap_idx_next = 0;
            end else begin
                tau_rx_tx2_bf_ap_idx_next = tau_rx_tx2_bf_ap_idx + 1;
            end // if(tau_rx_tx2_bf_ap_idx == bf_ap_size)
        end // if(tau_rx_tx2_en_saved)
    end // always_comb

    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(reset) begin
            // Initialize values
            bf_ap_size               <= `SD bf_ap_size_in;
            buffer                   <= `SD 0;
            tau_rx_tx2               <= `SD 0;
            head                     <= `SD 0;
            for(int i = 0; i < MAX_BF_AP_SIZE; i++) begin
                tail[i]              <= `SD tau_tx1_in[i][1:0]; // initialize tail to tau_tx1 for offset
                decrementer[i]       <= `SD tau_tx1_in[i][$clog2(TAU_TX1_MAX_VAL)-1:2];
            end // for(int i = 0; i < MAX_BF_AP_SIZE; i++)
            select_en                <= `SD 0;
            bf_ap_idx                <= `SD 0;
            tau_rx_tx2_bf_ap_idx     <= `SD 0;
            bf_ap_idx_saved          <= `SD 0;
            rotate_saved             <= `SD 0;
            rotate_en_saved          <= `SD 0;
            select_bits              <= `SD 0;

            rotate_saved_2           <= `SD 0;
            rotate_en_saved_2        <= `SD 0;
            tau_rx_tx2_saved         <= `SD 0;
            tau_rx_tx2_en_saved      <= `SD 0;
            rotate_bf_ap_idx_saved   <= `SD 0;

            rotate_saved_3           <= `SD 0;
            rotate_en_saved_3        <= `SD 0;
            rotate_bf_ap_idx_saved_2 <= `SD 0;
            head_0                   <= `SD 0;
            head_1                   <= `SD 0;
            head_2                   <= `SD 0;
            head_3                   <= `SD 0;
            head_temp                <= `SD 0;

            tau_rx_tx2_col           <= `SD 0;
        end else begin
            buffer                   <= `SD buffer_next;
            tau_rx_tx2               <= `SD tau_rx_tx2_next;
            head                     <= `SD head_next;
            tail                     <= `SD tail_next;
            select_en                <= `SD select_en_next;
            bf_ap_idx                <= `SD bf_ap_idx_next;
            decrementer              <= `SD decrementer_next;
            tau_rx_tx2_bf_ap_idx     <= `SD tau_rx_tx2_bf_ap_idx_next;
            bf_ap_idx_saved          <= `SD bf_ap_idx_saved_next;
            rotate_saved             <= `SD rotate_saved_next;
            rotate_en_saved          <= `SD rotate_en_saved_next;
            select_bits              <= `SD select_bits_next;

            rotate_saved_2           <= `SD rotate_saved_2_next;
            rotate_en_saved_2        <= `SD rotate_en_saved_2_next;
            tau_rx_tx2_saved         <= `SD tau_rx_tx2_saved_next;
            tau_rx_tx2_en_saved      <= `SD tau_rx_tx2_en_saved_next;
            rotate_bf_ap_idx_saved   <= `SD rotate_bf_ap_idx_saved_next;

            rotate_saved_3           <= `SD rotate_saved_3_next;
            rotate_en_saved_3        <= `SD rotate_en_saved_3_next;
            rotate_bf_ap_idx_saved_2 <= `SD rotate_bf_ap_idx_saved_2_next;
            head_0                   <= `SD head_0_next;
            head_1                   <= `SD head_1_next;
            head_2                   <= `SD head_2_next;
            head_3                   <= `SD head_3_next;
            head_temp                <= `SD head_temp_next;

            tau_rx_tx2_col           <= `SD tau_rx_tx2_col_next;
        end // if(reset) begin
    end // always_ff @(posedge clock)
endmodule // select
