/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  mac_stg2.sv                                         //
//                                                                     //
//  Description :  Performs Multiply-and-Accumulate operations; stores //
//                 partially-accumulated values in buffer, which is a  //
//                 virtual 2D register file.                           //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module mac_stg2 #(parameter SAMPLE_BIT_WIDTH = 12, parameter PIPELINE_SAMPLE_BIT_WIDTH = 10, parameter MAX_BF_AP_SIZE = 32, parameter MAC_BUFFER_ENTRIES = 128, parameter NUM_WAYS = 4) (

        // Inputs
        input                                               clock,
        input                                               reset,
        input [NUM_WAYS-1:0][PIPELINE_SAMPLE_BIT_WIDTH-1:0] mac_stg1_in,
        input [NUM_WAYS-1:0]                                mac_stg1_en_in,
        input [$clog2(MAX_BF_AP_SIZE)-1:0]                  mac_stg1_bf_ap_idx_in,
        input [$clog2(MAX_BF_AP_SIZE)-1:0]                  bf_ap_size_in,

        // Outputs
        output logic [SAMPLE_BIT_WIDTH-1:0] mac_stg2_out,
        output logic                        mac_stg2_en_out

    );

    // Local variables
    logic [$clog2(MAX_BF_AP_SIZE):0] bf_ap_size;

    // Saved variables
    logic [NUM_WAYS-1:0][PIPELINE_SAMPLE_BIT_WIDTH-1:0] mac_stg1_saved, mac_stg1_saved_next;
    logic [NUM_WAYS-1:0]                                mac_stg1_en_saved, mac_stg1_en_saved_next; // saved input for second cycle accumulation of apodized values

    // MAC buffer variables
    logic [MAC_BUFFER_ENTRIES-1:0][SAMPLE_BIT_WIDTH-1:0]       mac_buffer, mac_buffer_next; // holds the values staged for output
    logic [MAC_BUFFER_ENTRIES-1:0][$clog2(MAX_BF_AP_SIZE):0]   mac_count, mac_count_next; // holds the current number of accumulated values per mac buffer entry
    logic [$clog2(MAC_BUFFER_ENTRIES)-1:0]                     col_head, col_head_next; // SHARED head (points to mac buffer index at "head")
    logic [MAX_BF_AP_SIZE-1:0][$clog2(MAC_BUFFER_ENTRIES)-1:0] col_tail, col_tail_next; // holds number of values stored per bf_ap_idx (tail of virtual columns)
    logic [NUM_WAYS-1:0][$clog2(MAC_BUFFER_ENTRIES)-1:0]       temp_tail, temp_tail_next; // used to index into mac buffer
    logic [NUM_WAYS-1:0][SAMPLE_BIT_WIDTH-1:0]                 temp_value;
    logic [SAMPLE_BIT_WIDTH-1:0]                               buffer, buffer_next; // holds the values staged for output
    logic                                                      buffer_en, buffer_en_next; // holds the enable bit staged for output

    // Assign outputs
    assign mac_stg2_out    = buffer;
    assign mac_stg2_en_out = buffer_en;

    // Write Address Calculation
    always_comb begin
        // Set default values for this cycle
        mac_buffer_next               = mac_buffer;
        mac_count_next                = mac_count;
        col_head_next                 = col_head;
        col_tail_next                 = col_tail;
        mac_stg1_saved_next           = mac_stg1_in;
        mac_stg1_en_saved_next        = mac_stg1_en_in;
        buffer_next                   = 0;
        buffer_en_next                = 0;
        temp_tail_next                = 0;
        temp_value                    = 0;

        // Perform the accumulate operation
        for(int i = 0; i < NUM_WAYS; i++) begin
            if(mac_stg1_en_in[i]) begin
                // Set the tail which will be used to index into the mac buffer next cycle
                temp_tail_next[i]                    = col_tail[mac_stg1_bf_ap_idx_in] + i;

                // Increment the tail pointer for this virtual column by the number of valid samples being saved
                col_tail_next[mac_stg1_bf_ap_idx_in] = col_tail[mac_stg1_bf_ap_idx_in] + i + 1;
            end // if(mac_stg1_en_in[i])

            temp_value[i] = $signed(mac_buffer[temp_tail[i]]) + $signed(mac_stg1_saved[i]);

            // Store the accumulated values in the buffer
            if(mac_stg1_en_saved[i]) begin
                // Add the multiplication result to the mac buffer entry
                mac_buffer_next[temp_tail[i]] = temp_value[i];

                // Increment mac buffer entry count
                mac_count_next[temp_tail[i]]  = mac_count[temp_tail[i]] + 1;
            end // if(mac_stg1_en_saved[i])
        end // for(int i = 0; i < NUM_WAYS; i++)

        // Read completed voxels
        if(mac_count[col_head] == bf_ap_size) begin
            buffer_next               = mac_buffer[col_head];

            buffer_en_next            = 1;

            mac_buffer_next[col_head] = 0;

            mac_count_next[col_head]  = 0;

            col_head_next             = col_head + 1;
        end // if(mac_count[col_head] == bf_ap_size)
    end // always_comb

    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(reset) begin
            bf_ap_size               <= `SD bf_ap_size_in+1; // +1 because we're counting things, and zero is empty; TODO: make this cleaner
            mac_buffer               <= `SD 0;
            mac_count                <= `SD 0;
            col_head                 <= `SD 0;
            col_tail                 <= `SD 0;
            buffer                   <= `SD 0;
            buffer_en                <= `SD 0;
            temp_tail                <= `SD 0;
            mac_stg1_saved           <= `SD 0;
            mac_stg1_en_saved        <= `SD 0;
        end else begin
            mac_buffer               <= `SD mac_buffer_next;
            mac_count                <= `SD mac_count_next;
            col_head                 <= `SD col_head_next;
            col_tail                 <= `SD col_tail_next;
            buffer                   <= `SD buffer_next;
            buffer_en                <= `SD buffer_en_next;
            temp_tail                <= `SD temp_tail_next;
            mac_stg1_saved           <= `SD mac_stg1_saved_next;
            mac_stg1_en_saved        <= `SD mac_stg1_en_saved_next;
        end // if(reset) begin
    end // always_ff @(posedge clock)
endmodule // mac_stg2
