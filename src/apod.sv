/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  apod.sv                                             //
//                                                                     //
//  Description :  Apodizes selected samples.                          //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module apod #(parameter SAMPLE_BIT_WIDTH = 12, parameter PIPELINE_SAMPLE_BIT_WIDTH = 10, parameter MAX_BF_AP_SIZE = 32) (

        // Inputs
        input                                            clock,
        input                                            reset,
        input [PIPELINE_SAMPLE_BIT_WIDTH-1:0]            regfile_in,
        input                                            regfile_en_in,
        input                                            regfile_bf_ap_done_in,
        input [MAX_BF_AP_SIZE-1:0][SAMPLE_BIT_WIDTH-1:0] apod_consts_in,

        // Outputs
        output logic [PIPELINE_SAMPLE_BIT_WIDTH-1:0] apod_out,
        output logic                                 apod_en_out,
        output logic                                 apod_bf_ap_done_out

    );

    // Local variables
    logic [PIPELINE_SAMPLE_BIT_WIDTH+11-1:0]         buffer, buffer_next; // holds the apodized value; output logic is PIPELINE_SAMPLE_BIT_WIDTH+11 bits, then lose 11 LSB
    logic                                            buffer_en, buffer_en_next; // holds the apodized value's enable bit
    logic [$clog2(MAX_BF_AP_SIZE)-1:0]               bf_ap_idx, bf_ap_idx_next; // holds the output value after accumulation
    logic                                            bf_ap_done, bf_ap_done_next, bf_ap_done_saved, bf_ap_done_saved_next; // signals that the output value is ready
    logic [MAX_BF_AP_SIZE-1:0][SAMPLE_BIT_WIDTH-1:0] apod_consts; // holds the bf aperture size
    logic [SAMPLE_BIT_WIDTH-1:0]                     current_apod_const, current_apod_const_next;
    logic [PIPELINE_SAMPLE_BIT_WIDTH-1:0]            regfile_saved, regfile_saved_next;
    logic                                            regfile_en_saved, regfile_en_saved_next;

    // Assign outputs
    assign apod_out            = buffer[PIPELINE_SAMPLE_BIT_WIDTH+11-1:11];
    assign apod_en_out         = buffer_en;
    assign apod_bf_ap_done_out = bf_ap_done_saved;

    always_comb begin
        bf_ap_idx_next          = bf_ap_idx;
        bf_ap_done_next         = regfile_bf_ap_done_in;
        bf_ap_done_saved_next   = bf_ap_done;
        buffer_next             = 0;
        buffer_en_next          = 0;
        regfile_en_saved_next   = 0;
        regfile_saved_next      = 0;
        current_apod_const_next = 0;

        // Save the inputs and fetch apod constant
        if(regfile_en_in) begin
            regfile_saved_next      = regfile_in;
            regfile_en_saved_next   = regfile_en_in;
            current_apod_const_next = apod_consts[bf_ap_idx];
            
            // Increment bf_ap_idx
            bf_ap_idx_next          = bf_ap_idx + 1;
        end // if(regfile_en_in)

        // Perform apodization multiplication
        buffer_next = $signed(current_apod_const) * $signed(regfile_saved);
        if(regfile_en_saved) begin
            buffer_en_next = 1;
        end // if(regfile_en_saved)

        if(regfile_bf_ap_done_in) begin
            bf_ap_idx_next = 0;
        end // if(regfile_bf_ap_done_in)
    end // always_comb

    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(reset) begin
            bf_ap_idx          <= `SD 0;
            bf_ap_done         <= `SD 0;
            bf_ap_done_saved   <= `SD 0;
            buffer             <= `SD 0;
            buffer_en          <= `SD 0;
            regfile_saved      <= `SD 0;
            regfile_en_saved   <= `SD 0;
            current_apod_const <= `SD 0;
            apod_consts        <= `SD apod_consts_in; // initialize the apodization constant array
        end else begin
            bf_ap_idx          <= `SD bf_ap_idx_next;
            bf_ap_done         <= `SD bf_ap_done_next;
            bf_ap_done_saved   <= `SD bf_ap_done_saved_next;
            buffer             <= `SD buffer_next;
            buffer_en          <= `SD buffer_en_next;
            regfile_saved      <= `SD regfile_saved_next;
            regfile_en_saved   <= `SD regfile_en_saved_next;
            current_apod_const <= `SD current_apod_const_next;
        end // if(reset) begin
    end // always_ff @(posedge clock)
endmodule // apod
