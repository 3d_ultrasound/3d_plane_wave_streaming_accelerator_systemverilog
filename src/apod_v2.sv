/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  apod_v2.sv                                          //
//                                                                     //
//  Description :  Apodizes selected samples.                          //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module apod_v2 #(parameter SAMPLE_BIT_WIDTH = 12, parameter PIPELINE_SAMPLE_BIT_WIDTH = 10, parameter MAX_BF_AP_SIZE = 32) (

        // Inputs
        input                                 clock,
        input                                 reset,
        input [PIPELINE_SAMPLE_BIT_WIDTH-1:0] regfile_in,
        input                                 regfile_en_in,
        input [$clog2(MAX_BF_AP_SIZE)-1:0]    select_bf_ap_idx_in,
        input [SAMPLE_BIT_WIDTH-1:0]          current_apod_const_in,

        // Outputs
        output logic [PIPELINE_SAMPLE_BIT_WIDTH-1:0] apod_v2_out,
        output logic                                 apod_v2_en_out

    );

    // Local variables
    logic [PIPELINE_SAMPLE_BIT_WIDTH+11-1:0] buffer, buffer_next; // holds the apodized value; output logic is PIPELINE_SAMPLE_BIT_WIDTH+11 bits, then lose 11 LSB
    logic                                    buffer_en, buffer_en_next; // holds the apodized value's enable bit
    logic [SAMPLE_BIT_WIDTH-1:0]             current_apod_const, current_apod_const_next;
    logic [PIPELINE_SAMPLE_BIT_WIDTH-1:0]    regfile_saved, regfile_saved_next;
    logic                                    regfile_en_saved, regfile_en_saved_next;

    // Assign outputs
    assign apod_v2_out    = buffer[PIPELINE_SAMPLE_BIT_WIDTH+11-1:11];
    assign apod_v2_en_out = buffer_en;

    always_comb begin
        buffer_next             = 0;
        buffer_en_next          = 0;
        regfile_en_saved_next   = 0;
        regfile_saved_next      = 0;
        current_apod_const_next = 0;

        // Save the inputs and fetch apod constant
        if(regfile_en_in) begin
            regfile_saved_next      = regfile_in;
            regfile_en_saved_next   = regfile_en_in;
            current_apod_const_next = current_apod_const_in;
        end // if(regfile_en_in)

        // Perform apodization multiplication
        buffer_next = $signed(current_apod_const) * $signed(regfile_saved);
        if(regfile_en_saved) begin
            buffer_en_next = 1;
        end // if(regfile_en_saved)
    end // always_comb

    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(reset) begin
            buffer             <= `SD 0;
            buffer_en          <= `SD 0;
            regfile_saved      <= `SD 0;
            regfile_en_saved   <= `SD 0;
            current_apod_const <= `SD 0;
        end else begin
            regfile_saved      <= `SD regfile_saved_next;
            regfile_en_saved   <= `SD regfile_en_saved_next;
            buffer             <= `SD buffer_next;
            buffer_en          <= `SD buffer_en_next;
            current_apod_const <= `SD current_apod_const_next;
        end // if(reset) begin
    end // always_ff @(posedge clock)
endmodule // apod_v2
