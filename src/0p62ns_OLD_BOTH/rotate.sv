/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  rotate.sv                                           //
//                                                                     //
//  Description :  Rotates interpolated samples within a slice of      //
//                 pipelines.                                          //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module rotate #(parameter SAMPLE_BIT_WIDTH = 12, parameter MAX_BF_AP_SIZE = 32) (

        // Inputs
        input                              clock,
        input                              reset,
        input [3:0][SAMPLE_BIT_WIDTH-1:0]  interp_in,
        input                              interp_en_in,
        input [3:0][SAMPLE_BIT_WIDTH-1:0]  rotate_side_in,
        input                              rotate_en_side_in,
        input [$clog2(MAX_BF_AP_SIZE)-1:0] bf_ap_size_in,

        // Outputs
        output logic [3:0][SAMPLE_BIT_WIDTH-1:0]  rotate_side_out,
        output logic                              rotate_en_side_out,
        output logic [3:0][SAMPLE_BIT_WIDTH-1:0]  rotate_out,
        output logic                              rotate_en_out,
        output logic [$clog2(MAX_BF_AP_SIZE)-1:0] rotate_bf_ap_idx_out

    );

    // Local variables
    logic [$clog2(MAX_BF_AP_SIZE)-1:0] bf_ap_size; // holds the bf aperture size
    logic [3:0][SAMPLE_BIT_WIDTH-1:0]  buffer, buffer_next;
    logic                              buffer_en, buffer_en_next;
    logic [$clog2(MAX_BF_AP_SIZE)-1:0] bf_ap_idx, bf_ap_idx_next;

    // Assign outputs
    assign rotate_out[0]        = buffer[0];
    assign rotate_out[1]        = buffer[1];
    assign rotate_out[2]        = buffer[2];
    assign rotate_out[3]        = buffer[3];
    assign rotate_en_out        = (bf_ap_idx < bf_ap_size) ? buffer_en : 0;
    assign rotate_bf_ap_idx_out = bf_ap_idx;

    assign rotate_side_out[0] = buffer[0];
    assign rotate_side_out[1] = buffer[1];
    assign rotate_side_out[2] = buffer[2];
    assign rotate_side_out[3] = buffer[3];
    assign rotate_en_side_out = buffer_en;

    always_comb begin
        buffer_next    = buffer;
        buffer_en_next = interp_en_in | rotate_en_side_in;
        bf_ap_idx_next = bf_ap_idx;

        if(interp_en_in) begin
            // Load interpolated samples into the buffer
            buffer_next[0] = interp_in[0];
            buffer_next[1] = interp_in[1];
            buffer_next[2] = interp_in[2];
            buffer_next[3] = interp_in[3];

            // Reset bf_ap_idx
            bf_ap_idx_next = 0;
        end else if(rotate_en_side_in && (bf_ap_idx < bf_ap_size)) begin
            // Load interpolated samples into the buffer
            buffer_next[0] = rotate_side_in[0];
            buffer_next[1] = rotate_side_in[1];
            buffer_next[2] = rotate_side_in[2];
            buffer_next[3] = rotate_side_in[3];

            // Increment bf_ap_idx
            bf_ap_idx_next = bf_ap_idx + 1;
        end
    end

    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(reset) begin
            buffer     <= `SD 0;
            buffer_en  <= `SD 0;
            bf_ap_idx  <= `SD 0;
            bf_ap_size <= `SD bf_ap_size_in; // initialize the bf aperture size
        end else begin
            buffer    <= `SD buffer_next;
            buffer_en <= `SD buffer_en_next;
            bf_ap_idx <= `SD bf_ap_idx_next;
        end // if(reset)
    end // always_ff @(posedge clock)
endmodule // rotate
