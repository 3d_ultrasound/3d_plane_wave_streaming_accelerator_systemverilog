/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  regfile.sv                                          //
//                                                                     //
//  Description :  4-bank register file.                               //
//                                                                     //
//  FIX :  If SRAM array access is pipelined, *maybe* can meet 0.78ns? //
//  FIX :  Change rd_idx calc. from shift to bit select (faster)?      //
//  FIX :  Pipeline wr_idx calc (calculate idx previous cycle)?        //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module regfile #(parameter SAMPLE_BIT_WIDTH = 12, parameter MAX_BF_AP_SIZE = 32, parameter REG_FILE_X = 32, parameter REG_FILE_Y = 32, parameter NUM_BANKS = 4, parameter NUM_BANK_ENTRIES = ((REG_FILE_X*REG_FILE_Y)/NUM_BANKS)) (

        // Inputs
        input                                       clock,
        input                                       reset,
        input [NUM_BANKS-1:0][SAMPLE_BIT_WIDTH-1:0] select_in,
        input [NUM_BANKS-1:0]                       select_en_in,
        input [$clog2(MAX_BF_AP_SIZE)-1:0]          select_bf_ap_idx_in,
        input [$clog2(MAX_BF_AP_SIZE)-1:0]          bf_ap_size_in,

        // Outputs
        output logic [SAMPLE_BIT_WIDTH-1:0] regfile_out,
        output logic                        regfile_en_out,
        output logic                        regfile_bf_ap_done_out

    );

    // Local variables
    logic [$clog2(MAX_BF_AP_SIZE)-1:0]                   bf_ap_size; // holds the bf aperture size
    logic [$clog2(NUM_BANK_ENTRIES)-1:0]                 col_head, col_head_next; // SHARED head (for all columns)
    logic [REG_FILE_X-1:0][$clog2(NUM_BANK_ENTRIES)-1:0] col_tail, col_tail_next; // per-column tail

    // Variables for writing data
    logic [NUM_BANKS-1:0][$clog2(NUM_BANKS)-1:0]        wr_bank_idx_temp; // bank index for each write value; used as an index into wr_idx
    logic [NUM_BANKS-1:0][$clog2(NUM_BANK_ENTRIES)-1:0] wr_idx; // index within each bank for the write values
    logic [NUM_BANKS-1:0][SAMPLE_BIT_WIDTH-1:0]         wr_data; // index within the bank for each write value
    logic [NUM_BANKS-1:0]                               wr_en; // write enable bits for each bank

    // Variables for reading data
    logic [$clog2(NUM_BANK_ENTRIES)-1:0]                rd_col_pointer, rd_col_pointer_next;
    logic [$clog2(NUM_BANKS)-1:0]                       rd_bank_idx_test; // bank index for the single read value
    logic [NUM_BANKS-1:0][$clog2(NUM_BANK_ENTRIES)-1:0] rd_idx; // index within the bank for the single read value
    logic [NUM_BANKS-1:0][SAMPLE_BIT_WIDTH-1:0]         rd_data; // index within the bank for each write value
    logic [SAMPLE_BIT_WIDTH-1:0]                        rd_data_saved; // index within the bank for each write value
    logic                                               rd_en, rd_en_next;
    logic                                               bf_ap_done, bf_ap_done_next;

    `ifdef USE_REGFILE_SRAM_45
    // IBM 45nm SOI
    sram_2p_hs_v60_uvtsvt bank_array [NUM_BANKS-1:0] (

        // Inputs
        .CLKA(clock), // clock for port A - ADD "~" FOR SIMULATION; REMOVE FOR SYNTHESIS
        .CENA(~rd_en_next), // chip enable (clock gating) (if read and write enable, or them and invert)...active LOW, tie to 0
        .AA(rd_idx), // address A
        .CLKB(clock), // clock for port B
        .CENB(~wr_en), // chip enable (clock gating) (if read and write enable, or them and invert)...active LOW, tie to 0
        .AB(wr_idx), // address B
        .DB(wr_data), // data B
        .EMAA(3'b000), // extra margin adjustment A (tunable delay signal); some constant (user guide?); Austin uses 2 in TSMC16
        .EMASA(1'b0), // extra margin ajustment - tie to 0
        .EMAB(3'b000), // extra margin adjustment B (tunable delay signal); some constant (user guide?); Austin uses 2 in TSMC16
        .EMAWB(2'b01), // tie to 1
        .NWA(1'b0), // try setting to 1?
        .TENA(1'b1), // test mode enable - tie to 1
        .BENA(1'b1), // try setting to 1?
        .TCENA(1'b1), // chip enable test input, active LOW - tie to 1
        .TAA(8'b0), // address test input - tie to 0
        .TQA(12'b0), // test mode data input - tie to 0
        .TENB(1'b1), // test mode enable - tie to 1
        .TCENB(1'b1), // chip enable test input, active LOW - tie to 1
        .TAB(8'b0), // address test input - tie to 0
        .TDB(12'b0), // test mode data input - tie to 0
        .STOVA(1'b0), // self timed override - tie to 0
        .STOVB(1'b0), // tie to 0

        // Outputs
        .CENYA(), // chip enable multiplexer output
        .AYA(), // address multiplexer output
        .CENYB(), // chip enable multiplexer output
        .AYB(), // no idea
        .DYB(), // no idea
        .QA(rd_data) // output data for A

    );
    `elsif USE_REGFILE_SRAM_16
    // TSMC 16nm
    sram_2p_uhde bank_array [NUM_BANKS-1:0] (

        // Inputs
        .CLK(clock), // clock - ADD "~" FOR SIMULATION; REMOVE FOR SYNTHESIS
        .CENA(~rd_en_next), // read port chip enable
        .CENB(~wr_en), // write port chip enable
        .AA(rd_idx), // read port address
        .AB(wr_idx), // write port address
        .DB(wr_data), // write port data
        .STOV(1'b0), // When the STOV and STOVAB pins are HIGH, the internal clock pulse for port A is generated directly from the high phase of the external clock input
        .STOVAB(1'b0), // When the STOV pin is HIGH and the STOVAB pin is LOW, the internal clock pulse for port B is generated directly from the high phase of the external clock input.
        .EMA(3'b010), // extra margin adjustment - Austin uses 010; try 010 (default)
        .EMAW(2'b01), // similar to EMA; try 01 (default)
        .EMAS(1'b0), // default is LOW
        .EMAP(1'b0), // default is LOW
        .RET1N(1'b1), // retention mode; try 1?

        // Outputs
        .QA(rd_data) // output data for A

    );
    `else
    // Instantiate a bank array of size NUM_BANKS
    bank #(.SAMPLE_BIT_WIDTH(SAMPLE_BIT_WIDTH), .NUM_BANK_ENTRIES(NUM_BANK_ENTRIES)) bank_array [NUM_BANKS-1:0] (

        // Inputs
        .clock(clock),
        .rd_idx(rd_idx), // read index
        .wr_idx(wr_idx), // write index
        .wr_data(wr_data), // write data
        .wr_en(wr_en), // write enable

        // Outputs
        .rd_out(rd_data) // read data

    );
    `endif

    // Assign outputs
    assign regfile_out            = rd_en ? rd_data_saved : 0;
    assign regfile_en_out         = rd_en;
    assign regfile_bf_ap_done_out = bf_ap_done;

    // Read Address Calculation
    always_comb begin
        // Set default values for this cycle
        col_head_next       = col_head;
        rd_col_pointer_next = rd_col_pointer;
        rd_idx              = 0;
        rd_en_next          = 0;
        bf_ap_done_next     = 0;
        rd_bank_idx_test    = 0;
        
        if(col_head != col_tail[rd_col_pointer]) begin
            // Increment column pointer
            rd_col_pointer_next      = rd_col_pointer + 1;

            // Calculate the read index
            rd_bank_idx_test         = col_head[$clog2(NUM_BANKS)-1:0]; // bank index; this is just an index into rd_idx
            rd_idx[rd_bank_idx_test] = ((col_head >> $clog2(NUM_BANKS)) << $clog2(REG_FILE_Y)) + rd_col_pointer;
            rd_en_next               = 1;
        end

        if(rd_col_pointer_next == bf_ap_size) begin
            col_head_next       = col_head + 1;
            rd_col_pointer_next = 0;
            bf_ap_done_next     = 1;
        end // if(rd_col_pointer_next == bf_ap_size)
    end // always_comb

    // Write Address Calculation
    always_comb begin
        // Set default values for this cycle
        col_tail_next    = col_tail;
        wr_bank_idx_temp = 0;
        wr_idx           = 0;
        wr_data          = 0;
        wr_en            = 0;

        // NOTE: pointer wrapping occurs naturally since everything is a power of 2
        for(int i = 0; i < NUM_BANKS; i++) begin
            if(select_en_in[i]) begin
                // Increment tail pointer
                col_tail_next[select_bf_ap_idx_in] = col_tail[select_bf_ap_idx_in] + i + 1;

                // Calculate the write index
                wr_bank_idx_temp[i]                = col_tail[select_bf_ap_idx_in][$clog2(NUM_BANKS)-1:0] + i; // bank index; this is just an index into wr_idx
                // wr_en[col_tail[select_bf_ap_idx_in][$clog2(NUM_BANKS)-1:0] + i]   = 1;
                // wr_idx[col_tail[select_bf_ap_idx_in][$clog2(NUM_BANKS)-1:0] + i]  = (((col_tail[select_bf_ap_idx_in] + i) >> $clog2(NUM_BANKS)) << $clog2(REG_FILE_Y)) + select_bf_ap_idx_in;
                // wr_data[col_tail[select_bf_ap_idx_in][$clog2(NUM_BANKS)-1:0] + i] = select_in[i];
                wr_en[wr_bank_idx_temp[i]]         = 1;
                wr_idx[wr_bank_idx_temp[i]]        = (((col_tail[select_bf_ap_idx_in] + i) >> $clog2(NUM_BANKS)) << $clog2(REG_FILE_Y)) + select_bf_ap_idx_in;
                wr_data[wr_bank_idx_temp[i]]       = select_in[i];
            end // if(select_en_in[i])
        end // for(int i = 0; i < NUM_BANKS; i++)
    end // always_comb

    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(reset) begin
            // Initialize values
            col_head       <= `SD 0;
            col_tail       <= `SD 0;
            rd_col_pointer <= `SD 0;
            bf_ap_size     <= `SD bf_ap_size_in; // initialize the bf aperture size

            rd_en          <= `SD 0;
            bf_ap_done     <= `SD 0;
            rd_data_saved  <= `SD 0;
        end else begin
            // Update values
            col_tail       <= `SD col_tail_next;
            col_head       <= `SD col_head_next;
            rd_col_pointer <= `SD rd_col_pointer_next;

            rd_en          <= `SD rd_en_next;
            bf_ap_done     <= `SD bf_ap_done_next;
            rd_data_saved  <= `SD rd_data[rd_bank_idx_test];
        end // if(reset)
    end // always_ff @(posedge clock)
endmodule // regfile
