/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  select.sv                                           //
//                                                                     //
//  Description :  Reads delay bitstream and selects samples from the  //
//                 rotate buffer.                                      //
//                                                                     //
//  NOTE #1: tau_tx1 cannot exceed (BITSTREAM_BUFFER_LEN-4), and       //
//           BITSTREAM_BUFFER_LEN must be evenly divisible by 4.       //
//                                                                     //
//  NOTE #2: tau_rx_tx2 head must be >= 4 before rotate_en_in first    //
//           becomes valid.                                            //
//                                                                     //
//  FIX :  Try replacing bit array with SRAM (lower power)?            //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module select #(parameter SAMPLE_BIT_WIDTH = 12, parameter MAX_BF_AP_SIZE = 32, parameter BITSTREAM_BUFFER_LEN = 1024, parameter TAU_TX1_MAX_VAL = BITSTREAM_BUFFER_LEN-4) (

        // Inputs
        input                               clock,
        input                               reset,
        input [3:0][SAMPLE_BIT_WIDTH-1:0]   rotate_in,
        input                               rotate_en_in,
        input [$clog2(TAU_TX1_MAX_VAL)-1:0] tau_tx1_in,
        input [3:0]                         tau_rx_tx2_in,
        input                               tau_rx_tx2_en_in,
        input [$clog2(MAX_BF_AP_SIZE)-1:0]  rotate_bf_ap_idx_in,

        // Outputs
        output logic [3:0][SAMPLE_BIT_WIDTH-1:0]  select_out,
        output logic [3:0]                        select_en_out,
        output logic [$clog2(MAX_BF_AP_SIZE)-1:0] select_bf_ap_idx_out

    );

    // Local variables
    logic [3:0][SAMPLE_BIT_WIDTH-1:0]          buffer, buffer_next;
    logic                                      buffer_en, buffer_en_next;
    logic [$clog2(BITSTREAM_BUFFER_LEN/4)-1:0] head, head_next;
    logic [$clog2(BITSTREAM_BUFFER_LEN/4)-1:0] tail, tail_next;
    logic [3:0]                                select_bits;
    logic [3:0]                                select_en, select_en_next;
    logic [$clog2(4)-1:0]                      num_outputs; // used to pack the outputs
    logic [$clog2(MAX_BF_AP_SIZE)-1:0]         bf_ap_idx, bf_ap_idx_next;
    logic [3:0]                                bitmask;
    logic [3:0]                                small_buf;
    logic [3:0]                                small_read;
    logic [$clog2(4)-1:0]                      tx1_buf_pointer;

    `ifdef USE_SELECT_SRAM_45
    // IBM 45nm SOI
    sram_2p_hs_v60_uvtsvt_sel tau_rx_tx2 (

        // Inputs
        .CLKA(clock), // clock for port A - ADD "~" FOR SIMULATION; REMOVE FOR SYNTHESIS
        .CENA(1'b0), // chip enable (clock gating) (if read and write enable, or them and invert)...active LOW, tie to 0
        .AA(head), // address A
        .CLKB(clock), // clock for port B
        .CENB(~tau_rx_tx2_en_in), // chip enable (clock gating) (if read and write enable, or them and invert)...active LOW, tie to 0
        .AB(tail), // address B
        .DB(tau_rx_tx2_in), // data B
        .EMAA(3'b000), // extra margin adjustment A (tunable delay signal); some constant (user guide?); Austin uses 2 in TSMC16
        .EMASA(1'b0), // extra margin ajustment - tie to 0
        .EMAB(3'b000), // extra margin adjustment B (tunable delay signal); some constant (user guide?); Austin uses 2 in TSMC16
        .EMAWB(2'b01), // tie to 1
        .NWA(1'b0), // try setting to 1?
        .TENA(1'b1), // test mode enable - tie to 1
        .BENA(1'b1), // try setting to 1?
        .TCENA(1'b1), // chip enable test input, active LOW - tie to 1
        .TAA(8'b0), // address test input - tie to 0
        .TQA(4'b0), // test mode data input - tie to 0
        .TENB(1'b1), // test mode enable - tie to 1
        .TCENB(1'b1), // chip enable test input, active LOW - tie to 1
        .TAB(8'b0), // address test input - tie to 0
        .TDB(4'b0), // test mode data input - tie to 0
        .STOVA(1'b0), // self timed override - tie to 0
        .STOVB(1'b0), // tie to 0

        // Outputs
        .CENYA(), // chip enable multiplexer output
        .AYA(), // address multiplexer output
        .CENYB(), // chip enable multiplexer output
        .AYB(), // no idea
        .DYB(), // no idea
        .QA(small_read) // output data for A

    );
    `elsif USE_SELECT_SRAM_16
    // TSMC 16nm
    sram_2p_uhde bank_array [NUM_BANKS-1:0] (

        // Inputs
        .CLK(clock), // clock - ADD "~" FOR SIMULATION; REMOVE FOR SYNTHESIS
        .CENA(~1'b0), // read port chip enable
        .CENB(~tau_rx_tx2_en_in), // write port chip enable
        .AA(head), // read port address
        .AB(tail), // write port address
        .DB(tau_rx_tx2_in), // write port data
        .STOV(1'b0), // When the STOV and STOVAB pins are HIGH, the internal clock pulse for port A is generated directly from the high phase of the external clock input
        .STOVAB(1'b0), // When the STOV pin is HIGH and the STOVAB pin is LOW, the internal clock pulse for port B is generated directly from the high phase of the external clock input.
        .EMA(3'b010), // extra margin adjustment - Austin uses 010; try 010 (default)
        .EMAW(2'b01), // similar to EMA; try 01 (default)
        .EMAS(1'b0), // default is LOW
        .EMAP(1'b0), // default is LOW
        .RET1N(1'b1), // retention mode; try 1?

        // Outputs
        .QA(small_read) // output data for A

    );
    `else
    logic [(BITSTREAM_BUFFER_LEN/4)-1:0][3:0] tau_rx_tx2, tau_rx_tx2_next;
    `endif

    // Assign outputs
    assign select_out           = buffer;
    assign select_en_out        = (buffer_en & ~reset) ? select_en : 0;
    assign select_bf_ap_idx_out = bf_ap_idx;

    always_comb begin
        `ifndef USE_SELECT_SRAM_45
        `ifndef USE_SELECT_SRAM_16
        tau_rx_tx2_next = tau_rx_tx2;
        small_read      = tau_rx_tx2[head];
        `endif
        `endif
        head_next       = head;
        tail_next       = tail;
        select_en_next  = 0;
        bf_ap_idx_next  = 0; // ADDED
        buffer_next     = 0;
        buffer_en_next  = 0;
        num_outputs     = 0;

        if(tx1_buf_pointer == 0) begin
            select_bits = small_read;
        end else begin
            select_bits = (small_buf & bitmask) | (small_read & ~bitmask);
        end // if(tx1_buf_pointer == 0)

        // Select the appropriate samples
        if(rotate_en_in) begin
            if(select_bits[0] == 1'b1) begin
                buffer_next[num_outputs]    = rotate_in[0];
                select_en_next[num_outputs] = 1;
                num_outputs++;
            end
            if(select_bits[1] == 1'b1) begin
                buffer_next[num_outputs]    = rotate_in[1];
                select_en_next[num_outputs] = 1;
                num_outputs++;
            end
            if(select_bits[2] == 1'b1) begin
                buffer_next[num_outputs]    = rotate_in[2];
                select_en_next[num_outputs] = 1;
                num_outputs++;
            end
            if(select_bits[3] == 1'b1) begin
                buffer_next[num_outputs]    = rotate_in[3];
                select_en_next[num_outputs] = 1;
            end

            // Increment the head pointer
            head_next      = head + 1;

            // Save bf_ap_idx
            bf_ap_idx_next = rotate_bf_ap_idx_in;

            // Set the enable bit
            buffer_en_next = rotate_en_in;
        end // if(rotate_en_in)

        // Add to the tau_rx_tx2 buffer
        if(tau_rx_tx2_en_in) begin
            // Add the first bit
            `ifndef USE_SELECT_SRAM_45
            `ifndef USE_SELECT_SRAM_16
            tau_rx_tx2_next[tail] = tau_rx_tx2_in;
            `endif
            `endif

            // Increment the tail pointer
            tail_next             = tail + 1;
        end // if(tau_rx_tx2_en_in)
    end // always_comb

    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(reset) begin
            buffer          <= `SD 0;
            buffer_en       <= `SD 0;
            `ifndef USE_SELECT_SRAM_45
            `ifndef USE_SELECT_SRAM_16
            tau_rx_tx2      <= `SD 0;
            `endif
            `endif
            head            <= `SD 0;
            tail            <= `SD tau_tx1_in[$clog2(TAU_TX1_MAX_VAL)-1:2]; // initialize tail to tau_tx1 for offset
            select_en       <= `SD 0;
            bf_ap_idx       <= `SD 0;

            // Take care of the "cache" stuff
            tx1_buf_pointer <= `SD tau_tx1_in[1:0];
            small_buf       <= `SD 0;

            if(tau_tx1_in[1:0] == 4'b00) begin
                bitmask <= `SD 4'b0000;
            end else if(tau_tx1_in[1:0] == 4'b01) begin
                bitmask <= `SD 4'b0001;
            end else if(tau_tx1_in[1:0] == 4'b10) begin
                bitmask <= `SD 4'b0011;
            end else if(tau_tx1_in[1:0] == 4'b11) begin
                bitmask <= `SD 4'b0111;
            end
        end else begin
            buffer          <= `SD buffer_next;
            buffer_en       <= `SD buffer_en_next;
            `ifndef USE_SELECT_SRAM_45
            `ifndef USE_SELECT_SRAM_16
            tau_rx_tx2      <= `SD tau_rx_tx2_next;
            `endif
            `endif
            head            <= `SD head_next;
            tail            <= `SD tail_next;
            select_en       <= `SD select_en_next;
            bf_ap_idx       <= `SD bf_ap_idx_next;
            small_buf       <= `SD small_read & ~bitmask; // store remainder of the last read in the "cache"
        end // if(reset) begin
    end // always_ff @(posedge clock)
endmodule // select
