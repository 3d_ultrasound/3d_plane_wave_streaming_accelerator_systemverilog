/////////////////////////////////////////////////////////////////////////
//                                                                     //
//   Modulename :  sys_defs.vh                                         //
//                                                                     //
//  Description :  This file has the macro-defines for macros used in  //
//                 the pipeline design.                                //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

`ifndef __SYS_DEFS_VH__
`define __SYS_DEFS_VH__

//////////////////////////////////////////////
//                                          //
//           REQUIRED DEFINITIONS           //
//                                          //
//////////////////////////////////////////////
typedef enum logic [2:0] {
  DATA_SAMPLE                = 3'h0, // Implemented
  LOAD_APOD_CONSTS           = 3'h1, // Implemented
  LOAD_BF_AP_SIZE            = 3'h2, // Implemented
  LOAD_TAU_TX1               = 3'h3, // 
  LOAD_TAU_RX_TX2            = 3'h4, // Implemented
  LOAD_TAU_RX_TX2_INIT_DELAY = 3'h5 // Implemented
} INPUT_MODE_SELECT;

typedef enum logic [6:0] {
    NULL_PACKET              = 7'h0, // 
    RESET_PIPELINE_WRAPPER   = 7'h1, // 
    LD_BF_AP_SIZE            = 7'h2, // 
    LD_APOD_CONSTS           = 7'h3, // 
    LD_TAU_TX1               = 7'h4, // 
    LD_INPUT_DATA            = 7'h5, // 
    LD_TAU_RX_TX2            = 7'h6, // 
    LD_TAU_RX_TX2_INIT_DELAY = 7'h7, // 
    LD_NUM_SHIFTS            = 7'h8, // 
    INIT_PIPELINES           = 7'h9, // 
    RUN_STAGE_1              = 7'h10, // 
    RUN_STAGE_2              = 7'h11, // 
    READ_OUTPUT              = 7'h12 // 
} bsg_fsb_opcode_s;

typedef struct packed {
    // these bits are reserved and needed for this network
    logic [3:0]      destid; // 4 bits
    logic [0:0]      cmd;    // 1 bits (1 for switch, 0 for node)

    // for cmd=0, these 75 bits are free for general use; they can be repurposed
    bsg_fsb_opcode_s opcode; // 7 bits - only looked at by switch
    logic [3:0]      srcid;  // 4 bits
    logic [63:0]     data;   // 64 bits
} bsg_fsb_pkt_s;



//////////////////////////////////////////////
//                                          //
//              DESIGN CHOICES              //
//                                          //
//////////////////////////////////////////////
///////////Technology Node Agnostic///////////
// Output options
`define TAPEOUT
typedef enum logic [3:0] {
  ACCUM     = 4'h0,
  APOD      = 4'h1,
  REGFILE   = 4'h2,
  SELECT    = 4'h3,
  ROTATE_3  = 4'h4,
  ROTATE_2  = 4'h5,
  ROTATE_1  = 4'h6,
  ROTATE_0  = 4'h7,
  INTERP_3  = 4'h8,
  INTERP_2  = 4'h9,
  INTERP_1  = 4'hA,
  INTERP_0  = 4'hB,
  SAMPLE    = 4'hC,
  INPUT     = 4'hD
} PIPELINE_OUTPUT_SELECT;


/////////////////////45nm/////////////////////
// Enable 45nm SRAM instances
`define USE_SRAM_45

// Configure tau_rx_tx2 SRAM for ON-CHIP STORAGE
`define TAU_SRAM_READ_WIDTH 64
`define TAU_SRAM_NUM_ENTRIES 8192
`define TAU_RX_TX2_FILE_READ_WIDTH 4

// Enable SRAM Register File
// `define USE_REGFILE_SRAM


/////////////////////16nm/////////////////////
// Enable 16nm SRAM instances
// `define USE_SRAM_16

// Configure tau_rx_tx2 SRAM for ON-CHIP STORAGE
// `define TAU_SRAM_READ_WIDTH 64
// `define TAU_SRAM_NUM_ENTRIES 8192
// `define TAU_RX_TX2_FILE_READ_WIDTH 4

// Enable SRAM Register File
// `define USE_REGFILE_SRAM



//////////////////////////////////////////////
//                                          //
//           Testbench Attributes           //
//                                          //
//////////////////////////////////////////////
// Clock period for test bench
`define VERILOG_CLOCK_PERIOD 10.0

// Set the signal assignment delay; used for SRAMs that don't have a "fast" mode (IBM 45nm SOI)
`define SD #1

// Set up the SRAM for simulation
`define INITIALIZE_MEMORY 1
`define ARM_UD_MODEL

`endif
