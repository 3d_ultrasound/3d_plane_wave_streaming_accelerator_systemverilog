/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  mac.sv                                              //
//                                                                     //
//  Description :  Performs Multiply-and-Accumulate operations; stores //
//                 partially-accumulated values in buffer, which is a  //
//                 virtual 2D register file.                           //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module mac #(parameter SAMPLE_BIT_WIDTH = 12, parameter PIPELINE_SAMPLE_BIT_WIDTH = 10, parameter MAX_BF_AP_SIZE = 32, parameter MAC_BUFFER_ENTRIES = 64) (

        // Inputs
        input                                            clock,
        input                                            reset,
        input [3:0][PIPELINE_SAMPLE_BIT_WIDTH-1:0]       select_in,
        input [3:0]                                      select_en_in,
        input [$clog2(MAX_BF_AP_SIZE)-1:0]               select_bf_ap_idx_in,
        input [MAX_BF_AP_SIZE-1:0][SAMPLE_BIT_WIDTH-1:0] apod_consts_in,
        input [$clog2(MAX_BF_AP_SIZE)-1:0]               bf_ap_size_in,

        // Outputs
        output logic [SAMPLE_BIT_WIDTH-1:0] mac_out,
        output logic                        mac_en_out

    );

    // Local variables
    logic [$clog2(MAX_BF_AP_SIZE):0]                 bf_ap_size;
    logic [3:0][PIPELINE_SAMPLE_BIT_WIDTH+11-1:0]    buffer, buffer_next; // holds the apodized value; output logic is PIPELINE_SAMPLE_BIT_WIDTH+11 bits, then lose 11 LSB
    logic [MAX_BF_AP_SIZE-1:0][SAMPLE_BIT_WIDTH-1:0] apod_consts; // holds the bf aperture size

    // MAC buffer variables
    logic [MAC_BUFFER_ENTRIES-1:0][SAMPLE_BIT_WIDTH-1:0]       mac_buffer, mac_buffer_next; // holds the values staged for output
    logic [MAC_BUFFER_ENTRIES-1:0][$clog2(MAX_BF_AP_SIZE):0]   mac_count, mac_count_next; // holds the current number of accumulated values per mac buffer entry
    logic [$clog2(MAC_BUFFER_ENTRIES)-1:0]                     col_head, col_head_next; // SHARED head (points to mac buffer index at "head")
    logic [MAX_BF_AP_SIZE-1:0][$clog2(MAC_BUFFER_ENTRIES)-1:0] col_tail, col_tail_next; // holds number of values stored per bf_ap_idx (tail of virtual columns)
    logic [3:0][$clog2(MAC_BUFFER_ENTRIES)-1:0]                temp_tail, temp_tail_next; // used to index into mac buffer
    logic [SAMPLE_BIT_WIDTH-1:0]                               mac_output, mac_output_next; // holds the values staged for output
    logic                                                      mac_en_output, mac_en_output_next; // holds the enable bit staged for output
    logic [3:0][PIPELINE_SAMPLE_BIT_WIDTH-1:0]                 select_saved, select_saved_next;
    logic [3:0]                                                select_en_saved, select_en_saved_next, select_en_saved_2, select_en_saved_2_next; // saved input for second cycle accumulation of apodized values
    logic [$clog2(MAX_BF_AP_SIZE)-1:0]                         select_bf_ap_idx_saved, select_bf_ap_idx_saved_next;
    logic [SAMPLE_BIT_WIDTH-1:0]                               current_apod_const, current_apod_const_next;

    // Assign outputs
    assign mac_out    = mac_output;
    assign mac_en_out = mac_en_output;

    // Write Address Calculation
    always_comb begin
        // Set default values for this cycle
        mac_buffer_next             = mac_buffer;
        mac_count_next              = mac_count;
        col_head_next               = col_head;
        col_tail_next               = col_tail;
        buffer_next                 = 0;
        mac_output_next             = 0;
        mac_en_output_next          = 0;
        temp_tail_next              = 0;
        select_saved_next           = select_in;
        select_en_saved_next        = select_en_in;
        select_bf_ap_idx_saved_next = select_bf_ap_idx_in;
        current_apod_const_next     = apod_consts[select_bf_ap_idx_in];
        select_en_saved_2_next      = select_en_saved;

        // Perform the MAC operation
        for(int i = 0; i < 4; i++) begin
            if(select_en_saved[i]) begin
                // Set the tail which will be used to index into the mac buffer next cycle
                temp_tail_next[i]                     = col_tail[select_bf_ap_idx_saved] + i;

                // Increment the tail pointer for this virtual column by the number of valid samples being saved
                col_tail_next[select_bf_ap_idx_saved] = col_tail[select_bf_ap_idx_saved] + i + 1;

                // Perform apodization multiplication
                buffer_next[i]                        = $signed(current_apod_const) * $signed(select_saved[i]);
            end // if(select_en_saved[i])

            if(select_en_saved_2[i]) begin
                // Add the multiplication result to the mac buffer entry
                mac_buffer_next[temp_tail[i]] = $signed(mac_buffer[temp_tail[i]]) + $signed(buffer[i][PIPELINE_SAMPLE_BIT_WIDTH+11-1:11]);

                // Increment mac buffer entry count
                mac_count_next[temp_tail[i]]  = mac_count[temp_tail[i]] + 1;
            end // if(select_en_saved_2[i])
        end // for(int i = 0; i < 4; i++)

        if(mac_count[col_head] == bf_ap_size) begin
            mac_output_next           = mac_buffer[col_head];

            mac_en_output_next        = 1;

            mac_buffer_next[col_head] = 0;

            mac_count_next[col_head]  = 0;

            col_head_next             = col_head + 1;
        end // if(mac_count[col_head] == bf_ap_size)
    end // always_comb

    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(reset) begin
            bf_ap_size                  <= `SD bf_ap_size_in+1; // +1 because we're counting things, and zero is empty; TODO: make this cleaner
            apod_consts                 <= `SD apod_consts_in; // initialize the apodization constant array
            buffer                      <= `SD 0;
            mac_buffer                  <= `SD 0;
            mac_count                   <= `SD 0;
            col_head                    <= `SD 0;
            col_tail                    <= `SD 0;
            mac_output                  <= `SD 0;
            mac_en_output               <= `SD 0;
            temp_tail                   <= `SD 0;
            select_saved                <= `SD 0;
            select_en_saved             <= `SD 0;
            select_bf_ap_idx_saved      <= `SD 0;
            current_apod_const          <= `SD 0;
            select_en_saved_2           <= `SD 0;
        end else begin
            buffer                      <= `SD buffer_next;
            mac_buffer                  <= `SD mac_buffer_next;
            mac_count                   <= `SD mac_count_next;
            col_head                    <= `SD col_head_next;
            col_tail                    <= `SD col_tail_next;
            mac_output                  <= `SD mac_output_next;
            mac_en_output               <= `SD mac_en_output_next;
            temp_tail                   <= `SD temp_tail_next;
            select_saved                <= `SD select_saved_next;
            select_en_saved             <= `SD select_en_saved_next;
            select_bf_ap_idx_saved      <= `SD select_bf_ap_idx_saved_next;
            current_apod_const          <= `SD current_apod_const_next;
            select_en_saved_2           <= `SD select_en_saved_2_next;
        end // if(reset) begin
    end // always_ff @(posedge clock)
endmodule // mac
