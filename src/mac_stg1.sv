/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  mac_stg1.sv                                         //
//                                                                     //
//  Description :  Performs Multiply-and-Accumulate operations; stores //
//                 partially-accumulated values in buffer, which is a  //
//                 virtual 2D register file.                           //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module mac_stg1 #(parameter SAMPLE_BIT_WIDTH = 12, parameter PIPELINE_SAMPLE_BIT_WIDTH = 10, parameter MAX_BF_AP_SIZE = 32, parameter MAC_BUFFER_ENTRIES = 64) (

        // Inputs
        input                                            clock,
        input                                            reset,
        input [3:0][PIPELINE_SAMPLE_BIT_WIDTH-1:0]       select_in,
        input [3:0]                                      select_en_in,
        input [$clog2(MAX_BF_AP_SIZE)-1:0]               select_bf_ap_idx_in,
        input [MAX_BF_AP_SIZE-1:0][SAMPLE_BIT_WIDTH-1:0] apod_consts_in,

        // Outputs
        output logic [3:0][PIPELINE_SAMPLE_BIT_WIDTH-1:0] mac_stg1_out,
        output logic [3:0]                                mac_stg1_en_out,
        output logic [$clog2(MAX_BF_AP_SIZE)-1:0]         mac_stg1_bf_ap_idx_out

    );

    // Local variables
    logic [3:0][PIPELINE_SAMPLE_BIT_WIDTH-1:0]       buffer; 
    logic [3:0][PIPELINE_SAMPLE_BIT_WIDTH+11-1:0]    buffer_next; // holds the apodized value; output logic is PIPELINE_SAMPLE_BIT_WIDTH+11 bits, then lose 11 LSB
    logic [3:0]                                      buffer_en, buffer_en_next; // holds the apodized value's enable bit
    logic [MAX_BF_AP_SIZE-1:0][SAMPLE_BIT_WIDTH-1:0] apod_consts; // holds the bf aperture size
    logic [SAMPLE_BIT_WIDTH-1:0]                     current_apod_const, current_apod_const_next;
    logic [3:0][PIPELINE_SAMPLE_BIT_WIDTH-1:0]       select_saved, select_saved_next;
    logic [3:0]                                      select_en_saved, select_en_saved_next;
    logic [$clog2(MAX_BF_AP_SIZE)-1:0]               select_bf_ap_idx_saved, select_bf_ap_idx_saved_next, select_bf_ap_idx_saved_2, select_bf_ap_idx_saved_2_next;

    // Assign outputs
    assign mac_stg1_out           = buffer;
    assign mac_stg1_en_out        = buffer_en;
    assign mac_stg1_bf_ap_idx_out = select_bf_ap_idx_saved_2;

    always_comb begin
        buffer_next                   = 0;
        buffer_en_next                = 0;

        // Save the inputs and fetch apod constant
        select_saved_next             = select_in;
        select_en_saved_next          = select_en_in;
        select_bf_ap_idx_saved_next   = select_bf_ap_idx_in;
        select_bf_ap_idx_saved_2_next = select_bf_ap_idx_saved;
        current_apod_const_next       = apod_consts[select_bf_ap_idx_in];

        for(int i = 0; i < 4; i++) begin
            // Perform apodization multiplication
            buffer_next[i] = $signed(current_apod_const) * $signed(select_saved[i]);

            // Enable outputs
            if(select_en_saved[i]) begin
                buffer_en_next[i] = 1;
            end // if(select_en_saved[i])
        end // for(int i = 0; i < 4; i++)
    end // always_comb

    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(reset) begin
            buffer                   <= `SD 0;
            buffer_en                <= `SD 0;
            select_saved             <= `SD 0;
            select_en_saved          <= `SD 0;
            select_bf_ap_idx_saved   <= `SD 0;
            select_bf_ap_idx_saved_2 <= `SD 0;
            current_apod_const       <= `SD 0;
            apod_consts              <= `SD apod_consts_in; // initialize the apodization constant array
        end else begin
            for(int i = 0; i < 4; i++) begin
                buffer[i]            <= `SD buffer_next[i][PIPELINE_SAMPLE_BIT_WIDTH+11-1:11];
            end // for(int i = 0; i < 4; i++)
            buffer_en                <= `SD buffer_en_next;
            select_saved             <= `SD select_saved_next;
            select_en_saved          <= `SD select_en_saved_next;
            select_bf_ap_idx_saved   <= `SD select_bf_ap_idx_saved_next;
            select_bf_ap_idx_saved_2 <= `SD select_bf_ap_idx_saved_2_next;
            current_apod_const       <= `SD current_apod_const_next;
        end // if(reset) begin
    end // always_ff @(posedge clock)
endmodule // mac_stg1
