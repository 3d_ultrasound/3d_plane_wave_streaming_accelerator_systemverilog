#!/usr/bin/env python
import filecmp
import os
import fileinput

def prRed(skk): print("\033[91m {}\033[00m" .format(skk))
def prGreen(skk): print("\033[92m {}\033[00m" .format(skk))
def prYellow(skk): print("\033[93m {}\033[00m" .format(skk))
def prLightPurple(skk): print("\033[94m {}\033[00m" .format(skk))
def prPurple(skk): print("\033[95m {}\033[00m" .format(skk))
def prCyan(skk): print("\033[96m {}\033[00m" .format(skk))
def prLightGray(skk): print("\033[97m {}\033[00m" .format(skk))
def prBlack(skk): print("\033[98m {}\033[00m" .format(skk))



os.system('cat test.out | tr -d "[:blank:]" > checker_files/test.out.check')

if filecmp.cmp('checker_files/test.out.check', 'checker_files/sv_firing_1_stg1_output_SLICE1'):
	prGreen("Pass :D")
else:
	prRed("Fail :(")

os.remove('checker_files/test.out.check')
