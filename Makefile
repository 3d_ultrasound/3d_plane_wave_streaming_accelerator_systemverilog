# make          <- runs simv (after compiling simv if needed)
# make all      <- runs simv (after compiling simv if needed)
# make simv     <- compile simv if needed (but do not run)
# make syn      <- runs syn_simv (after synthesizing if needed then 
#                                 compiling synsimv if needed)
# make clean    <- remove files created during compilations (but not synthesis)
# make nuke     <- remove all files created during compilation and synthesis
#
# To compile additional files, add them to the TESTBENCH or SIMFILES as needed
# Every .vg file will need its own rule and one or more synthesis scripts
# The information contained here (in the rules for those vg files) will be 
# similar to the information in those scripts but that seems hard to avoid.
#
#

VCS = SW_VCS=2015.09 vcs -sverilog +vc -Mupdate -line -full64
LIB = /afs/umich.edu/class/eecs470/lib/verilog/lec25dscc25.v

# For visual debugger
VISFLAGS = -lncurses

all:    simv
	./simv | tee program.out

##### 
# Modify starting here
#####

TESTBENCH =	sys_defs.vh				\
			testbench/testbench.sv

SIMFILES =	src/pipeline_wrapper.sv										\
			src/pipeline.sv												\
			src/sample.sv												\
			src/interp.sv												\
			src/rotate.sv												\
			src/select.sv									\
			src/regfile.sv												\
			src/bank.sv													\
			src/apod.sv													\
			src/accum.sv												\
			src/mac.sv													\
			src/apod_v2_wrap.sv											\
			src/apod_v2.sv												\
			src/mac_stg1.sv												\
			src/mac_stg2.sv									\
			src/mac_stg2_bank.sv										\
			sram/45nm/input_4096x12b/sram_2p_hs_v60_uvtsvt_input.v		\
			sram/45nm/regfile_256x10b/sram_2p_hs_v60_uvtsvt_10b.v		\
			sram/45nm/tau_rx_tx2_512x64b/sram_2p_hs_v60_uvtsvt_tau64.v	\
			sram/16nm/input_4096x12b/sram_2p_uhde_in.v					\
			sram/16nm/regfile_256x10b/sram_2p_uhde_10b.v				\
			sram/16nm/tau_rx_tx2_512x64b/sram_2p_uhde_tau.v				\
			sram/14nm/regfile_256x10b/rf_2p_hsc_10b.v					\
			sram/14nm/mac_buffer_32x12b/rf_2p_hsc_12b.v

SYNFILES =	synth/sample.vg

# For visual debugger
VISTESTBENCH = $(TESTBENCH:tempbench_pipeline.sv=visual_tempbench_pipeline.sv)	\
		testbench/visual_c_hooks.c

synth/sample.vg:	$(SIMFILES) synth/pipeline.tcl
	dc_shell-t -f ./synth/pipeline.tcl | tee pipeline.out 
	# cd synth && dc_shell-t -f ./pipeline.tcl | tee pipeline.out 

#####
# Should be no need to modify after here
#####
simv:	$(SIMFILES) $(TESTBENCH)
	$(VCS) $(TESTBENCH) $(SIMFILES) -o simv
	
dve:	$(SIMFILES) $(TESTBENCH)
	$(VCS) +memcbk $(TESTBENCH) $(SIMFILES) -o dve -R -gui
.PHONY:	dve

# For visual debugger
vis_simv:	$(SIMFILES) $(VISTESTBENCH)
	$(VCS) $(VISFLAGS) $(VISTESTBENCH) $(SIMFILES) -o vis_simv 
	./vis_simv

syn_simv:	$(SYNFILES) $(TESTBENCH)
	$(VCS) $(TESTBENCH) $(SYNFILES) $(LIB) -o syn_simv 

syn:	syn_simv
	./syn_simv | tee syn_program.out

clean:
	rm -rf simv simv.daidir csrc vcs.key program.out
	rm -rf vis_simv vis_simv.daidir
	rm -rf dve*
	rm -rf syn_simv syn_simv.daidir syn_program.out
	rm -rf synsimv synsimv.daidir csrc vcdplus.vpd vcs.key synprog.out pipeline.out writeback.out vc_hdrs.h
	rm -rf *.log ucli.key
	rm -rf crte_0000* Synopsys_stack_trace_*
	rm -f *.saif

nuke:	clean
	rm -f synth/*.vg synth/*.rep synth/*.ddc synth/*.chk synth/command.log
	rm -f synth/*.out command.log synth/*.db synth/*.svf
	rm -f synth/results/*
	rm -rf alib-52/
	rm -rf DVEfiles/
	rm -rf default.svf inter.vpd
	rm -rf synth/work
