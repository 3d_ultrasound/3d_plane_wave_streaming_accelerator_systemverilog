# ================================ SETUP =================================

set src [list sys_defs.vh src/apod.sv]
# set src [list sys_defs.vh src/apod_piped.sv src/pipe_mult.sv src/mult_stage.sv]
set top_module apod

# # Arm library
# set target_library [list db/sc9_cln65lp_base_rvt_tt_typical_max_1p20v_25c.db]
# set link_library [list db/sc9_cln65lp_base_rvt_tt_typical_max_1p20v_25c.db]

# # 470 Library
# set target_library [list "/afs/umich.edu/class/eecs470/lib/synopsys/lec25dscc25_TT.db"]
# set link_library [list "/afs/umich.edu/class/eecs470/lib/synopsys/lec25dscc25_TT.db"]

# # NanGate Library
# set target_library [list /home/westbl/Downloads/NangateOpenCellLibrary.db]
# set link_library [list /home/westbl/Downloads/NangateOpenCellLibrary.db]

# # IBM 45nm SOI Library
# set target_library [list /afs/eecs.umich.edu/kits/ARM/IBM_soi12s0/sc12_rvt/db/sc12_base_v31_rvt_soi12s0_ss_nominal_max_0p81v_125c_mns.db]
# set link_library [list /afs/eecs.umich.edu/kits/ARM/IBM_soi12s0/sc12_rvt/db/sc12_base_v31_rvt_soi12s0_ss_nominal_max_0p81v_125c_mns.db]

# TEST
set target_library [list /afs/eecs.umich.edu/kits/ARM/IBM_soi12s0/sc9_hvt/db/sc9_base_v31_hvt_soi12s0_ss_nominal_max_0p72v_m40c_mxs.db]
set link_library [list /afs/eecs.umich.edu/kits/ARM/IBM_soi12s0/sc9_hvt/db/sc9_base_v31_hvt_soi12s0_ss_nominal_max_0p72v_m40c_mxs.db]

# set_host_options -max_cores [expr min(16, $::env(USE_NUM_CORES))]
set_host_options -max_cores 16

# set_flatten true -effort medium -minimize multiple_output
set_flatten false

define_design_lib WORK -path "./synth/work"

analyze -format sverilog -lib WORK $src
elaborate $top_module -lib WORK
current_design $top_module

link
uniquify

# set_dont_touch [get_cells {icache BTB tlb} ]

# =============================== CLOCKING ===============================

create_clock -period 0.78 clock
# create_clock -period 1 clock
# create_clock -period 1.5 clock

set real_inputs [remove_from_collection [all_inputs] [list clock] ]

set_input_delay -clock clock -max 0 $real_inputs
set_output_delay -clock clock -max 0 [all_outputs]

set_max_delay 10 [all_outputs]

# ============================= COMPILATION ==============================


# ungroup -all -flatten

check_design
# compile -area_effort high -map_effort high -power_effort high
# compile_ultra -no_boundary_optimization
compile_ultra
# compile_ultra -retime
# compile_ultra -gate_clock
# compile_ultra -retime -gate_clock

# =============================== REPORTS ================================

report_area > synth/results/apod_area.rpt
report_timing > synth/results/apod_timing.rpt
report_power > synth/results/apod_power.rpt
report_units > synth/results/apod_units.rpt
# write -format verilog -output nl.sv


exit
