#!/usr/bin/env python
import filecmp
import os
import fileinput
import sys

def prRed(skk): print("\033[91m {}\033[00m" .format(skk))
def prGreen(skk): print("\033[92m {}\033[00m" .format(skk))
def prYellow(skk): print("\033[93m {}\033[00m" .format(skk))
def prLightPurple(skk): print("\033[94m {}\033[00m" .format(skk))
def prPurple(skk): print("\033[95m {}\033[00m" .format(skk))
def prCyan(skk): print("\033[96m {}\033[00m" .format(skk))
def prLightGray(skk): print("\033[97m {}\033[00m" .format(skk))
def prBlack(skk): print("\033[98m {}\033[00m" .format(skk))


def replace_words(base_text, device_values):
    for key, val in device_values.items():
        base_text = base_text.replace(key, val)
    return base_text

input_data_folder = "CYST_HARMONIC_5_ANGLE"

# Change the testbench in the Makefile
replace_set = {}
replace_set["testbench.sv"] = "tempbench.sv"
t = open('Makefile', 'r')
tempstr = t.read()
t.close()
output = replace_words(tempstr, replace_set)
fout = open('Makefile', 'w')
fout.write(output)
fout.close()

# Comment out any SAIF generation lines
replace_set = {}
replace_set["        $set_gate_level_monitoring"] = "        // $set_gate_level_monitoring"
replace_set["        $set_toggle_region"] = "        // $set_toggle_region"
replace_set["        $toggle_start"] = "        // $toggle_start"
replace_set["        $toggle_stop"] = "        // $toggle_stop"
replace_set["        $toggle_report"] = "        // $toggle_report"
replace_set["input_data_folder"] = input_data_folder
# t = open('testbench/tempbench.bak', 'r')
t = open('testbench/tempbench_pipeline.bak', 'r')
tempstr = t.read()
t.close()
output = replace_words(tempstr, replace_set)
fout = open('testbench/tempbench.sv', 'w')
fout.write(output)
fout.close()

os.system('make clean')
os.system('clear')

# Perform the tests
for firing_id in range(1, 6):
    for stage_id in range(1, 3):
        for slice_id in range(1, 2):
            # print("Processing Firing %d, Stage %d, Slice %d" % (firing_id, stage_id, slice_id))
            sys.stdout.write('\r' + str("Processing Firing %d, Stage %d, Slice %d..." % (firing_id, stage_id, slice_id)))
            sys.stdout.flush() # important

            replace_set = {}
            replace_set["sv_firing_id"] = "sv_firing_%d" % firing_id
            replace_set["stgid"] = "stg%d" % stage_id
            replace_set["SLICE_id"] = "SLICE_%d" % slice_id

            t = open('testbench/tempbench.sv', 'r')
            tempstr = t.read()
            t.close()

            output = replace_words(tempstr, replace_set)

            # Write out the new file
            fout = open('testbench/tempbench.sv', 'w')
            fout.write(output)
            fout.close()

            # os.system('make')
            os.system('make > /dev/null')

            os.system('cat test.out | tr -d "[:blank:]" > checker_files/test.out.check')

            if filecmp.cmp('checker_files/test.out.check', 'checker_files/%s/sv_firing_%d_stg%d_output_SLICE%d' % (input_data_folder, firing_id, stage_id, slice_id)):
                prGreen("\rFiring %d, Stage %d, Slice %d" % (firing_id, stage_id, slice_id) + ' ' * 20)
                # prGreen("Pass :D")
            else:
                prRed("\rFiring %d, Stage %d, Slice %d" % (firing_id, stage_id, slice_id) + ' ' * 20)
                # prRed("Fail :(")

            replace_set = {}
            replace_set["sv_firing_%d" % firing_id] = "sv_firing_id"
            replace_set["stg%d" % stage_id] = "stgid"
            replace_set["SLICE_%d" % slice_id] = "SLICE_id"

            t = open('testbench/tempbench.sv', 'r')
            tempstr = t.read()
            t.close()

            output = replace_words(tempstr, replace_set)

            # Write out the new file
            fout = open('testbench/tempbench.sv', 'w')
            fout.write(output)
            fout.close()

# Reset the testbench in the Makefile
replace_set = {}
replace_set["tempbench.sv"] = "testbench.sv"
t = open('Makefile', 'r')
tempstr = t.read()
t.close()
output = replace_words(tempstr, replace_set)
fout = open('Makefile', 'w')
fout.write(output)
fout.close()

os.remove('checker_files/test.out.check')
