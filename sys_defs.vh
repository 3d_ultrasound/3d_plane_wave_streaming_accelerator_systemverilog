/////////////////////////////////////////////////////////////////////////
//                                                                     //
//   Modulename :  sys_defs.vh                                         //
//                                                                     //
//  Description :  This file has the macro-defines for macros used in  //
//                 the pipeline design.                                //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

`ifndef __SYS_DEFS_VH__
`define __SYS_DEFS_VH__

//////////////////////////////////////////////
//                                          //
//           REQUIRED DEFINITIONS           //
//                                          //
//////////////////////////////////////////////
typedef enum logic [6:0] {
    NULL_PACKET              = 7'h0, // 
    RESET_PIPELINE_WRAPPER   = 7'h1, // 
    LD_BF_AP_SIZE            = 7'h2, // 
    LD_APOD_CONSTS           = 7'h3, // 
    LD_TAU_TX1               = 7'h4, // 
    LD_INPUT_DATA            = 7'h5, // 
    LD_TAU_RX_TX2            = 7'h6, // 
    LD_TAU_RX_TX2_INIT_DELAY = 7'h7, // 
    LD_NUM_SHIFTS            = 7'h8, //
    LD_OUTPUT_SELECT         = 7'h9, // 
    INIT_PIPELINES           = 7'h10, // 
    RUN_PIPELINES            = 7'h11, // 
    READ_OUTPUT              = 7'h12 //  
} bsg_fsb_opcode_s;

typedef struct packed {
    // these bits are reserved and needed for this network
    logic [3:0]      destid; // 4 bits
    logic [0:0]      cmd;    // 1 bits (1 for switch, 0 for node)

    // for cmd=0, these 75 bits are free for general use; they can be repurposed
    bsg_fsb_opcode_s opcode; // 7 bits - only looked at by switch
    logic [3:0]      srcid;  // 4 bits
    logic [63:0]     data;   // 64 bits
} bsg_fsb_pkt_s;



//////////////////////////////////////////////
//                                          //
//              DESIGN CHOICES              //
//                                          //
//////////////////////////////////////////////
///////////Technology Node Agnostic///////////
// Output options
`define TAPEOUT
typedef enum logic [3:0] {
  ACCUM     = 4'h0,
  APOD      = 4'h1,
  REGFILE   = 4'h2,
  SELECT    = 4'h3,
  ROTATE_3  = 4'h4,
  ROTATE_2  = 4'h5,
  ROTATE_1  = 4'h6,
  ROTATE_0  = 4'h7,
  INTERP_3  = 4'h8,
  INTERP_2  = 4'h9,
  INTERP_1  = 4'hA,
  INTERP_0  = 4'hB,
  SAMPLE    = 4'hC,
  INPUT     = 4'hD
} PIPELINE_OUTPUT_SELECT;

//////////////////////////////////////////////
//                                          //
//              Pipeline Setup              //
//                                          //
//////////////////////////////////////////////
// Configure Imaging Parameters
`define BF_AP_SIZE 20 // number of transducers in the beamforming aperture (per dim)

// Configure Input Data Location
`define INPUT_DATA_FOLDER "CYST_HARMONIC_5_ANGLE" // number of transducers in the beamforming aperture (per dim)

// Configure general constant initialization
`define TAU_TX1_FILE_READ_WIDTH 4 // number of values per read
`define APOD_CONSTS_FILE_READ_WIDTH 4 // number of values per read

// Configure tau_rx_tx2 SRAM for ON-CHIP STORAGE
`define TAU_SRAM_WIDTH 64 // number of values (one bit in this case) per SRAM entry
`define TAU_SRAM_NUM_ENTRIES 512 // number of entries per SRAM (we use banked SRAMs for lower latency)
`define TAU_RX_TX2_FILE_READ_WIDTH 32 // number of values (one bit in this case) per read; THIS MUST BE A POWER OF TWO LESS THAN TAU_SRAM_WIDTH

// Configure input_data SRAM for ON-CHIP STORAGE
`define DATA_SRAM_NUM_ENTRIES 4096 // number of entries per SRAM (we use banked SRAMs for lower latency)
`define INPUT_DATA_FILE_READ_WIDTH 4 // number of values per read

// Enable Register File - enabling uses regfile + apod + accum; disabling uses MAC unit
// `define USE_REGFILE

// Enable MAC banked buffer - enabling uses MAC banked buffer (SRAM or other); disabling uses non-banked buffer
// `define USE_MAC_BANK

// Enable SRAM Register File
// `define USE_REGFILE_SRAM

// Enable SRAM MAC Buffer
// `define USE_MAC_SRAM

// Enable 45nm SRAM instance
// `define USE_SRAM_45

// Enable 16nm SRAM instances
// `define USE_SRAM_16

// Enable 14nm SRAM instances
// `define USE_SRAM_14



//////////////////////////////////////////////
//                                          //
//           Testbench Attributes           //
//                                          //
//////////////////////////////////////////////
// Clock period for test bench
`define VERILOG_CLOCK_PERIOD 10.0

// Set the signal assignment delay; used for SRAMs that don't have a "fast" mode (IBM 45nm SOI)
`define SD //#1

// Set up the SRAM for simulation
`define INITIALIZE_MEMORY 1
`define ARM_UD_MODEL

`endif
